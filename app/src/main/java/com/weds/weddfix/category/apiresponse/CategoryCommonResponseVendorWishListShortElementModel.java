package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 03-03-2017.
 */

public class CategoryCommonResponseVendorWishListShortElementModel {

    @SerializedName("shortlist")
    public CategoryVendorWishListShortDataModel categoryVendorWishListShortDataModel;


}
