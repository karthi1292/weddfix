package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 3/11/2017.
 */

public class CategoryBannerPhotoDataModel {
    @SerializedName("id")
    public String id;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("statusName")
    public String statusName;
}
