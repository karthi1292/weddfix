package com.weds.weddfix.category.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryNameDataModel;
import com.weds.weddfix.category.common.CategoryBaseActivity;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.fragment.CategoryListFragment;
import com.weds.weddfix.category.fragment.HomeFragment;
import com.weds.weddfix.category.fragment.MyAccountFragment;
import com.weds.weddfix.matrimony.activity.MatrimonyHomeActivity;
import com.weds.weddfix.matrimony.activity.MtnyLoginActivity;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends CategoryBaseActivity {

    private Toolbar toolbar;
    private Button matrimonyBtn;
    private Fragment fragment;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView mNavigationView;
    private int isSearch = 0;
    private MtnyStorePreference mtnyStorePreference;
    private CategoryStorePreference categoryStorePreference;
    private boolean mtnyLoggedIn;
    private boolean categoryLoggedIn;
    private String uProfilePic;
    private View menuHeader;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_home);

    /*    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }*/

        initWidgets();

        initNavigationView();

        loadDrawerHeader();

        matrimonyBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (mtnyLoggedIn) {
                    Intent mtnyHomeintent = new Intent(HomeActivity.this, MatrimonyHomeActivity.class);
                    startActivity(mtnyHomeintent);
                } else {
                    Intent mtnyLoginIntent = new Intent(HomeActivity.this, MtnyLoginActivity.class);
                    startActivity(mtnyLoginIntent);
                }
            }
        });

        fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.category_list_frame_layout, fragment).commit();
    }

    private void initWidgets() {
        matrimonyBtn = (Button) findViewById(R.id.matrimony_login);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mtnyStorePreference = new MtnyStorePreference(this);
        mtnyLoggedIn = mtnyStorePreference.getBooleanValue(MtnyConstants.LOGGED_IN);

        categoryStorePreference = new CategoryStorePreference(this);
        categoryLoggedIn = categoryStorePreference.getBooleanValue(CategoryConstants.U_LOGGED);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initNavigationView() {
        Menu categoryMenu = mNavigationView.getMenu();

        ArrayList<CategoryNameDataModel> categoryNameDataModelList = CategorySingleton.getInstance().categoryHomeDataModel.categoryNameDataModel;

        for (int i = 0; i < categoryNameDataModelList.size(); i++) {
            categoryMenu.add(R.id.group1, Integer.parseInt(categoryNameDataModelList.get(i).id), 0, categoryNameDataModelList.get(i).categoryName);
        }

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open_nav_drawer, R.string.close_nav_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        final MenuItem menu = mNavigationView.getMenu().findItem(R.id.menu_logout);

        if (categoryLoggedIn) {
            menu.setTitle(CategoryConstants.LOGOUT);
        } else {
            menu.setTitle(CategoryConstants.LOGIN);
        }

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                String categoryId = String.valueOf(menuItem.getItemId());

                switch (menuItem.getItemId()) {
                    case R.id.menu_home:
                        fragment = new HomeFragment();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.category_list_frame_layout, fragment).commit();
                        break;

                    case R.id.nav_matrimony:
                        if (mtnyLoggedIn) {
                            Intent mtnyHomeintent = new Intent(HomeActivity.this, MatrimonyHomeActivity.class);
                            startActivity(mtnyHomeintent);
                        } else {
                            Intent mtnyLoginIntent = new Intent(HomeActivity.this, MtnyLoginActivity.class);
                            startActivity(mtnyLoginIntent);
                        }
                        break;

                    case R.id.menu_logout:
                        if (categoryLoggedIn) {
                            categoryStorePreference.clearEditor(CategoryConstants.MyPREFERENCESNAME);
                            categoryStorePreference.setString(CategoryConstants.USER_ID, "0");
                            categoryLoggedIn = categoryStorePreference.getBooleanValue(CategoryConstants.U_LOGGED);
                            if (!categoryLoggedIn) {
                                menu.setTitle(CategoryConstants.LOGIN);
                            }
                        } else {
                            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                            finish();
                        }
                        break;

                    case R.id.menu_your_account:
                        if (categoryLoggedIn) {
                            fragment = new MyAccountFragment();
                            FragmentManager fragmentManager1 = getSupportFragmentManager();
                            fragmentManager1.beginTransaction().replace(R.id.category_list_frame_layout, fragment).commit();
                        } else {
                            Toast.makeText(getApplicationContext(), CategoryConstants.LOGIN_TO_CONTINUE, Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        categoryVendorServicesList(categoryId);
                        break;
                }

                mDrawerLayout.closeDrawers();
                return false;
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mDrawerToggle.onOptionsItemSelected(item);
        switch (item.getItemId()) {

          /*   case R.id.menu_notifications:
                break;
           case R.id.menu_actionbar_search:
                if (isSearch == 0) {
                    // ed_search.setVisibility(View.VISIBLE);
                    fragment = new SearchFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.category_list_frame_layout, fragment).commit();
                    if (item.getItemId() == R.id.menu_actionbar_search)
                        item.setIcon(R.mipmap.ic_home_white_24dp);
                    isSearch = 1;
                } else {
                    // ed_search.setVisibility(View.GONE);
                    if (item.getItemId() == R.id.menu_actionbar_search)
                        item.setIcon(R.mipmap.ic_search_white_24dp);
                    fragment = new HomeFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.category_list_frame_layout, fragment).commit();
                    isSearch = 0;
                }
                break;*/
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void loadDrawerHeader() {
        View header = mNavigationView.getHeaderView(0);

        TextView txtUserName = (TextView) header.findViewById(R.id.text_drawer_username);

        txtUserName.setText(categoryStorePreference.getStringValue(CategoryConstants.U_FNAME));
        uProfilePic = categoryStorePreference.getStringValue(CategoryConstants.U_PRO_PIC);

        CircleImageView profileImage = (CircleImageView) header.findViewById(R.id.img_drawer_profile_pic);
        Glide.with(this).load(uProfilePic).placeholder(R.drawable.profile_placeholder).into(profileImage);
    }

    public void categoryVendorServicesList(String categoryId) {

        fragment = new CategoryListFragment(categoryId);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.category_list_frame_layout, fragment).commit();
    }
}
