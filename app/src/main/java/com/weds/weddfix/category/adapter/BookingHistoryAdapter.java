package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.common.CategorySingleton;

/**
 * Created by rameshmuthu on 08-03-2017.
 */

public class BookingHistoryAdapter extends RecyclerView.Adapter<BookingHistoryAdapter.ViewHolder> {
    private Context mContext;
    private String id;


    public BookingHistoryAdapter(Context context) {

        this.mContext = context;


    }


    @Override
    public BookingHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.custom_booking_history_row, parent, false);

        return new BookingHistoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookingHistoryAdapter.ViewHolder holder, final int position) {


        String vCompanyName = CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).companyName;
        String vId = CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).id;
        String vAddress = CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).address;
        String vLocation = CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).location;
        String vPrice = CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).price;
        String vMaxRating = CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).maxRating;
        String vMaxUserRating = CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).maxUsersRating;
        String vMinRating = CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).minRating;
        String vImage = CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).fileName;
        String vName=CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).contactName;
        String vEmail=CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).contactEmail;
        String vPhone=CategorySingleton.getInstance().categoryOrderHistoryDataModels.get(position).contactMobile;

        Glide.with(mContext).load(vImage).into(holder.imgVendor);
        holder.vCompanyName.setText(vCompanyName);
        holder.vAddress.setText(vAddress);
        holder.vPrice.setText(vPrice);
        holder.vContactPhone.setText(vPhone);
        holder.vContactEmail.setText(vEmail);
        holder.vContactName.setText(vName);



    }


    @Override
    public int getItemCount() {
        Log.d("ramesh", CategorySingleton.getInstance().categoryOrderHistoryDataModels.size() + " myBookings");
        return CategorySingleton.getInstance().categoryOrderHistoryDataModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView vCompanyName,vAddress,vContactName,vContactEmail,vContactPhone,vPrice;
        ImageView imgVendor;



        public ViewHolder(View itemView) {
            super(itemView);
            vCompanyName = (TextView) itemView.findViewById(R.id.txt_booking_history_name);
            imgVendor = (ImageView) itemView.findViewById(R.id.img_booking_history);
            vAddress = (TextView) itemView.findViewById(R.id.txt_booking_history_address);
            vContactName= (TextView) itemView.findViewById(R.id.txt_booking_history_contact_name);
            vContactEmail= (TextView) itemView.findViewById(R.id.txt_booking_history_contact_email);
            vContactPhone= (TextView) itemView.findViewById(R.id.txt_booking_history_contact_phone);
            vPrice= (TextView) itemView.findViewById(R.id.txt_booking_history_price);



        }

    }


}
