package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 18-02-2017.
 */

public class CategoryCommonResponseModelOTPVerified {
    @SerializedName("verification")
    public CategoryUserOTPVerifiedProfileModel categoryUserOTPVerifiedProfileModel;
}
