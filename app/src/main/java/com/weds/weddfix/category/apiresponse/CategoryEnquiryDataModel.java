package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 15-03-2017.
 */

public class CategoryEnquiryDataModel {
    @SerializedName("status")
    public String status;
    @SerializedName("mtny_message")
    public String message;
}
