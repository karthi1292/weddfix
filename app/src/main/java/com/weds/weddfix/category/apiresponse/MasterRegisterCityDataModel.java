package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;
import com.weds.weddfix.category.apiresponse.CategoryRegisterCityDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class MasterRegisterCityDataModel {

    @SerializedName("city")
    public List<CategoryRegisterCityDataModel> categRegisterCityDataModelList=new ArrayList<>();

    public MasterRegisterCityDataModel() {
    }

    public List<CategoryRegisterCityDataModel> getcategRegisterCityDataModelList() {
        return categRegisterCityDataModelList;
    }

    public void setMtnyRegisterCityDataModelList(List<CategoryRegisterCityDataModel> categRegisterCityDataModelList) {
        this.categRegisterCityDataModelList = categRegisterCityDataModelList;
    }

}
