package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 07-03-2017.
 */

public class CategoryCommonResponseProfileDetailsElementModel {

    @SerializedName("myProfile")
    public CategoryUserProfileDetailModel categoryUserProfileDetailModel;

}
