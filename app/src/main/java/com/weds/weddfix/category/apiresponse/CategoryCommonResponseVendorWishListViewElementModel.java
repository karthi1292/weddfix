package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rameshmuthu on 04-03-2017.
 */

public class CategoryCommonResponseVendorWishListViewElementModel {
    @SerializedName("myWishList")
    public ArrayList<CategoryVendorWishListViewDataModel> categoryVendorWishListViewDataModel;
}
