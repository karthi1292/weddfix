package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryRegisterStateDataModel;

import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class CatgStateSpinnerAdapter extends BaseAdapter {

    private Context mContext;
    private List<CategoryRegisterStateDataModel> stateDataModelArrayList;

    public CatgStateSpinnerAdapter(Context context, List<CategoryRegisterStateDataModel> catgRegisterStateDataModelList) {
        this.mContext=context;
        this.stateDataModelArrayList=catgRegisterStateDataModelList;
    }
    @Override
    public int getCount() {
        return stateDataModelArrayList.size();
    }

    @Override
    public CategoryRegisterStateDataModel getItem(int position) {
        return stateDataModelArrayList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }
    public String getStateName(int position) {
        return getItem(position).getStateName();
    }
    public String getStateId(int position) {
        return getItem(position).getId();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        CatgStateSpinnerAdapter.ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new CatgStateSpinnerAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, viewGroup, false);
            holder.txtStateName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (CatgStateSpinnerAdapter.ViewHolder) convertView.getTag();
        }
        holder.txtStateName.setText(getStateName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtStateName;
    }
}
