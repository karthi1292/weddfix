package com.weds.weddfix.category.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.weds.weddfix.R;

/**
 * Created by CIPL307 on 9/28/2015.
 */
public class CategoryCommonMethods {

    //to check network availability
    public static boolean isNetworkConnectionAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null)
            return false;
        NetworkInfo.State network = info.getState();
        if (network == null)
            return false;
        return network == NetworkInfo.State.CONNECTED;
    }



    /*public static Dialog showAlertDialog(final Context context, String title, String mtny_message, final DialogResponse dialogResponse) {

        final Context mContext;
        mContext = context;

        final Dialog alertDialogBuilder = new Dialog(mContext);
        alertDialogBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogBuilder.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.alert_dialog, null, false);

        TextView titleText, messageText;
        final EditText editForgot;
        Button okBtn, cancelBtn;
        // titleText = (TextView)view.findViewById(R.id.dialog_title);
        messageText = (TextView) view.findViewById(R.id.dialog_body);
        editForgot = (EditText) view.findViewById(R.id.edit_forgot_pass);
        okBtn = (Button) view.findViewById(R.id.dialog_ok_btn);
        //cancelBtn=(Button)view.findViewById(R.id.dialog_cancel_btn);

        //set typeface to views
        Typeface typeface = CategoryFontUtil.getHelvetica(mContext);
        //titleText.setTypeface(typeface);
        //titleText.setTypeface(typeface);
        okBtn.setTypeface(typeface);

        //set the values
        // titleText.setText(title);
        messageText.setText(mtny_message);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mContext.onOkButtonClicked(alertDialogBuilder,"");
                dialogResponse.onOkButtonClicked(alertDialogBuilder, editForgot);
            }
        });

        alertDialogBuilder.setContentView(view);

        return alertDialogBuilder;

    }*/

    public static void showToast(String message, Context mContext) {
        Toast.makeText(mContext, "" + message, Toast.LENGTH_SHORT).show();
    }

    //customized toast
    public static void showToast(Context mContext, String message) {
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_toast, null, false);

        TextView messageView = (TextView) view.findViewById(R.id.custom_toast_text);

        //set typeface to views
        Typeface typeface = CategoryFontUtil.getHelvetica(mContext);
        messageView.setTypeface(typeface);


        messageView.setText(message);

        Toast toast = new Toast(mContext);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setView(view);//setting the view of custom toast layout
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

}
