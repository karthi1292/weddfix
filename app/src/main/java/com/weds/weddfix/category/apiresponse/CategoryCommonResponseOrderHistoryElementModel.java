package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rameshmuthu on 08-03-2017.
 */

public class CategoryCommonResponseOrderHistoryElementModel {

    @SerializedName("myBookings")
    public ArrayList<CategoryOrderHistoryDataModel> categoryOrderHistoryDataModels;
}
