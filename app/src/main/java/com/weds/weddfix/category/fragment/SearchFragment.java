package com.weds.weddfix.category.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


import com.weds.weddfix.R;


import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment {

    RecyclerView recyclerView;
    EditText ed_search;
    LinearLayoutManager layoutManager;
    List<String> l_id, l_pname, l_pperday, l_image, l_quantity;
    String url_search;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_search);
        ed_search = (EditText) view.findViewById(R.id.ed_search_home);
        l_id = new ArrayList<String>();
        l_pname = new ArrayList<String>();
        l_pperday = new ArrayList<String>();
        l_image = new ArrayList<String>();
        l_quantity = new ArrayList<String>();
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().isEmpty()) {



                } else {

            }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }
}
