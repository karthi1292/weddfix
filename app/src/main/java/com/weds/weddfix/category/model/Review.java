package com.weds.weddfix.category.model;

/**
 * Created by Acer on 30-11-2016.
 */
public class Review {

    String name, review, date, rating;

    public Review(String name, String review, String date, String rating) {

        this.name = name;
        this.review = review;
        this.date = date;
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
