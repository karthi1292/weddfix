package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.activity.CategoryDetailsActivity;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.interfaces.CategoryTriggerWithTwoString;

/**
 * Created by rameshmuthu on 04-03-2017.
 */

public class YourWishListAdapter extends RecyclerView.Adapter<YourWishListAdapter.ViewHolder>{

    private Context mcontext;
    private String vId, vCompanyName;
    private CategoryTriggerWithTwoString trig;
    private int wishListPosition;

    public YourWishListAdapter(Context context) {

        this.mcontext = context;
        trig =(CategoryTriggerWithTwoString) context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mcontext).inflate(R.layout.custom_wishlist_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        wishListPosition=position;

        vCompanyName = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).companyName;
        vId = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).id;
        String vAddress = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).address;
        String vLocation = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).location;
        String vPrice = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).price;
        String vMaxRating = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).maxRating;
        String vMaxUserRating = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).maxUsersRating;
        String vMinRating = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).minRating;
        String vImage = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).fileName;

        Glide.with(mcontext).load(vImage).into(holder.img_vendor);

        holder.vName.setText(vCompanyName);
        holder.txt_booking_history_price.setText(vPrice);

        holder.btnBookService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vId = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).id;
                Intent i = new Intent(mcontext, CategoryDetailsActivity.class);
                i.putExtra(CategoryConstants.VENDOR_ID,vId);
                mcontext.startActivity(i);
            }
        });
        holder.btnUnshortistService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vId = CategorySingleton.getInstance().categoryVendorWishListViewDataModel.get(position).id;
                trig.initTrigger(true,vId,position);
            }
        });
    }

    public void removeWishList(int wishListPosition){
        CategorySingleton.getInstance().categoryVendorWishListViewDataModel.remove(wishListPosition);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CategorySingleton.getInstance().categoryVendorWishListViewDataModel.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView vName, vRemove,txt_booking_history_price;
        private ImageView img_vendor;
        private Button btnBookService, btnUnshortistService;

        public ViewHolder(View view) {
            super(view);
            vName = (TextView) view.findViewById(R.id.txt_custom_wish_list_company_name);
            txt_booking_history_price=(TextView)view.findViewById(R.id.txt_booking_history_price);
            img_vendor = (ImageView) view.findViewById(R.id.img_custom_wish_list);
            btnUnshortistService = (Button) view.findViewById(R.id.btn_unshortlist_service);
            btnBookService = (Button) view.findViewById(R.id.btn_book_service);


        }


    }
}
