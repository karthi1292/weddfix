package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 3/6/2017.
 */

public class CategoryVendorServicestResponseElementModel {
   @SerializedName("vendorServices")
    public CategoryVendorServicesListModel categoryVendorServicesListModel;
}


