package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 18-02-2017.
 */

public class CategoryUserOTPVerifiedProfileModel {

    @SerializedName("status")
    public String status;
    @SerializedName("mtny_message")
    public String message;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("verifiedMobileNumber")
    public String verifiedMobileNumber;
}
