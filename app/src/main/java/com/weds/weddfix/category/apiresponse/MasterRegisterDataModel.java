package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;
import com.weds.weddfix.category.apiresponse.CategoryRegisterCountryDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class MasterRegisterDataModel  {


    @SerializedName("country")
    public List<CategoryRegisterCountryDataModel> countryDataModelList = new ArrayList<>();


    public MasterRegisterDataModel() {

    }

    public List<CategoryRegisterCountryDataModel> getCountryDataModelList() {
        return countryDataModelList;
    }

    public void setCountryDataModelList(List<CategoryRegisterCountryDataModel> countryDataModelList) {
        this.countryDataModelList = countryDataModelList;
    }

}
