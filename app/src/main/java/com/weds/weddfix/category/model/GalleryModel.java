package com.weds.weddfix.category.model;

/**
 * Created by Acer on 03-11-2016.
 */
public class GalleryModel {
    private int image;

    public GalleryModel(int image) {

        this.image = image;
    }

    public int getImage() {
        return image;
    }
}
