package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 07-03-2017.
 */

public class CategoryProfileUpdateDataModel {

    @SerializedName("status")
    public String status;
    @SerializedName("mtny_message")
    public String message;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("verifyMobileNumber")
    public String verifyMobileNumber;
    @SerializedName("verifyedMobileNumber")
    public String verifyedMobileNumber;


}
