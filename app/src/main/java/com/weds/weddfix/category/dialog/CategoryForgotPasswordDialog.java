package com.weds.weddfix.category.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.weds.weddfix.R;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.interfaces.CategoryTriggerWithString;

/**
 * Created by KarthickRaja on 2/19/2017.
 */

public class CategoryForgotPasswordDialog extends Dialog implements View.OnClickListener {
    private EditText emailIdEditText;
    private Context mContext;
    private Dialog dialog;
    private Button btnSubmit;
    private CategoryTriggerWithString trigger;
    private String message;

    public CategoryForgotPasswordDialog(Context context, String msg, CategoryTriggerWithString trig) {
        super(context);
        // TODO Auto-generated constructor stub
        mContext = context;
        trigger = trig;
        message = msg;
    }

    public void showDialog() {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams WMLP = dialog.getWindow().getAttributes();
        WMLP.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(WMLP);
      /*  dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));*/

       // dialog.setCancelable(false);
      //  dialog.setCanceledOnTouchOutside(false);

        dialog.setContentView(R.layout.dialog_forgot_pwd_alert);
        btnSubmit = (Button) dialog.findViewById(R.id.dialog_submit_btn);
        emailIdEditText = (EditText) dialog.findViewById(R.id.edittext_mail_id);

        btnSubmit.setOnClickListener(this);
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.dialog_submit_btn:
                if(emailIdEditText.length()>0){
                    trigger.initTrigger(true,emailIdEditText.getText().toString());

                }else{
                    CategoryCommonMethods.showToast("Please Enter your regsitered mailId",mContext);
                }
                break;
        }
    }

    public void dismiss(){
        if(dialog!=null){
            dialog.dismiss();
        }
    }
}
