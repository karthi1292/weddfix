package com.weds.weddfix.category.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponsePasswordChangeElementModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;

import retrofit2.Call;
import retrofit2.Callback;

import static com.weds.weddfix.R.id.ed_old_pass;


public class PasswordChangeActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private EditText editOldPassword, editNewPassword, editConfirmPassword;
    private Button btnSave;
    private String oldPassword, newPassword, confirmPassword;
    private CategoryActivityIndicator pDialog;
    private Context mContext;
    private CategoryRetrofitApiInterface apiService;
    private CategoryStorePreference categoryStorePreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        intWidgets();
        mContext = this;
        categoryStorePreference = new CategoryStorePreference(mContext);
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    public void intWidgets() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        editOldPassword = (EditText) findViewById(ed_old_pass);
        editNewPassword = (EditText) findViewById(R.id.ed_new_pass);
        editConfirmPassword = (EditText) findViewById(R.id.ed_confirm_pass);
        btnSave = (Button) findViewById(R.id.btn_change_pwd_save);
        btnSave.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_change_pwd_save:

                oldPassword = editOldPassword.getText().toString();
                if (oldPassword.trim().equals("")) {
                    Toast.makeText(getBaseContext(), "Please Enter Old Password",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                newPassword = editNewPassword.getText().toString();
                if (newPassword.trim().equals("")) {
                    Toast.makeText(getBaseContext(), "Please Enter New Password",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                confirmPassword = editConfirmPassword.getText().toString();
                if (confirmPassword.trim().equals("")) {
                    Toast.makeText(getBaseContext(), "Please Enter Confirm Password",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                if (newPassword.equals(confirmPassword)) {
                    passwordChange();
                } else {
                    Toast.makeText(getBaseContext(), "Password is Mismatch",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                break;

        }

    }


    private void passwordChange() {

        pDialog = new CategoryActivityIndicator(this);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponsePasswordChangeElementModel> call = apiService.getPasswordChange(categoryStorePreference.getStringValue(CategoryConstants.USER_ID), confirmPassword);
        call.enqueue(new Callback<CategoryCommonResponsePasswordChangeElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponsePasswordChangeElementModel> call, retrofit2.Response<CategoryCommonResponsePasswordChangeElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().categoryPasswordChangeDataModel.status;
                    String message = response.body().categoryPasswordChangeDataModel.message;


                    if (status.equals(CategoryConstants.SUCCESS)) {
                        Intent homeIntent = new Intent(PasswordChangeActivity.this, HomeActivity.class);
                        startActivity(homeIntent);
                        finish();
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CategoryCommonResponsePasswordChangeElementModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());

            }
        });
    }
}
