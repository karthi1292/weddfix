package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 03-03-2017.
 */

public class CategoryVendorDetailsDataModel {

    @SerializedName("id")
    public String id;
    @SerializedName("categoryName")
    public String categoryName;
    @SerializedName("companyName")
    public String companyName;
    @SerializedName("address")
    public String address;
    @SerializedName("location")
    public String location;
    @SerializedName("cityName")
    public String cityName;
    @SerializedName("stateName")
    public String stateName;
    @SerializedName("countryName")
    public String countryName;
    @SerializedName("pincode")
    public String pincode;
    @SerializedName("price")
    public String price;
    @SerializedName("maxRating")
    public String maxRating;
    @SerializedName("maxUsersRating")
    public String maxUsersRating;
    @SerializedName("minRating")
    public String minRating;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("aboutUs")
    public String aboutUs;
    @SerializedName("mapUrl")
    public String mapUrl;
    @SerializedName("vendorFullName")
    public String vendorFullName;
    @SerializedName("vendorCityName")
    public String vendorCityName;
    @SerializedName("vendorStateName")
    public String vendorStateName;
    @SerializedName("vendorCountryName")
    public String vendorCountryName;
    @SerializedName("vendorPincode")
    public String vendorPincode;
    @SerializedName("vendorWebsiteUrl")
    public String vendorWebsiteUrl;
    @SerializedName("vendorFacebookUrl")
    public String vendorFacebookUrl;
    @SerializedName("vendorTwitterUrl")
    public String vendorTwitterUrl;
    @SerializedName("vendorLinkedinUrl")
    public String vendorLinkedinUrl;
    @SerializedName("vendorPinterestUrl")
    public String vendorPinterestUrl;
    @SerializedName("vendorInstagramUrl")
    public String vendorInstagramUrl;
    @SerializedName("vendorProfilePicture")
    public String vendorProfilePicture;
    @SerializedName("vendorContactName")
    public String vendorContactName;
    @SerializedName("vendorMobile")
    public String vendorMobile;
    @SerializedName("vendorPhone")
    public String vendorPhone;
    @SerializedName("vendorEmail")
    public String vendorEmail;
    @SerializedName("vendorVideoUrl")
    public String vendorVideoUrl;


}
