package com.weds.weddfix.category.activity;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.category.adapter.BookingHistoryAdapter;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseOrderHistoryElementModel;
import com.weds.weddfix.category.apiresponse.CategoryOrderHistoryDataModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BookingHistoryActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private RecyclerView recyclerOrderHistory;
    private Context mContext;
    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private CategoryStorePreference categoryStorePreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_history);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        mContext = this;
        categoryStorePreference = new CategoryStorePreference(mContext);
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
        recyclerOrderHistory= (RecyclerView) findViewById(R.id.recycler_order_history);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
            loadBookingHistory();
        } else {
            CategoryCommonMethods.showToast(CategoryConstants.TOAST_CONNECTION_ERROR, mContext);
        }


    }

    private void loadBookingHistory() {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseOrderHistoryElementModel> call = apiService.getOrderHistory(categoryStorePreference.getStringValue(CategoryConstants.USER_ID));
        call.enqueue(new Callback<CategoryCommonResponseOrderHistoryElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseOrderHistoryElementModel> call, Response<CategoryCommonResponseOrderHistoryElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {


                    ArrayList<CategoryOrderHistoryDataModel> orderHistoryDataModels = response.body().categoryOrderHistoryDataModels;

                    if (orderHistoryDataModels.size() > 0) {

                        CategorySingleton.getInstance().categoryOrderHistoryDataModels = orderHistoryDataModels;

                        BookingHistoryAdapter bookingHistoryAdapter = new BookingHistoryAdapter(mContext);
                        LinearLayoutManager categoryLinearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerOrderHistory.setLayoutManager(categoryLinearLayoutManager);
                        recyclerOrderHistory.setAdapter(bookingHistoryAdapter);
                    }
                    if (pDialog != null)
                        pDialog.dismiss();


                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<CategoryCommonResponseOrderHistoryElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });


    }


}
