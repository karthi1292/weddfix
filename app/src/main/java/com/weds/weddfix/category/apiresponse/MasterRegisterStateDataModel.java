package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class MasterRegisterStateDataModel {


    @SerializedName("state")
    public List<CategoryRegisterStateDataModel> catRegsiterStateDataModelList =new ArrayList<>();

    public MasterRegisterStateDataModel() {
    }

    public List<CategoryRegisterStateDataModel> getCatRegsiterStateDataModelList() {
        return catRegsiterStateDataModelList;
    }

    public void setMasterRegisterStateDataModel(List<CategoryRegisterStateDataModel> RegsiterStateDataModelList) {
        this.catRegsiterStateDataModelList = RegsiterStateDataModelList;
    }



}
