package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by KarthickRaja on 3/6/2017.
 */

public class CategoryHomeDataModel {
    @SerializedName("category")
    public ArrayList<CategoryNameDataModel> categoryNameDataModel;
    @SerializedName("city")
    public ArrayList<CategoryCityNameDataModel> categoryCityDataModel =new ArrayList();
    @SerializedName("bannerPhotos")
    public ArrayList<CategoryBannerPhotoDataModel> categoryBannerPhotoDataModel;

}
