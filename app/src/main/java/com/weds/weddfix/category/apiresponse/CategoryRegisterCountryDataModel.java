package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class CategoryRegisterCountryDataModel  {
    @SerializedName("id")
    public String id;
    @SerializedName("countryName")
    public String countryName;

    public CategoryRegisterCountryDataModel() {
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
