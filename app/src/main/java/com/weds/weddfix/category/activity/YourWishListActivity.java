package com.weds.weddfix.category.activity;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.category.adapter.YourWishListAdapter;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorWishListUnShortElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorWishListViewElementModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorWishListUnShortDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorWishListViewDataModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;
import com.weds.weddfix.category.interfaces.CategoryTriggerWithTwoString;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class YourWishListActivity extends AppCompatActivity implements CategoryTriggerWithTwoString {
    private Toolbar toolbar;
    private RecyclerView recyclerWishList;
    private Context mContext;
    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private CategoryStorePreference categoryStorePreference;
    private String unShortListVendorId;
    private YourWishListAdapter yourWishListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_wish_list);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        initWidgets();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
            getAllWishList();
        } else {
            CategoryCommonMethods.showToast(CategoryConstants.TOAST_CONNECTION_ERROR, mContext);
        }

    }

    private void initWidgets() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerWishList = (RecyclerView) findViewById(R.id.recycler_wish_list);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = this;
        categoryStorePreference = new CategoryStorePreference(mContext);
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
    }

    private void getAllWishList() {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseVendorWishListViewElementModel> call = apiService.getWishListView(categoryStorePreference.getStringValue(CategoryConstants.USER_ID));
        call.enqueue(new Callback<CategoryCommonResponseVendorWishListViewElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseVendorWishListViewElementModel> call, Response<CategoryCommonResponseVendorWishListViewElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {


                    ArrayList<CategoryVendorWishListViewDataModel> categoryVendorWishListViewDataModels = response.body().categoryVendorWishListViewDataModel;

                    if (categoryVendorWishListViewDataModels.size() > 0) {

                        CategorySingleton.getInstance().categoryVendorWishListViewDataModel = categoryVendorWishListViewDataModels;


                        yourWishListAdapter = new YourWishListAdapter(mContext);
                        LinearLayoutManager categoryLinearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerWishList.setLayoutManager(categoryLinearLayoutManager);
                        recyclerWishList.setAdapter(yourWishListAdapter);
                    }
                    if (pDialog != null)
                        pDialog.dismiss();


                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<CategoryCommonResponseVendorWishListViewElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    private void unLikeWishlist(String shortListVendorId) {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseVendorWishListUnShortElementModel> call = apiService.getWishListUnShortList(shortListVendorId);
        call.enqueue(new Callback<CategoryCommonResponseVendorWishListUnShortElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseVendorWishListUnShortElementModel> call, Response<CategoryCommonResponseVendorWishListUnShortElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    String status = response.body().categoryVendorWishListUnShortDataModel.status;
                    String message = response.body().categoryVendorWishListUnShortDataModel.message;
                    if (status.equals(CategoryConstants.SUCCESS)) {

                        CategoryVendorWishListUnShortDataModel categoryVendorWishListUnShortDataModel = response.body().categoryVendorWishListUnShortDataModel;
                        unShortListVendorId = categoryVendorWishListUnShortDataModel.shortlistedVendorId;
                        String shortListId = categoryVendorWishListUnShortDataModel.shortlistedId;
                        categoryVendorWishListUnShortDataModel.shortlistedVendorId = unShortListVendorId;
                        categoryVendorWishListUnShortDataModel.shortlistedId = shortListId;

                        if (pDialog != null)
                            pDialog.dismiss();

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<CategoryCommonResponseVendorWishListUnShortElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }


    @Override
    public void initTrigger(boolean Trigger, String shortListedVendorId, int position) {
        if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
            unLikeWishlist(shortListedVendorId);
            yourWishListAdapter.removeWishList(position);
        } else {
            CategoryCommonMethods.showToast(CategoryConstants.TOAST_CONNECTION_ERROR, mContext);
        }
    }
}
