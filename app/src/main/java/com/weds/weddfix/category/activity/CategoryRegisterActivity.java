package com.weds.weddfix.category.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.category.adapter.CatgCitySpinnerAdapter;
import com.weds.weddfix.category.adapter.CatgCountrySpinnerAdapter;
import com.weds.weddfix.category.adapter.CatgStateSpinnerAdapter;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseElementModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;
import com.weds.weddfix.category.apiresponse.CategoryRegisterCityDataModel;
import com.weds.weddfix.category.apiresponse.CategoryRegisterCountryDataModel;
import com.weds.weddfix.category.apiresponse.CategoryRegisterStateDataModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterCityDataModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterCountryDataModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterStateDataModel;

import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategorySingleton;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryRegisterActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private Button signUpButton;
    private EditText edUserName, edUserEmail, edUserPassword, edUserMobileNumber, edUserPincode, edUserWeddDate;
    private String uName, uEmail, uPassword, uMobile, uPincode, uWeddingDate;
    private CategoryRetrofitApiInterface apiService;
    private TextView txtLogin;
    private CatgCitySpinnerAdapter catgCitySpinnerAdapter;
    private CatgStateSpinnerAdapter catgStateSpinnerAdapter;
    private CatgCountrySpinnerAdapter catgCountrySpinnerAdapter;
    private CheckBox checkBox;
    private CategoryActivityIndicator pDialog;
    private Toolbar toolbar;
    private CategoryStorePreference categoryStorePreference;
    private Context mContext;
    private String cityId, countryId, stateId;
    private ImageButton weddCalander;
    private int day, month, year;
    private Calendar calendar;
    private AppCompatSpinner citySpinner, countrySpinner, stateSpinner;
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            edUserWeddDate.setText(selectedDay + " / " + (selectedMonth + 1) + " / "
                    + selectedYear);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_register);

        mContext = this;

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        initWidgets();

        if (CategoryCommonMethods.isNetworkConnectionAvailable(this)) {
            getAllMasterRegsiterCountryData();
        } else {
            CategoryCommonMethods.showToast(this, CategoryConstants.TOAST_CONNECTION_ERROR);
        }

        categoryStorePreference = new CategoryStorePreference(mContext);


        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);
    }

    private void initWidgets() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        signUpButton = (Button) findViewById(R.id.btn_category_reg_singn_up);
        edUserName = (EditText) findViewById(R.id.ed_user_reg_name);
        edUserEmail = (EditText) findViewById(R.id.ed_user_reg_email);
        edUserMobileNumber = (EditText) findViewById(R.id.ed_user_reg_mobile_no);
        edUserPassword = (EditText) findViewById(R.id.ed_user_reg_password);
        txtLogin = (TextView) findViewById(R.id.txt_category_reg_login);
        edUserPincode = (EditText) findViewById(R.id.ed_user_reg_pincode);
        edUserWeddDate = (EditText) findViewById(R.id.ed_user_reg_date);
        checkBox = (CheckBox) findViewById(R.id.user_reg_checkbox);
        countrySpinner = (AppCompatSpinner) findViewById(R.id.spinner_category_reg_country);
        stateSpinner = (AppCompatSpinner) findViewById(R.id.spinner_category_reg_state);
        citySpinner = (AppCompatSpinner) findViewById(R.id.spinner_category_reg_city);
        weddCalander = (ImageButton) findViewById(R.id.image_button_wedd_cal);
        signUpButton.setOnClickListener(this);
        txtLogin.setOnClickListener(this);
        countrySpinner.setOnItemSelectedListener(this);
        stateSpinner.setOnItemSelectedListener(this);
        citySpinner.setOnItemSelectedListener(this);
        weddCalander.setOnClickListener(this);
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }

    public void sendRegistrationBioData() {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseElementModel> call = apiService.registerCategoryData(uWeddingDate, uName, uEmail, uPassword, uMobile, cityId, stateId, countryId, uPincode);
        call.enqueue(new Callback<CategoryCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseElementModel> call, Response<CategoryCommonResponseElementModel> response) {

                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().categoryCommonResponseModel.status;
                    String message = response.body().categoryCommonResponseModel.message;
                    String userId=response.body().categoryCommonResponseModel.userId;
                    if (status.equals(CategoryConstants.SUCCESS)) {

                       /* categoryStorePreference.setString(CategoryConstants.U_PINCODE, uPincode);
                        categoryStorePreference.setString(CategoryConstants.U_STATE_ID, stateId);
                        categoryStorePreference.setString(CategoryConstants.U_CITY_ID, cityId);
                        categoryStorePreference.setString(CategoryConstants.U_COUNTRY_ID, countryId);
                        categoryStorePreference.setString(CategoryConstants.U_WEDDING_DATE,uWeddingDate);*/
                        categoryStorePreference.setString(CategoryConstants.USER_ID, userId);
                        categoryStorePreference.setString(CategoryConstants.U_PHONE, uMobile);

                        Intent homeIntent = new Intent(CategoryRegisterActivity.this, UserAuthenticationActivity.class);
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(homeIntent);

                        if (pDialog != null)
                            pDialog.dismiss();
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override

            public void onFailure(Call<CategoryCommonResponseElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
            }
        });
    }

    public void getAllRegisterMasterStateData(String countryId) {

        Call<MasterRegisterStateDataModel> call = apiService.getAllCateStateMasterRegisterData(countryId);
        call.enqueue(new Callback<MasterRegisterStateDataModel>() {
            @Override
            public void onResponse(Call<MasterRegisterStateDataModel> call, Response<MasterRegisterStateDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MasterRegisterStateDataModel catgMasterStateDataModel = new MasterRegisterStateDataModel();

                    List<CategoryRegisterStateDataModel> stateDataModelList = response.body().catRegsiterStateDataModelList;

                    CategoryRegisterStateDataModel catgRegsiterStateDataModel = new CategoryRegisterStateDataModel();
                    catgRegsiterStateDataModel.id = "-1";
                    catgRegsiterStateDataModel.stateName = "State";

                    catgMasterStateDataModel.catRegsiterStateDataModelList.add(catgRegsiterStateDataModel);
                    catgMasterStateDataModel.catRegsiterStateDataModelList.addAll(stateDataModelList);

                    CategorySingleton.getInstance().catgMasterStateDataModel = catgMasterStateDataModel;

                    catgStateSpinnerAdapter = new CatgStateSpinnerAdapter(mContext, CategorySingleton.getInstance().catgMasterStateDataModel.catRegsiterStateDataModelList);
                    stateSpinner.setAdapter(catgStateSpinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<MasterRegisterStateDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }


    public void getAllMasterRegsiterCityData(String stateId) {

        Call<MasterRegisterCityDataModel> call = apiService.getAllCateCityMasterRegisterData(stateId);
        call.enqueue(new Callback<MasterRegisterCityDataModel>() {
            @Override
            public void onResponse(Call<MasterRegisterCityDataModel> call, Response<MasterRegisterCityDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    MasterRegisterCityDataModel MasterCityDataModel = new MasterRegisterCityDataModel();
                    List<CategoryRegisterCityDataModel> cityDataModelList = response.body().categRegisterCityDataModelList;

                    CategoryRegisterCityDataModel RegisterCityDataModel = new CategoryRegisterCityDataModel();
                    RegisterCityDataModel.id = "-1";
                    RegisterCityDataModel.cityName = "City";

                    MasterCityDataModel.categRegisterCityDataModelList.add(RegisterCityDataModel);
                    MasterCityDataModel.categRegisterCityDataModelList.addAll(cityDataModelList);

                    CategorySingleton.getInstance().catgMasterCityDataModel = MasterCityDataModel;

                    catgCitySpinnerAdapter = new CatgCitySpinnerAdapter(mContext, CategorySingleton.getInstance().catgMasterCityDataModel.categRegisterCityDataModelList);
                    citySpinner.setAdapter(catgCitySpinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<MasterRegisterCityDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }


    public void getAllMasterRegsiterCountryData() {
        Call<MasterRegisterCountryDataModel> call = apiService.getAllCateCountryMasterRegisterData();
        call.enqueue(new Callback<MasterRegisterCountryDataModel>() {
            @Override
            public void onResponse(Call<MasterRegisterCountryDataModel> call, Response<MasterRegisterCountryDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    MasterRegisterCountryDataModel MasterCountryDataModel = new MasterRegisterCountryDataModel();
                    List<CategoryRegisterCountryDataModel> countryDataModelList = response.body().categRegisterCountryDataModelList;

                    CategoryRegisterCountryDataModel RegisterCountryDataModel = new CategoryRegisterCountryDataModel();
                    RegisterCountryDataModel.id = "-1";
                    RegisterCountryDataModel.countryName = "Country";

                    MasterCountryDataModel.categRegisterCountryDataModelList.add(RegisterCountryDataModel);
                    MasterCountryDataModel.categRegisterCountryDataModelList.addAll(countryDataModelList);

                    CategorySingleton.getInstance().catgMasterCountryDataModel = MasterCountryDataModel;
                    if (pDialog != null)
                        pDialog.dismiss();

                    catgCountrySpinnerAdapter = new CatgCountrySpinnerAdapter(mContext, CategorySingleton.getInstance().catgMasterCountryDataModel.categRegisterCountryDataModelList);
                    countrySpinner.setAdapter(catgCountrySpinnerAdapter);

                }
            }

            @Override
            public void onFailure(Call<MasterRegisterCountryDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_category_reg_singn_up:
                if (CategoryCommonMethods.isNetworkConnectionAvailable(this)) {
                    if (edUserName.getText().toString().isEmpty()) {

                        Toast.makeText(CategoryRegisterActivity.this, "Enter the Name", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edUserEmail.getText().toString().isEmpty()) {
                        Toast.makeText(CategoryRegisterActivity.this, "Enter the Email", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edUserMobileNumber.getText().toString().isEmpty()) {
                        Toast.makeText(CategoryRegisterActivity.this, "Enter the Mobile Number", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edUserPassword.getText().toString().isEmpty()) {
                        Toast.makeText(CategoryRegisterActivity.this, "Enter the Password", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (edUserPincode.getText().toString().isEmpty()) {
                        Toast.makeText(CategoryRegisterActivity.this, "Enter the Pincode", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edUserWeddDate.getText().toString().isEmpty()) {
                        Toast.makeText(CategoryRegisterActivity.this, "Enter the Wedding Date", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (checkBox.isChecked()) {

                    } else {
                        Toast.makeText(CategoryRegisterActivity.this, "Please the check the checkbox", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (countryId != null && stateId != null && cityId != null) {
                        if (!countryId.equals("-1") && !stateId.equals("-1") && !cityId.equals("-1")) {

                            uName = edUserName.getText().toString();
                            uEmail = edUserEmail.getText().toString();
                            uMobile = edUserMobileNumber.getText().toString();
                            uPassword = edUserPassword.getText().toString();
                            uPincode = edUserPincode.getText().toString();
                            uWeddingDate = edUserWeddDate.getText().toString();

                            sendRegistrationBioData();

                        } else {
                            Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                    }
                } else {
                    CategoryCommonMethods.showToast(this, "Please check the internet connection");
                }

                break;
            case R.id.txt_category_reg_login:
                Intent intentLogin = new Intent(CategoryRegisterActivity.this, LoginActivity.class);
                startActivity(intentLogin);
                break;

            case R.id.image_button_wedd_cal:
                showDialog(0);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        int viewId = parent.getId();
        switch (viewId) {
            case R.id.spinner_category_reg_city:
                cityId = catgCitySpinnerAdapter.getCityId(position);
                break;
            case R.id.spinner_category_reg_state:
                stateId = catgStateSpinnerAdapter.getStateId(position);
                getAllMasterRegsiterCityData(stateId);
                break;
            case R.id.spinner_category_reg_country:
                countryId = catgCountrySpinnerAdapter.getCountryId(position);
                getAllRegisterMasterStateData(countryId);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}