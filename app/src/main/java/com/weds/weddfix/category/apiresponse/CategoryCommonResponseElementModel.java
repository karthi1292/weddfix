package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;
import com.weds.weddfix.matrimony.apiresponse.MtnyCommonResponseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rameshmuthu on 17-02-2017.
 */

public class CategoryCommonResponseElementModel {
    @SerializedName("register")
    public CategoryCommonResponseModel categoryCommonResponseModel;
    @SerializedName("forgotPassword")
    public CategoryCommonResponseModel categoryForgotPwdCommonResponseModel;
    @SerializedName("login")
    public CategoryUserProfileDataModel categoryUserProfileDataModel;
    @SerializedName("loginDetails")
    public CategoryUserLoginDetailsDataModel categoryUserLoginDetailsDataModel;
    @SerializedName("verification")
    public CategoryUserOTPProfileModel categoryUserOTPProfileModel;



}
