package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 03-03-2017.
 */

public class CategoryVendorDetailsPhotoGalleryDataModel {
    @SerializedName("id")
    public String id;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("orgId")
    public String orgId;
    @SerializedName("photoType")
    public String photoType;


}
