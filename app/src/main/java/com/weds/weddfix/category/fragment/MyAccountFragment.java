package com.weds.weddfix.category.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.activity.BookingHistoryActivity;
import com.weds.weddfix.category.activity.PasswordChangeActivity;
import com.weds.weddfix.category.activity.ProfileEditActivity;
import com.weds.weddfix.category.activity.YourWishListActivity;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategoryStorePreference;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Acer on 26-10-2016.
 */
public class MyAccountFragment extends Fragment implements View.OnClickListener {

    private CircleImageView profileImage;
    private TextView txtProfileName, socialLink;
    private ImageView imgProfile, imgWishList, imgOrderHistory, inbox, imgChangePwd, sociallinks;
    private CategoryStorePreference categoryStorePreference;
    private Context mContext;
    private CategoryActivityIndicator pDialog;
    private View view;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_my_account, container, false);

        intWidgets();

        loadProfilePic();

        return view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_profile:

                startActivity(new Intent(getActivity().getApplicationContext(), ProfileEditActivity.class));

                break;
            case R.id.img_wish_list:
                startActivity(new Intent(getActivity().getApplicationContext(), YourWishListActivity.class));

                break;
            case R.id.img_order_history:

                startActivity(new Intent(getActivity().getApplicationContext(), BookingHistoryActivity.class));

                break;

            case R.id.img_change_pwd:
                startActivity(new Intent(getActivity().getApplicationContext(), PasswordChangeActivity.class));

                break;
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void intWidgets() {

        txtProfileName = (TextView) view.findViewById(R.id.txt_my_profile_name);
        imgProfile = (ImageView) view.findViewById(R.id.img_profile);
        imgWishList = (ImageView) view.findViewById(R.id.img_wish_list);
        imgOrderHistory = (ImageView) view.findViewById(R.id.img_order_history);
        imgChangePwd = (ImageView) view.findViewById(R.id.img_change_pwd);
        profileImage = (CircleImageView) view.findViewById(R.id.service_map_img);

        categoryStorePreference = new CategoryStorePreference(mContext);

        imgProfile.setOnClickListener(this);
        imgWishList.setOnClickListener(this);
        imgOrderHistory.setOnClickListener(this);
        imgChangePwd.setOnClickListener(this);

        Glide.with(MyAccountFragment.this).load(R.drawable.banner).into((ImageView) view.findViewById(R.id.img_profile_banner));
       /* Glide.with(MyAccountFragment.this).load(R.drawable.wishlist).into((ImageView) view.findViewById(R.id.img_wish_list));
        Glide.with(MyAccountFragment.this).load(R.drawable.cart_one).into((ImageView) view.findViewById(R.id.img_order_history));
        Glide.with(MyAccountFragment.this).load(R.drawable.men).into((ImageView) view.findViewById(R.id.img_profile));
        Glide.with(MyAccountFragment.this).load(R.drawable.pwdchange).into((ImageView) view.findViewById(R.id.img_change_pwd));*/
    }

    private void loadProfilePic() {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();
        Glide.with(getActivity()).load(categoryStorePreference.getStringValue(CategoryConstants.U_PRO_PIC)).placeholder(R.drawable.profile_placeholder).into(profileImage);
        txtProfileName.setText(categoryStorePreference.getStringValue(CategoryConstants.U_FNAME));
        if (pDialog != null)
            pDialog.dismiss();
    }

}
