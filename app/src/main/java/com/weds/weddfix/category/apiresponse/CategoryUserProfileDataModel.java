package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;
import com.weds.weddfix.matrimony.apiresponse.MtnyMyPartnerPreferenceModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMyProfilePicModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDetails;

import java.util.ArrayList;

/**
 * Created by rameshmuthu on 16-02-2017.
 */

public class CategoryUserProfileDataModel {
    @SerializedName("baseUrl")
    public String baseUrl;
    @SerializedName("status")
    public String status;
    @SerializedName("mtny_message")
    public String message;


}
