package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryBannerPhotoDataModel;

import java.util.ArrayList;


/**
 * Created by Acer on 01-08-2016.
 */
public class SlidingImageAdapter extends PagerAdapter {

    private ArrayList<CategoryBannerPhotoDataModel> bannerImagesList;
    private LayoutInflater inflater;
    private Context mContext;


    public SlidingImageAdapter(Context context, ArrayList<CategoryBannerPhotoDataModel> bannerPhotosList) {
        this.mContext = context;
        this.bannerImagesList = bannerPhotosList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return bannerImagesList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {

        View slideImageLayout = inflater.inflate(R.layout.adapter_sliding_images_layout, view, false);

        ImageView slideImageView = (ImageView) slideImageLayout.findViewById(R.id.slide_imageview);

        Glide.with(mContext).load(bannerImagesList.get(position).fileName).placeholder(R.drawable.wed_logo).into(slideImageView);

        view.addView(slideImageLayout, 0);

        return slideImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}

