package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 04-03-2017.
 */

public class CategoryCommonResponseVendorWishListUnShortElementModel {

    @SerializedName("unShortlist")
    public CategoryVendorWishListUnShortDataModel categoryVendorWishListUnShortDataModel;


}
