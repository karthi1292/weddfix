package com.weds.weddfix.category.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.weds.weddfix.R;
import com.weds.weddfix.category.interfaces.CategoryTriggerWithObject;
import com.weds.weddfix.category.model.CallbackBookModel;

/**
 * Created by KarthickRaja on 3/23/2017.
 */

public class BookingDialog extends Dialog implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private Context mContext;
    private Dialog dialog;
    private CategoryTriggerWithObject trigger;
    private CheckBox sendEmail, needCallBack;
    private String email, callBack;
    private Button btnOk,btnCancel;


    public BookingDialog(Context context, CategoryTriggerWithObject trig) {
        super(context);
        mContext = context;
        trigger = trig;
    }
    public void showDialog() {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams WMLP = dialog.getWindow().getAttributes();
        WMLP.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(WMLP);

        dialog.setContentView(R.layout.dialog_booking);

        sendEmail = (CheckBox) dialog.findViewById(R.id.chk_email);
        needCallBack = (CheckBox) dialog.findViewById(R.id.chk_callback);
        btnOk=(Button)dialog.findViewById(R.id.btn_ok);
        btnCancel=(Button)dialog.findViewById(R.id.btn_cancel);


        sendEmail.setOnCheckedChangeListener(this);
        needCallBack.setOnCheckedChangeListener(this);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);


        dialog.show();
    }

    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton view, boolean isChecked) {
        switch (view.getId()) {
            case R.id.chk_email:
                if (isChecked)
                    email = "true";
                else
                    email = "false";
                break;

            case R.id.chk_callback:
                if (isChecked)
                    callBack = "true";
                else
                    callBack = "false";
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                CallbackBookModel callbackBookModel=new CallbackBookModel();
                callbackBookModel.sendEmail=email;
                callbackBookModel.needCallback=callBack;
                trigger.initTrigger(true,callbackBookModel);
                dialog.dismiss();
                break;
            case R.id.btn_cancel:
                dialog.dismiss();
                break;

        }
    }
}
