package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryVendorDetailsReviewsDataModel;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.model.Review;

import java.util.List;



public class CustomerReviewAdapter extends RecyclerView.Adapter<CustomerReviewAdapter.ViewHolder> {

    private Context context;

    public CustomerReviewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.customer_review, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        CategoryVendorDetailsReviewsDataModel categoryVendorDetailsReviewsDataModel=CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsReviewsDataModel.get(position);

        holder.reviewDetail.setText(categoryVendorDetailsReviewsDataModel.review);
        holder.customerNameAndDate.setText(categoryVendorDetailsReviewsDataModel.name + " " +categoryVendorDetailsReviewsDataModel.createdDate);
        int rating = Integer.parseInt(categoryVendorDetailsReviewsDataModel.maxRating);

       /* for(int i=0;i<rating;i++){
            ImageView ratingImg=new ImageView(context);
          holder.lloutRating.addView((ratingImg));
            ratingImg.setImageDrawable(context.getResources().getDrawable(R.drawable.star));
        }*/

        Glide.with(context).load(categoryVendorDetailsReviewsDataModel.fileName).into(holder.imgCustomer);

    }

    @Override
    public int getItemCount() {
        return CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsReviewsDataModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgCustomer,imgRating;
        private TextView reviewDetail,customerNameAndDate;
        private LinearLayout lloutRating;

        public ViewHolder(View view) {
            super(view);

            reviewDetail = (TextView) view.findViewById(R.id.txt_customer_review_detail);
            customerNameAndDate=(TextView)view.findViewById(R.id.txt_customer_name_date);
            imgCustomer =(ImageView)view.findViewById(R.id.customer_img);
            //lloutRating =(LinearLayout)view.findViewById(R.id.llout_rating);
            imgRating=(ImageView)view.findViewById(R.id.img_rating);
        }
    }
}
