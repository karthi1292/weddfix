package com.weds.weddfix.category.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weds.weddfix.R;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;
import com.weds.weddfix.category.adapter.VendorImageListAdapter;
import com.weds.weddfix.category.adapter.VendorVideoListAdapter;
import com.weds.weddfix.category.model.Image;

import java.util.ArrayList;


/**
 * Created by Acer on 22-06-2016.
 */
public class CategoryGalleryFragment extends Fragment {

    private LinearLayoutManager imageLlManager, videoLlManager;
    //private TextView imageViewAll, videosImageAll;
    private ArrayList<Image> images;
    private VendorImageListAdapter vendorImageListAdapter;
    private RecyclerView recyclerViewVideoList, recyclerViewImageList;
    private String id;
    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private CategoryStorePreference categoryStorePreference;
    private String userId;
    private FragmentManager fragmentManager;
    private Context mContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_gallery, container, false);

        recyclerViewVideoList = (RecyclerView) view.findViewById(R.id.category_videos_recycler_view);
        recyclerViewImageList = (RecyclerView) view.findViewById(R.id.category_images_recycler_view);
        //imageViewAll = (TextView) view.findViewById(R.id.images_view_all);
      //  videosImageAll = (TextView) view.findViewById(R.id.videos_view_all);

        videoLlManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        imageLlManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

       /* imageViewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                    startActivity(intent);
                }
        });*/

        loadVendorImagesAndVideos();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
        categoryStorePreference = new CategoryStorePreference(mContext);
        userId = categoryStorePreference.getStringValue(CategoryConstants.USER_ID);

        fragmentManager = getActivity().getSupportFragmentManager();
    }


    private void loadVendorImagesAndVideos() {

        vendorImageListAdapter = new VendorImageListAdapter(mContext);
        recyclerViewImageList.setLayoutManager(imageLlManager);
        recyclerViewImageList.setAdapter(vendorImageListAdapter);

        VendorVideoListAdapter videoListAdapter = new VendorVideoListAdapter(mContext);
        recyclerViewVideoList.setLayoutManager(videoLlManager);
        recyclerViewVideoList.setAdapter(videoListAdapter);

       /* recyclerViewImageList.addOnItemTouchListener(new VendorImageListAdapter.RecyclerTouchListener(getActivity(), recyclerViewVideoList, new VendorImageListAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = new SlideshowDialogFragment();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/
    }
}
