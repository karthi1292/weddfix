package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.weds.weddfix.R;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.fragment.SlideshowDialogFragment;

/**
 * Created by Acer on 03-11-2016.
 */
public class VendorImageListAdapter extends RecyclerView.Adapter<VendorImageListAdapter.MyViewHolder> {

    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView vendorCusPhotos;

        public MyViewHolder(View view) {
            super(view);
            vendorCusPhotos = (ImageView) view.findViewById(R.id.vendor_cus_img);
        }
    }

    public VendorImageListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_vendor_customer_photos, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        String vendorImageUrl = CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsPhotoGalleryDataModel.get(position).fileName;
        Glide.with(mContext).load(vendorImageUrl)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.wed_logo).into(holder.vendorCusPhotos);


        holder.vendorCusPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("id",CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsPhotoGalleryDataModel.get(position).fileName);

                FragmentManager manager = ((AppCompatActivity)mContext).getSupportFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();

                SlideshowDialogFragment newFragment = new SlideshowDialogFragment();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "id");

            }
        });
    }

    @Override
    public int getItemCount() {
        return (null !=  CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsPhotoGalleryDataModel ? CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsPhotoGalleryDataModel.size() : 0);
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private VendorImageListAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final VendorImageListAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}