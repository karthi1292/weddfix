package com.weds.weddfix.category.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryCityNameDataModel;
import com.weds.weddfix.category.apiresponse.CategoryHomeDataModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterMthrTongueDataModel;
import com.weds.weddfix.matrimony.common.MtnyConstants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {

    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private Context mContext;
    private CountDownTimer countDownTimer;
    private boolean categoryLoggedIn;
    private CategoryStorePreference categoryStorePreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initWidgets();

        if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
            categoryHomeData();
        } else {
            CategoryCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
        }

        Glide.with(SplashActivity.this).load(R.drawable.wed_logo).into((ImageView) findViewById(R.id.img_splash));

        countDownTimer = new CountDownTimer(1000, 1000) {

              @Override
              public void onTick(long millisUntilFinished) {

              }

              @Override
              public void onFinish() {
                  if(categoryLoggedIn){
                      startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                      finish();
                  }else{
                      startActivity(new Intent(SplashActivity.this, LoginRegisterActivity.class));
                      finish();
                  }
              }
          };
    }


    private void initWidgets(){
        mContext = this;
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
        categoryStorePreference = new CategoryStorePreference(this);
        categoryLoggedIn = categoryStorePreference.getBooleanValue(CategoryConstants.U_LOGGED);
    }


    public void categoryHomeData(){
        pDialog = new CategoryActivityIndicator(this);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryHomeDataModel> call = apiService.getCategoryHomeData();
        call.enqueue(new Callback<CategoryHomeDataModel>() {
            @Override
            public void onResponse(Call<CategoryHomeDataModel> call, Response<CategoryHomeDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    ArrayList<CategoryCityNameDataModel> categoryCityDataModel=response.body().categoryCityDataModel;

                    CategoryHomeDataModel categoryHomeDataModel=new CategoryHomeDataModel();

                    categoryHomeDataModel.categoryNameDataModel=response.body().categoryNameDataModel;
                    categoryHomeDataModel.categoryBannerPhotoDataModel=response.body().categoryBannerPhotoDataModel;

                    CategoryCityNameDataModel categoryCityNameDataModel = new CategoryCityNameDataModel();
                    categoryCityNameDataModel.id = "0";
                    categoryCityNameDataModel.cityName = "Select";
                    categoryHomeDataModel.categoryCityDataModel.add(categoryCityNameDataModel);
                    categoryHomeDataModel.categoryCityDataModel.addAll(categoryCityDataModel);

                    CategorySingleton.getInstance().categoryHomeDataModel=categoryHomeDataModel;

                    countDownTimer.start();

                } else {
                    CategoryCommonMethods.showToast(CategoryConstants.TOAST_CONNECTION_ERROR, mContext);
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<CategoryHomeDataModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }
}
