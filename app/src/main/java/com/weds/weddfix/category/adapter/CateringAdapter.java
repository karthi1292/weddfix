package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.activity.CategoryDetailsActivity;
import com.weds.weddfix.category.apiresponse.CategoryVendorServiceDataModel;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.model.DataModel;

import java.util.ArrayList;

/**
 * Created by Acer on 22-10-2016.
 */


public class CateringAdapter extends RecyclerView.Adapter<CateringAdapter.ViewHolder> {

    private ArrayList<DataModel> arrayList;
    private Context mContext;
    private CategoryVendorServiceDataModel cateringServicesDataModel;
    public CateringAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.custom_home_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        cateringServicesDataModel = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.CATERINGS).get(position);

        String imageUrl = cateringServicesDataModel.fileName;
        String categoryName = cateringServicesDataModel.companyName;
        String price = cateringServicesDataModel.price;
        String rating = cateringServicesDataModel.maxRating;

        Glide.with(mContext).load(imageUrl).placeholder(R.drawable.wed_logo).into(holder.categoryImageView);
        holder.categoryNameTxtView.setText(categoryName);
        holder.categoryPriceTxtView.setText(price);
        if (rating.length()>0){
            holder.categoryRatingTxtView.setText(rating);
            holder.linearRating.setVisibility(View.VISIBLE);
        }else {
            holder.linearRating.setVisibility(View.GONE);
        }

        holder.categoryImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cateringServicesDataModel = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.CATERINGS).get(position);

                Intent i = new Intent(mContext, CategoryDetailsActivity.class);
                i.putExtra(CategoryConstants.VENDOR_ID, cateringServicesDataModel.id);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                notifyDataSetChanged();
                mContext.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.CATERINGS) ? CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.CATERINGS).size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView categoryImageView;
        private LinearLayout linearRating;
        private TextView categoryNameTxtView,categoryPriceTxtView,categoryRatingTxtView;

        public ViewHolder(View itemView) {
            super(itemView);
            categoryImageView = (ImageView) itemView.findViewById(R.id.vendor_cus_img);
            categoryNameTxtView = (TextView) itemView.findViewById(R.id.txtview_category_name);
            categoryPriceTxtView=(TextView)itemView.findViewById(R.id.txtview_category_price);
            categoryRatingTxtView=(TextView)itemView.findViewById(R.id.txtview_category_rating);
            linearRating = (LinearLayout) itemView.findViewById(R.id.linear_rating);
        }
    }
}