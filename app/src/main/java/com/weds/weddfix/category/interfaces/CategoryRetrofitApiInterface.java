package com.weds.weddfix.category.interfaces;

import com.weds.weddfix.category.apiresponse.CategoryCommonResponseEnquiryElementModel;
import com.weds.weddfix.category.apiresponse.CategoryHomeDataModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseModelOTPVerified;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseOrderHistoryElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponsePasswordChangeElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseProfileDetailsElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseProfileUpdateElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorBookingElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorDetailsElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorServicesElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorWishListShortElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorWishListUnShortElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorWishListViewElementModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorServicestResponseElementModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterCityDataModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterCountryDataModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterDataModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterStateDataModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by KarthickRaja on 1/22/2017.
 */

public interface CategoryRetrofitApiInterface {

    @GET("register.json")
    Call<MasterRegisterDataModel> getAllCategoryMasterRegisterData();

    @GET("register.json")
    Call<MasterRegisterCityDataModel> getAllCateCityMasterRegisterData(@Query("state_Id") String stateId);

    @GET("register.json")
    Call<MasterRegisterStateDataModel> getAllCateStateMasterRegisterData(@Query("country_Id") String countryId);

    @GET("register.json")
    Call<MasterRegisterCountryDataModel> getAllCateCountryMasterRegisterData();

    @GET("login.json")
    Call<CategoryCommonResponseElementModel> sendCategoryForgotPassword(@Query("forgotPasswordEmailId") String forgotPasswordEmailId);

    @GET("sendinfo.json?verificationCode=send")
    Call<CategoryCommonResponseElementModel> getCateUserOTPData(@Query("userId") String userId, @Query("verifyMobile") String verifyMobile);

    @GET("home.json")
    Call<CategoryHomeDataModel> getCategoryHomeData();

    @GET("categorylist.json")
    Call<CategoryHomeDataModel> getCategoryAndCityNameList();

    @GET("vendorlistbylocation.json")
    Call<CategoryVendorServicestResponseElementModel> getCategoryVendorServiceListBasedOnCity(@Query("userId") String userId, @Query("searchCityId") String searchCityId);

    @GET("vendorlist.json")
    Call<CategoryCommonResponseVendorServicesElementModel> getCategoryVendorServicesListBasedOnCategoryId(@Query("userId") String userId, @Query("searchCategoryId") String searchCategoryId);

    @GET("vendordetails.json")
    Call<CategoryCommonResponseVendorDetailsElementModel> getVendorDetails(@Query("vendorId") String vendorId, @Query("userId") String userId);

    @GET("mywishlist.json?save=shortlist")
    Call<CategoryCommonResponseVendorWishListShortElementModel> getWishListShortList(@Query("shortlistedVendorId") String shortlistedVendorId, @Query("userId") String userId);

    @GET("mywishlist.json?save=unShortlist")
    Call<CategoryCommonResponseVendorWishListUnShortElementModel> getWishListUnShortList(@Query("shortlistedId") String shortlistedVendorId);

    @GET("mywishlist.json?save=shortlist")
    Call<CategoryCommonResponseVendorWishListViewElementModel> getWishListView(@Query("userId") String userId);


    @GET("mybookings.json?")
    Call<CategoryCommonResponseOrderHistoryElementModel> getOrderHistory(@Query("userId") String userId);

    @GET("myprofile.json?update=changePassword")
    Call<CategoryCommonResponsePasswordChangeElementModel> getPasswordChange(@Query("userId") String userId,@Query("password") String password);

    @GET("myprofile.json?")
    Call<CategoryCommonResponseProfileDetailsElementModel> getUserProfileDetails(@Query("userId") String userId);

    @FormUrlEncoded
    @POST("register.json?save=register")
    Call<CategoryCommonResponseElementModel> registerCategoryData(@Field("weddingDate") String weddingDate, @Field("fullName") String fullName, @Field("email") String email, @Field("password") String password, @Field("mobile") String mobile, @Field("cityId") String cityId, @Field("stateId") String stateId, @Field("countryId") String countryId, @Field("pincode") String pincode);

    @FormUrlEncoded
    @POST("login.json")
    Call<CategoryCommonResponseElementModel> getCateUserProfileData(@Field("username") String userName, @Field("password") String password);

    @FormUrlEncoded
    @POST("sendinfo.json?verificationCode=verify")
    Call<CategoryCommonResponseModelOTPVerified> getVerifyCategoryOTPData(@Field("verifyMobileNumber") String verifyMobileNumber, @Field("verifyCode") String verifyCode, @Field("mobile") String mobile, @Field("userId") String userId);

    @FormUrlEncoded
    @POST("sendinfo.json?save=booking")
    Call<CategoryCommonResponseVendorBookingElementModel> getVendorBooking(@Field("vendorId") String vendorId, @Field("vendorCompanyName") String vendorCompanyName, @Field("vendorMobile") String vendorMobile, @Field("vendorPhone") String vendorPhone, @Field("vendorEmail") String vendorEmail, @Field("weddingDate") String weddingDate, @Field("profile") String name, @Field("mobile") String mobile, @Field("email") String email, @Field("userId") String userId, @Field("sendEmail") String sendEmail, @Field("needCallBack") String needCallBack);

    @FormUrlEncoded
    @POST("myprofile.json?update=myProfile")
    Call<CategoryCommonResponseProfileUpdateElementModel> getProfileUpdate(@Field("id") String id, @Field("weddingDate") String weddingDate, @Field("fullName") String fullName, @Field("mobile") String mobile, @Field("cityId") String cityId, @Field("stateId") String stateId, @Field("countryId") String countryId, @Field("pincode") String pincode);

    @FormUrlEncoded
    @POST("comment.json?save=comment")
    Call<CategoryCommonResponseEnquiryElementModel> getSendEnquiry(@Field("profile") String name, @Field("email") String email, @Field("subject") String subject, @Field("comment") String comment);


}
