package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 06-03-2017.
 */

public class CategoryCommonResponsePasswordChangeElementModel {
    @SerializedName("changePassword")
    public CategoryPasswordChangeDataModel categoryPasswordChangeDataModel;


}
