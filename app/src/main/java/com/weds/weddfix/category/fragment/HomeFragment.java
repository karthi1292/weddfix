package com.weds.weddfix.category.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.weds.weddfix.R;
import com.weds.weddfix.category.adapter.AstroAdapter;
import com.weds.weddfix.category.adapter.CitySpinnerAdapter;
import com.weds.weddfix.category.adapter.HotelsAdapter;
import com.weds.weddfix.category.adapter.TravelAdapter;
import com.weds.weddfix.category.adapter.WeddingHallAdapter;
import com.weds.weddfix.category.apiresponse.CategoryNameDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorServiceDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorServicestResponseElementModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;
import com.weds.weddfix.category.adapter.WeddingCardsAdapter;
import com.weds.weddfix.category.adapter.CateringAdapter;
import com.weds.weddfix.category.adapter.DecorationAdapter;
import com.weds.weddfix.category.adapter.EntertainAdapter;
import com.weds.weddfix.category.adapter.JewelAdapter;
import com.weds.weddfix.category.adapter.ParlourAdapter;
import com.weds.weddfix.category.adapter.SlidingImageAdapter;
import com.weds.weddfix.category.adapter.StudioAdapter;
import com.weds.weddfix.category.adapter.TextilesAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Acer on 23-10-2016.
 */
public class HomeFragment extends Fragment implements View.OnClickListener,AdapterView.OnItemSelectedListener{
    private RecyclerView recyclerWedHall;
    private RecyclerView recyclerStudios, recyclerAstrology, recyclerParlour, recyclerTravel, recyclerHotels, recyclerCatering, recycleWedCards,recyclerJewel, recyclerEntertain, recyclerTextiles, recyclerDecortion;
    private LinearLayoutManager layoutManagerWedding, layoutManagerTravel, layoutManagerAstro, layoutManagerStudios, layoutManagerCards, layoutManagerParlour, layoutManagerCatering, layoutManagerHotels, layoutManagerJewel, layoutManagerEntertain, layoutManagerTextiles, layoutManagerDecoration;
    private View view;
    private TextView txtWeddingHall,txtStudio,txtDecoration,txtParlour,txtJewel,txtCatering,txtEntertain,txtWedddingCard,txtTextiles,txtTravels,txtHotels,txtAstro, txtHallAll,txtStudioAll,txtDecoAll,txtParlourAll,txtJewelAll,txtcateringAll,txtEntertainAll,txtCardAll,txtTextileAll,txtTravelAll,txtHotelAll,txtAstroAll;
    private Context mContext;
    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private CategoryStorePreference categoryStorePreference;
    private ViewPager mPager;
    private static int currentPage = 0;
    private static int numOfPages = 0;
    private String userId, cityId;
    private Spinner citySpinner;
    private CitySpinnerAdapter citySpinnerAdapter;
    private FragmentManager fragmentManager;
    private RelativeLayout rloutWedHall,rloutStudio,rloutDecoration,rloutParlour,rloutJewel,rloutCatering,rloutAstrologers,rloutCards,rloutEntertainment,rloutTextiles,rloutTravels,rloutHotels;

    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initWidgets();

        slidingImages();

        return view;

    }

    public void initWidgets() {

        citySpinner = (Spinner)view.findViewById(R.id.category_spinner);

        rloutWedHall=(RelativeLayout)view.findViewById(R.id.rlout_wedding_hall);
        rloutStudio=(RelativeLayout)view.findViewById(R.id.rlout_studio);
        rloutDecoration=(RelativeLayout)view.findViewById(R.id.rlout_decoration);
        rloutParlour=(RelativeLayout)view.findViewById(R.id.rlout_parlour);
        rloutJewel=(RelativeLayout)view.findViewById(R.id.rlout_jewel);
        rloutCatering=(RelativeLayout)view.findViewById(R.id.rlout_caterings);
        rloutAstrologers=(RelativeLayout)view.findViewById(R.id.rlout_astrologers);
        rloutCards=(RelativeLayout)view.findViewById(R.id.rlout_wedd_card);
        rloutEntertainment=(RelativeLayout)view.findViewById(R.id.rlout_entertainment);
        rloutTextiles=(RelativeLayout)view.findViewById(R.id.rlout_textiles);
        rloutTravels=(RelativeLayout)view.findViewById(R.id.rlout_travels);
        rloutHotels=(RelativeLayout)view.findViewById(R.id.rlout_hotels);

        recyclerWedHall = (RecyclerView) view.findViewById(R.id.recycler_wedd_hall);
        recyclerStudios = (RecyclerView) view.findViewById(R.id.recycler_studios);
        recyclerDecortion = (RecyclerView) view.findViewById(R.id.recycler_decoration);
        recyclerParlour = (RecyclerView) view.findViewById(R.id.recycler_parlour);
        recyclerJewel = (RecyclerView) view.findViewById(R.id.recycler_jewel);
        recyclerCatering = (RecyclerView) view.findViewById(R.id.recycler_catering);
        recyclerEntertain = (RecyclerView) view.findViewById(R.id.recycler_entertainment);
        recycleWedCards = (RecyclerView) view.findViewById(R.id.recycler_wed_cards);
        recyclerTextiles = (RecyclerView) view.findViewById(R.id.recycler_textiles);
        recyclerTravel = (RecyclerView) view.findViewById(R.id.recycler_travels);
        recyclerHotels = (RecyclerView) view.findViewById(R.id.recycler_hotels);
        recyclerAstrology = (RecyclerView) view.findViewById(R.id.recycler_astrology);

        txtWeddingHall = (TextView) view.findViewById(R.id.txt_hall_title);
        txtAstro= (TextView) view.findViewById(R.id.txt_astro_title);
        txtCatering= (TextView) view.findViewById(R.id.txt_catering_title);
        txtDecoration= (TextView) view.findViewById(R.id.txt_decoration_title);
        txtEntertain= (TextView) view.findViewById(R.id.txt_entertainment_title);
        txtHotels= (TextView) view.findViewById(R.id.txt_hotel_title);
        txtJewel= (TextView) view.findViewById(R.id.txt_jewel_title);
        txtStudio= (TextView) view.findViewById(R.id.txt_studio_title);
        txtParlour= (TextView) view.findViewById(R.id.txt_parlour_title);
        txtWedddingCard= (TextView) view.findViewById(R.id.txt_card_title);
        txtTextiles= (TextView) view.findViewById(R.id.txt_textiles_title);
        txtTravels= (TextView) view.findViewById(R.id.txt_travels_title);
        txtHallAll = (TextView) view.findViewById(R.id.txt_viewAll_hall);
        txtStudioAll= (TextView) view.findViewById(R.id.txt_viewAll_studio);

        mPager = (ViewPager) view.findViewById(R.id.pager);

        txtHallAll.setOnClickListener(this);
        txtStudioAll.setOnClickListener(this);

        citySpinner.setOnItemSelectedListener(this);

        citySpinnerAdapter =new CitySpinnerAdapter(mContext);
        citySpinner.setAdapter(citySpinnerAdapter);

        layoutManagerWedding = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerStudios = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerDecoration = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerParlour = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerJewel = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerCatering = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerEntertain = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerCards = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerTextiles = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerTravel = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerHotels = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerAstro = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerWedHall.setLayoutManager(layoutManagerWedding);
        recyclerStudios.setLayoutManager(layoutManagerStudios);
        recyclerDecortion.setLayoutManager(layoutManagerDecoration);
        recyclerParlour.setLayoutManager(layoutManagerParlour);
        recyclerJewel.setLayoutManager(layoutManagerJewel);
        recyclerCatering.setLayoutManager(layoutManagerCatering);
        recyclerEntertain.setLayoutManager(layoutManagerEntertain);
        recycleWedCards.setLayoutManager(layoutManagerCards);
        recyclerTextiles.setLayoutManager(layoutManagerTextiles);
        recyclerTravel.setLayoutManager(layoutManagerTravel);
        recyclerHotels.setLayoutManager(layoutManagerHotels);
        recyclerAstrology.setLayoutManager(layoutManagerAstro);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
        categoryStorePreference = new CategoryStorePreference(mContext);
        userId = categoryStorePreference.getStringValue(CategoryConstants.USER_ID);

        fragmentManager = getActivity().getSupportFragmentManager();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setCategoryNamesAndLoadRecyclerViews(){

        CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.WEDDING_HALL);

        ArrayList<CategoryVendorServiceDataModel> mWeddingHallList = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.WEDDING_HALL);
        ArrayList<CategoryVendorServiceDataModel> mStudiosList = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.STUDIOS);
        ArrayList<CategoryVendorServiceDataModel> mBeautyParlourList = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.BEAUTY_PARLOUR);
        ArrayList<CategoryVendorServiceDataModel> mCateringList = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.CATERINGS);
        ArrayList<CategoryVendorServiceDataModel> mHotelsList = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.H0TELS);
        ArrayList<CategoryVendorServiceDataModel> mTextileList =CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.TEXTILES);
        ArrayList<CategoryVendorServiceDataModel> mJewelList =CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.JEWEL_SHOPS);
        ArrayList<CategoryVendorServiceDataModel> mEntertainList = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.ENTERTAINMENT);
        ArrayList<CategoryVendorServiceDataModel> mDecortionList = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.DECORATIONS);
        ArrayList<CategoryVendorServiceDataModel> mWedCardList = CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.WED_CARDS);
        ArrayList<CategoryVendorServiceDataModel> mAstroList =CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.ASTROLOGERS);
        ArrayList<CategoryVendorServiceDataModel> mTravelList =CategorySingleton.getInstance().allCategoryVendorServicesList.get(CategoryConstants.TRAVELS);

        ArrayList<CategoryNameDataModel> categoryNameDataModelList = CategorySingleton.getInstance().categoryHomeDataModel.categoryNameDataModel;

        for(CategoryNameDataModel categoryNameDataModel:categoryNameDataModelList){
            int categoryId=Integer.parseInt(categoryNameDataModel.getId());
            switch (categoryId){
                case 1:
                    if(mWeddingHallList.size()>0){
                        txtWeddingHall.setText(categoryNameDataModel.getCategoryName());
                        rloutWedHall.setVisibility(View.VISIBLE);

                        txtHallAll.setTag(categoryNameDataModel.getId());

                        WeddingHallAdapter wedHallAdapter = new WeddingHallAdapter(mContext);
                        recyclerWedHall.setAdapter(wedHallAdapter);

                    } else{
                        rloutWedHall.setVisibility(View.GONE);
                    }
                    break;
                case 2:
                    if(mStudiosList.size()>0){
                        txtStudio.setText(categoryNameDataModel.getCategoryName());
                        rloutStudio.setVisibility(View.VISIBLE);

                        txtStudioAll.setTag(categoryNameDataModel.getId());

                        StudioAdapter studioAdapter = new StudioAdapter(mContext);
                        recyclerStudios.setAdapter(studioAdapter);
                    }else{
                        rloutStudio.setVisibility(View.GONE);
                    }
                    break;
                case 3:
                    if(mDecortionList.size()>0){
                        txtDecoration.setText(categoryNameDataModel.getCategoryName());
                        rloutDecoration.setVisibility(View.VISIBLE);

                        DecorationAdapter decorationAdapter = new DecorationAdapter(mContext);
                        recyclerDecortion.setAdapter(decorationAdapter);
                    }else{
                        rloutDecoration.setVisibility(View.GONE);
                    }
                    break;
                case 4:
                    if(mBeautyParlourList.size()>0){
                        txtParlour.setText(categoryNameDataModel.getCategoryName());
                        rloutParlour.setVisibility(View.VISIBLE);

                        ParlourAdapter parlourAdapter = new ParlourAdapter(mContext);
                        recyclerParlour.setAdapter(parlourAdapter);

                    }else{
                        rloutParlour.setVisibility(View.GONE);
                    }
                    break;
                case 5:
                    if(mJewelList.size()>0){
                        txtJewel.setText(categoryNameDataModel.getCategoryName());
                        rloutJewel.setVisibility(View.VISIBLE);

                        JewelAdapter jewelAdapter = new JewelAdapter(mContext);
                        recyclerJewel.setAdapter(jewelAdapter);
                    }else{
                        rloutJewel.setVisibility(View.GONE);
                    }
                    break;
                case 6:
                    if(mCateringList.size()>0){
                        txtCatering.setText(categoryNameDataModel.getCategoryName());
                        rloutCatering.setVisibility(View.VISIBLE);

                        CateringAdapter cateringAdapter = new CateringAdapter(mContext);
                        recyclerCatering.setAdapter(cateringAdapter);

                    }else{
                        rloutCatering.setVisibility(View.GONE);
                    }
                    break;
                case 7:
                    if(mAstroList.size()>0) {
                        txtAstro.setText(categoryNameDataModel.getCategoryName());
                        rloutAstrologers.setVisibility(View.VISIBLE);

                        AstroAdapter astroAdapter = new AstroAdapter(mContext);
                        recyclerAstrology.setAdapter(astroAdapter);

                    }else{
                        rloutAstrologers.setVisibility(View.GONE);
                    }
                    break;
                case 8:
                    if(mWedCardList.size()>0) {
                        txtWedddingCard.setText(categoryNameDataModel.getCategoryName());
                        rloutCards.setVisibility(View.VISIBLE);

                        WeddingCardsAdapter wedClothesAdapter = new WeddingCardsAdapter(mContext);
                        recycleWedCards.setAdapter(wedClothesAdapter);
                    }else {
                        rloutCards.setVisibility(View.GONE);
                    }
                    break;
                case 9:
                    if(mEntertainList.size()>0){
                        txtEntertain.setText(categoryNameDataModel.getCategoryName());
                        rloutEntertainment.setVisibility(View.VISIBLE);

                        EntertainAdapter entertainAdapter = new EntertainAdapter(mContext);
                        recyclerEntertain.setAdapter(entertainAdapter);
                    }
                     else{
                        rloutEntertainment.setVisibility(View.GONE);
                    }
                    break;
                case 10:
                    if(mTextileList.size()>0){
                        txtTextiles.setText(categoryNameDataModel.getCategoryName());
                        rloutTextiles.setVisibility(View.VISIBLE);

                        TextilesAdapter textilesAdapter = new TextilesAdapter(mContext);
                        recyclerTextiles.setAdapter(textilesAdapter);

                    }else{
                        rloutTextiles.setVisibility(View.GONE);
                    }
                    break;
                case 11:
                    if(mTravelList.size()>0){
                        txtTravels.setText(categoryNameDataModel.getCategoryName());
                        rloutTravels.setVisibility(View.VISIBLE);

                        TravelAdapter travelAdapter = new TravelAdapter(mContext);
                        recyclerTravel.setAdapter(travelAdapter);
                    }else{
                        rloutTravels.setVisibility(View.GONE);
                    }
                    break;
                case 12:
                    if(mHotelsList.size()>0){
                        txtHotels.setText(categoryNameDataModel.getCategoryName());
                        rloutHotels.setVisibility(View.VISIBLE);

                        HotelsAdapter hotelsAdapter = new HotelsAdapter(mContext);
                        recyclerHotels.setAdapter(hotelsAdapter);
                    }else{
                        rloutHotels.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    private void slidingImages() {

        mPager.setAdapter(new SlidingImageAdapter(getActivity(), CategorySingleton.getInstance().categoryHomeDataModel.categoryBannerPhotoDataModel));

        numOfPages = CategorySingleton.getInstance().categoryHomeDataModel.categoryBannerPhotoDataModel.size();

        // Auto start of viewpager
        final Handler slideImageHandler = new Handler();
        final Runnable slideImageRunnable = new Runnable() {
            public void run() {
                if (currentPage == numOfPages) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                slideImageHandler.post(slideImageRunnable);
            }
        }, 3500, 3500);
    }


    @Override
    public void onClick(View view) {



                CategoryListFragment fragment = new CategoryListFragment((view.getTag()).toString());
                fragmentManager.beginTransaction().replace(R.id.category_list_frame_layout, fragment).commit();


    }

    public void categoryVendorServicesListBasedOnCity(String cityId) {
        pDialog = new CategoryActivityIndicator(getActivity());
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryVendorServicestResponseElementModel> call = apiService.getCategoryVendorServiceListBasedOnCity(userId, cityId);
        call.enqueue(new Callback<CategoryVendorServicestResponseElementModel>() {
            @Override
            public void onResponse(Call<CategoryVendorServicestResponseElementModel> call, Response<CategoryVendorServicestResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    if(CategorySingleton.getInstance().allCategoryVendorServicesList.size()>0)
                        CategorySingleton.getInstance().allCategoryVendorServicesList.clear();

                    ArrayList<CategoryVendorServiceDataModel> mWeddingHallList = response.body().categoryVendorServicesListModel.wedHallServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mStudiosList = response.body().categoryVendorServicesListModel.studiosServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mBeautyParlourList = response.body().categoryVendorServicesListModel.beautyParloursServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mCateringList = response.body().categoryVendorServicesListModel.cateringsServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mHotelsList = response.body().categoryVendorServicesListModel.hotelsServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mTextileList = response.body().categoryVendorServicesListModel.textilesServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mJewelList = response.body().categoryVendorServicesListModel.jewelShopsServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mEntertainList = response.body().categoryVendorServicesListModel.entertainmentsServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mDecortionList = response.body().categoryVendorServicesListModel.decorationsServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mWedCardList = response.body().categoryVendorServicesListModel.weddingClothesServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mAstroList = response.body().categoryVendorServicesListModel.wedAstrologersServiceModelList;
                    ArrayList<CategoryVendorServiceDataModel> mTravelList = response.body().categoryVendorServicesListModel.travelServiceModelList;

                    HashMap<String,ArrayList<CategoryVendorServiceDataModel>> categoryHashMapList=new HashMap();
                    categoryHashMapList.put(CategoryConstants.WEDDING_HALL,mWeddingHallList);
                    categoryHashMapList.put(CategoryConstants.STUDIOS,mStudiosList);
                    categoryHashMapList.put(CategoryConstants.DECORATIONS,mDecortionList);
                    categoryHashMapList.put(CategoryConstants.BEAUTY_PARLOUR,mBeautyParlourList);
                    categoryHashMapList.put(CategoryConstants.JEWEL_SHOPS,mJewelList);
                    categoryHashMapList.put(CategoryConstants.CATERINGS,mCateringList);
                    categoryHashMapList.put(CategoryConstants.ENTERTAINMENT,mEntertainList);
                    categoryHashMapList.put(CategoryConstants.WED_CARDS,mWedCardList);
                    categoryHashMapList.put(CategoryConstants.TEXTILES,mTextileList);
                    categoryHashMapList.put(CategoryConstants.TRAVELS,mTravelList);
                    categoryHashMapList.put(CategoryConstants.H0TELS,mHotelsList);
                    categoryHashMapList.put(CategoryConstants.ASTROLOGERS,mAstroList);

                    CategorySingleton.getInstance().allCategoryVendorServicesList=categoryHashMapList;

                    setCategoryNamesAndLoadRecyclerViews();

                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                }
                if (pDialog != null)
                    pDialog.dismiss();

            }

            @Override
            public void onFailure(Call<CategoryVendorServicestResponseElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        cityId = citySpinnerAdapter.getCityId(position);
        categoryVendorServicesListBasedOnCity(cityId);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
