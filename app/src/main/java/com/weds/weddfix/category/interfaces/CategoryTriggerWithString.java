package com.weds.weddfix.category.interfaces;

/**
 * Created by KarthickRaja on 2/19/2017.
 */

public interface CategoryTriggerWithString {
    public void initTrigger(boolean Trigger, String string);
}
