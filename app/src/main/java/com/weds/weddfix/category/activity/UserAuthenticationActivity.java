package com.weds.weddfix.category.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseModelOTPVerified;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;

import retrofit2.Call;
import retrofit2.Callback;


public class UserAuthenticationActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private Button authenticationBtn;
    private EditText edOtp;
    private String OTP;
    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private Context mContext;
    private CategoryStorePreference categoryStorePreference;
    private String verifyMobileNumber;
    private String mobile;
    private TextView txtResend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_authenticate);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        initWidgets();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (CategoryCommonMethods.isNetworkConnectionAvailable(this)) {
            sendOTP();
        } else {
            CategoryCommonMethods.showToast(this, "Please check your network connection !!!");
        }
    }

    private void initWidgets() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        edOtp = (EditText) findViewById(R.id.ed_user_otp);
        authenticationBtn = (Button) findViewById(R.id.btn_authentication);
        txtResend=(TextView)findViewById(R.id.txt_resend);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtResend.setOnClickListener(this);
        authenticationBtn.setOnClickListener(this);

        mContext = this;
        categoryStorePreference = new CategoryStorePreference(mContext);
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);

    }

    public void verifyOTP() {

        pDialog = new CategoryActivityIndicator(this);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseModelOTPVerified> call = apiService.getVerifyCategoryOTPData(verifyMobileNumber, verifyMobileNumber, mobile, categoryStorePreference.getStringValue(CategoryConstants.USER_ID));
        call.enqueue(new Callback<CategoryCommonResponseModelOTPVerified>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseModelOTPVerified> call, retrofit2.Response<CategoryCommonResponseModelOTPVerified> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().categoryUserOTPVerifiedProfileModel.status;
                    String message = response.body().categoryUserOTPVerifiedProfileModel.message;
                    mobile = response.body().categoryUserOTPVerifiedProfileModel.mobile;
                    verifyMobileNumber = response.body().categoryUserOTPVerifiedProfileModel.verifiedMobileNumber;


                    if (status.equals(CategoryConstants.SUCCESS)) {

                        Intent homeIntent = new Intent(UserAuthenticationActivity.this, HomeActivity.class);
                        startActivity(homeIntent);
                        finish();
                        if (pDialog != null)
                            pDialog.dismiss();

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CategoryCommonResponseModelOTPVerified> call, Throwable t) {
                Log.e("Failure", t.getMessage());

            }
        });
    }

    public void sendOTP() {
        pDialog = new CategoryActivityIndicator(this);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseElementModel> call = apiService.getCateUserOTPData(categoryStorePreference.getStringValue(CategoryConstants.USER_ID), categoryStorePreference.getStringValue(CategoryConstants.U_PHONE));
        call.enqueue(new Callback<CategoryCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseElementModel> call, retrofit2.Response<CategoryCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().categoryUserOTPProfileModel.status;
                    String message = response.body().categoryUserOTPProfileModel.message;
                    String mobile = response.body().categoryUserOTPProfileModel.mobile;
                    verifyMobileNumber = response.body().categoryUserOTPProfileModel.verifyMobileNumber;

                    if (status.equals(CategoryConstants.SUCCESS)) {

                        if (pDialog != null)
                            pDialog.dismiss();

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CategoryCommonResponseElementModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_authentication:
                if (CategoryCommonMethods.isNetworkConnectionAvailable(UserAuthenticationActivity.this)) {

                    OTP = edOtp.getText().toString();
                    if (OTP.trim().equals("")) {
                        CategoryCommonMethods.showToast(UserAuthenticationActivity.this, "Please Enter the OTP");
                        return;
                    }

                    if (OTP.equals(verifyMobileNumber)) {
                        verifyOTP();
                    } else {
                        CategoryCommonMethods.showToast(UserAuthenticationActivity.this, "Please enter the correct OTP");
                    }
                } else {
                    CategoryCommonMethods.showToast(UserAuthenticationActivity.this, "Please check your network connection !!!");
                }
                break;
            case R.id.txt_resend:
                if (CategoryCommonMethods.isNetworkConnectionAvailable(this)) {
                    sendOTP();
                } else {
                    CategoryCommonMethods.showToast(this, "Please check your network connection !!!");
                }
                break;

        }
    }
}
