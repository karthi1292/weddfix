package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 06-03-2017.
 */

public class CategoryPasswordChangeDataModel {

    @SerializedName("status")
    public String status;
    @SerializedName("mtny_message")
    public String message;
}
