package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 3/28/2017.
 */

public class CategoryVendorDetailsVideoUrlsDataModel {
    @SerializedName("categoryVideoImage")
    public String categoryVideoImage;
    @SerializedName("categoryVideoUrl")
    public String categoryVideoUrl;
}
