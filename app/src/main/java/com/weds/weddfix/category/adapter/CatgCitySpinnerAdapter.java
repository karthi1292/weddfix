package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryRegisterCityDataModel;

import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class CatgCitySpinnerAdapter extends BaseAdapter {

    private Context mContext;
    private List<CategoryRegisterCityDataModel> cityDataModelArrayList;


    public CatgCitySpinnerAdapter(Context context, List<CategoryRegisterCityDataModel> catgRegisterCityDataModelList) {
        this.mContext=context;
        this.cityDataModelArrayList=catgRegisterCityDataModelList;
    }
    @Override
    public int getCount() {
        return cityDataModelArrayList.size();
    }

    @Override
    public CategoryRegisterCityDataModel getItem(int position) {
        return cityDataModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public String getCityName(int position) {
        return getItem(position).getCityName();
    }
    public String getCityId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        CatgCitySpinnerAdapter.ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new CatgCitySpinnerAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, viewGroup, false);
            holder.txtCityName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (CatgCitySpinnerAdapter.ViewHolder) convertView.getTag();
        }
        holder.txtCityName.setText(getCityName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtCityName;
    }
}
