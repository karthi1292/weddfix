package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rameshmuthu on 02-03-2017.
 */

public class CategoryCommonResponseVendorServicesElementModel {

    @SerializedName("vendorServices")
    public ArrayList<CategoryVendorServiceDataModel> categoryVendorServiceDataModelList;


}
