package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 03-03-2017.
 */

public class CategoryVendorWishListShortDataModel {

    @SerializedName("status")
    public String status;
    @SerializedName("mtny_message")
    public String message;
    @SerializedName("shortlistedVendorId")
    public String shortlistedVendorId;
    @SerializedName("shortlistedId")
    public String shortlistedId;

}
