package com.weds.weddfix.category.common;


import com.weds.weddfix.category.apiresponse.CategoryCityNameDataModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorDetailsElementModel;
import com.weds.weddfix.category.apiresponse.CategoryHomeDataModel;
import com.weds.weddfix.category.apiresponse.CategoryNameDataModel;
import com.weds.weddfix.category.apiresponse.CategoryOrderHistoryDataModel;
import com.weds.weddfix.category.apiresponse.CategoryUserProfileDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorDetailsPhotoGalleryDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorDetailsReviewsDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorServiceDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorWishListViewDataModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterCityDataModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterCountryDataModel;
import com.weds.weddfix.category.apiresponse.MasterRegisterStateDataModel;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by KarthickRaja on 1/22/2017.
 */

public class CategorySingleton {

    private static CategorySingleton instance = null;

    public static CategorySingleton getInstance() {
        if (instance == null) {
            synchronized (CategorySingleton.class) {
                if (instance == null) {
                    instance = new CategorySingleton();
                }
            }
        }
        return instance;
    }


    public MasterRegisterCityDataModel catgMasterCityDataModel = new MasterRegisterCityDataModel();
    public MasterRegisterStateDataModel catgMasterStateDataModel = new MasterRegisterStateDataModel();
    public MasterRegisterCountryDataModel catgMasterCountryDataModel = new MasterRegisterCountryDataModel();
    public CategoryUserProfileDataModel categoryUserProfileDataModel = new CategoryUserProfileDataModel();


    public CategoryHomeDataModel categoryHomeDataModel;

    public HashMap<String,ArrayList<CategoryVendorServiceDataModel>> allCategoryVendorServicesList=new HashMap<>();

    public ArrayList<CategoryVendorServiceDataModel> categoryVendorServicesList;

    public CategoryCommonResponseVendorDetailsElementModel categoryCommonResponseVendorDetailsElementModel=new CategoryCommonResponseVendorDetailsElementModel();

    public ArrayList<CategoryVendorWishListViewDataModel> categoryVendorWishListViewDataModel;
    public ArrayList<CategoryOrderHistoryDataModel> categoryOrderHistoryDataModels;
    public ArrayList<CategoryVendorDetailsPhotoGalleryDataModel> categoryVendorDetailsPhotoGalleryDataModels;

}
