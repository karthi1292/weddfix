package com.weds.weddfix.category.interfaces;

/**
 * Created by KarthickRaja on 4/12/2017.
 */

public interface CategoryTriggerWithObject {
    public void initTrigger(boolean Trigger, Object obj);
}
