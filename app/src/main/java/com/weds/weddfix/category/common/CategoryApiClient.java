package com.weds.weddfix.category.common;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by KarthickRaja on 1/22/2017.
 */

public class CategoryApiClient {
    public static final String CATEGORY_BASE_URL="https://weddfix.com/";
    private static Retrofit retrofit = null;

    public static Retrofit getCategoryClient() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(interceptor).connectTimeout(2, TimeUnit.MINUTES).writeTimeout(2, TimeUnit.MINUTES).readTimeout(2, TimeUnit.MINUTES).build();

            retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(CATEGORY_BASE_URL).client(okHttpClient).build();
        }
        return retrofit;
    }



}
