package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class CategoryRegisterCityDataModel {


    @SerializedName("id")
    public String id;
    @SerializedName("cityName")
    public String cityName;

    public CategoryRegisterCityDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }


}
