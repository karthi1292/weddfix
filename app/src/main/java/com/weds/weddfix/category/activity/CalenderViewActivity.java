package com.weds.weddfix.category.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.squareup.timessquare.CalendarPickerView;
import com.weds.weddfix.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Acer on 23-10-2016.
 */
public class CalenderViewActivity extends AppCompatActivity {

    CalendarPickerView calendarPickerView;
    FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calender_date);


        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_calender_done);
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 5);

        calendarPickerView = (CalendarPickerView) findViewById(R.id.calendar_view);
        Date today = new Date();
        calendarPickerView.init(today, nextYear.getTime())
                .withSelectedDate(today)
                .inMode(CalendarPickerView.SelectionMode.RANGE);
        // calendar.highlightDates(getHolidays());

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<Date> selectedDates = (ArrayList<Date>) calendarPickerView.getSelectedDates();

                String startDate = selectedDates.get(0).toString();
                String endDate = selectedDates.get(selectedDates.size() - 1).toString();
                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                Date date1 = null;
                Date date2 = null;
                try {
                    date1 = (Date) formatter.parse(startDate);
                    date2 = (Date) formatter.parse(endDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                // System.out.println(date1);

                Calendar cal = Calendar.getInstance();
                cal.setTime(date1);
                String formatedDate1 = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);
                cal.setTime(date2);
                String formatedDate2 = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);


                Intent i = new Intent(CalenderViewActivity.this, CategoryDetailsActivity.class);
                i.putExtra("startDate", formatedDate1);
                i.putExtra("endDate", formatedDate2);
                setResult(111, i);
                finish();


            }
        });
    }
}
