package com.weds.weddfix.category.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.weds.weddfix.R;
import com.weds.weddfix.category.adapter.CustomerReviewAdapter;
import com.weds.weddfix.category.interfaces.CategoryTriggerWithString;

/**
 * Created by KarthickRaja on 4/2/2017.
 */

public class ServiceReviewDialog extends Dialog {
    private Context mContext;
    private Dialog dialog;
    private RecyclerView rvReviews;
    private CategoryTriggerWithString trigger;
    private CustomerReviewAdapter reviewAdapter;
    private LinearLayoutManager llManager;

    public ServiceReviewDialog(Context context) {
        super(context);
        this.mContext=context;
    }

    public void showDialog() {

        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams WMLP = dialog.getWindow().getAttributes();
        WMLP.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(WMLP);

        dialog.setContentView(R.layout.dialog_service_review);

        rvReviews = (RecyclerView) dialog.findViewById(R.id.rv_review);

        CustomerReviewAdapter customerReviewAdapter = new CustomerReviewAdapter(mContext);
        llManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvReviews.setLayoutManager(llManager);
        rvReviews.setAdapter(customerReviewAdapter);

        dialog.show();
    }
}
