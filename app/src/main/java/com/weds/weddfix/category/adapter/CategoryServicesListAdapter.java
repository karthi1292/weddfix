package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.activity.CategoryDetailsActivity;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.activity.QuoteActivity;
import com.weds.weddfix.category.common.CategoryFontUtil;


/**
 * Created by Acer on 26-10-2016.
 */
public class CategoryServicesListAdapter extends RecyclerView.Adapter<CategoryServicesListAdapter.ViewHolder> {

    private Context mContext;
    private String vendorId;
    private String reviewCount;

    public CategoryServicesListAdapter(Context context) {

        this.mContext = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_category_services_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        vendorId = CategorySingleton.getInstance().categoryVendorServicesList.get(position).id;
        String vendorName = CategorySingleton.getInstance().categoryVendorServicesList.get(position).companyName;
        String vendorPrice = CategorySingleton.getInstance().categoryVendorServicesList.get(position).price;
        String vendorLocation = CategorySingleton.getInstance().categoryVendorServicesList.get(position).location;
        String vendorImage = CategorySingleton.getInstance().categoryVendorServicesList.get(position).fileName;

        reviewCount = CategorySingleton.getInstance().categoryVendorServicesList.get(position).maxRating;

        Glide.with(mContext).load(vendorImage).into(holder.imageView);

        holder.textName.setText(vendorName);
        holder.textLocation.setText(vendorLocation);
        holder.textPackage.setText(vendorPrice);

        if (reviewCount.length()>0){
            holder.textReview.setText(reviewCount);
            holder.relativeRating.setVisibility(View.VISIBLE);
        }else {
            holder.relativeRating.setVisibility(View.GONE);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mContext, CategoryDetailsActivity.class);
                i.putExtra(CategoryConstants.VENDOR_ID, CategorySingleton.getInstance().categoryVendorServicesList.get(position).id);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                notifyDataSetChanged();
                mContext.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return CategorySingleton.getInstance().categoryVendorServicesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, VideoBtn;
        TextView textName, textPackage, textLocation, textReview, txt_price, txt_location;
        CardView cardView;
        Button quoteBtn;
        RelativeLayout relativeRating;
        ImageView imgRating;
        String link = "https://www.youtube.com/watch?v=6HWRfkMUJV4";

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageview_catgeory);
            textName = (TextView) itemView.findViewById(R.id.txtview_category_name);
            textPackage = (TextView) itemView.findViewById(R.id.txtview_category_service_price);
            textLocation = (TextView) itemView.findViewById(R.id.txtview_category_service_location);
            textReview = (TextView) itemView.findViewById(R.id.txtview_category_service_reviews);
            relativeRating= (RelativeLayout) itemView.findViewById(R.id.rating_relative_layout);
            imgRating= (ImageView) itemView.findViewById(R.id.image_rating);

            //txt_location = (TextView) itemView.findViewById(R.id.txtview_location);
            //txt_price = (TextView) itemView.findViewById(R.id.txtview_price);
            cardView = (CardView) itemView.findViewById(R.id.card_custom_items);
            quoteBtn = (Button) itemView.findViewById(R.id.btn_hall_get_quote);
            VideoBtn = (ImageView) itemView.findViewById(R.id.img_youtube);

            Typeface typeface = CategoryFontUtil.getHelvetica(mContext);

            //textName.setTypeface(typeface);
            //textPackage.setTypeface(typeface);
            //textLocation.setTypeface(typeface);
            //txt_location.setTypeface(typeface);
            //txt_price.setTypeface(typeface);


            quoteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mContext, QuoteActivity.class);
                    mContext.startActivity(i);

                }
            });
            VideoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (link.isEmpty()) {

                    } else {
                        mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
                    }

                }
            });

        }
    }
}