package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by KarthickRaja on 3/6/2017.
 */

public class CategoryVendorServicesListModel {

    @SerializedName("weddingHalls")
    public ArrayList<CategoryVendorServiceDataModel> wedHallServiceModelList;
    @SerializedName("studios")
    public  ArrayList<CategoryVendorServiceDataModel> studiosServiceModelList;
    @SerializedName("decorations")
    public ArrayList<CategoryVendorServiceDataModel> decorationsServiceModelList;
    @SerializedName("beautyParlours")
    public ArrayList<CategoryVendorServiceDataModel> beautyParloursServiceModelList;
    @SerializedName("jewelShops")
    public ArrayList<CategoryVendorServiceDataModel> jewelShopsServiceModelList;
    @SerializedName("caterings")
    public ArrayList<CategoryVendorServiceDataModel> cateringsServiceModelList;
    @SerializedName("entertainments")
    public  ArrayList<CategoryVendorServiceDataModel> entertainmentsServiceModelList;
    @SerializedName("weddingClothes")
    public ArrayList<CategoryVendorServiceDataModel> weddingClothesServiceModelList;
    @SerializedName("textiles")
    public ArrayList<CategoryVendorServiceDataModel> textilesServiceModelList;
    @SerializedName("travels")
    public  ArrayList<CategoryVendorServiceDataModel> travelServiceModelList;
    @SerializedName("hotels")
    public  ArrayList<CategoryVendorServiceDataModel> hotelsServiceModelList;
    @SerializedName("weddingAstrologers")
    public ArrayList<CategoryVendorServiceDataModel> wedAstrologersServiceModelList;

}
