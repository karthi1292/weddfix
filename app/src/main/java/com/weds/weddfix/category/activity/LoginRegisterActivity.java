package com.weds.weddfix.category.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategoryFontUtil;
import com.weds.weddfix.category.common.CategoryStorePreference;


/**
 * Created by Acer on 27-10-2016.
 */
public class LoginRegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_signIn, btn_register, btn_skip_signin;
    private TextView txtWish;
    private Context mContext;
    private boolean categoryLoggedIn;
    private CategoryStorePreference categoryStorePreference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_login_register);

        if (categoryLoggedIn) {
            startActivity(new Intent(LoginRegisterActivity.this, HomeActivity.class));
            finish();
        }

        initWidgets();

    }

    private void initWidgets() {
        btn_signIn = (Button) findViewById(R.id.btn_signin);
        btn_register = (Button) findViewById(R.id.btn_register);
        btn_skip_signin = (Button) findViewById(R.id.btn_skip_signin);
        txtWish = (TextView) findViewById(R.id.txt_wish);

        btn_skip_signin.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        btn_signIn.setOnClickListener(this);

        Typeface typeface = CategoryFontUtil.getHelvetica(LoginRegisterActivity.this);
        txtWish.setTypeface(typeface);

        mContext = this;
        categoryStorePreference = new CategoryStorePreference(this);
        categoryLoggedIn = categoryStorePreference.getBooleanValue(CategoryConstants.U_LOGGED);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                startActivity(new Intent(LoginRegisterActivity.this, LoginActivity.class));
                break;
            case R.id.btn_register:
                startActivity(new Intent(LoginRegisterActivity.this, CategoryRegisterActivity.class));
                break;
            case R.id.btn_skip_signin:
                //Without login have to send userId as 0
                categoryStorePreference.setString(CategoryConstants.USER_ID, "0");
                startActivity(new Intent(LoginRegisterActivity.this, HomeActivity.class));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
