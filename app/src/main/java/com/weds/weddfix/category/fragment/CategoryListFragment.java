package com.weds.weddfix.category.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.category.adapter.CategoryServicesListAdapter;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorServicesElementModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorServiceDataModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Acer on 26-10-2016.
 */
public class CategoryListFragment extends Fragment {

    private RecyclerView recyclerViewCatgeoryList;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String categoryId;
    private Context mContext;
    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private CategoryServicesListAdapter categoryRecyclerViewAdapter;
    private LinearLayoutManager categoryLinearLayoutManager;
    private CategoryStorePreference categoryStorePreference;

    public CategoryListFragment() {

    }

    @SuppressLint("ValidFragment")
    public CategoryListFragment(String categoryId) {

        this.categoryId = categoryId;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.category_list_items, container, false);

        initWidgets(view);

        if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
            getVendorServicesList();
        } else {
            CategoryCommonMethods.showToast(CategoryConstants.TOAST_CONNECTION_ERROR, mContext);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
                    populatRecyclerView();
                } else {
                    CategoryCommonMethods.showToast(CategoryConstants.TOAST_CONNECTION_ERROR, mContext);
                }
            }
        });

        return view;
    }

    public void initWidgets(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh_category_list);
        recyclerViewCatgeoryList = (RecyclerView) view.findViewById(R.id.recycler_view_category_list);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
        categoryStorePreference = new CategoryStorePreference(mContext);
    }


    public void getVendorServicesList() {

        pDialog = new CategoryActivityIndicator(getActivity());
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseVendorServicesElementModel> call = apiService.getCategoryVendorServicesListBasedOnCategoryId(categoryStorePreference.getStringValue(CategoryConstants.USER_ID), categoryId);
        call.enqueue(new Callback<CategoryCommonResponseVendorServicesElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseVendorServicesElementModel> call, Response<CategoryCommonResponseVendorServicesElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    ArrayList<CategoryVendorServiceDataModel> categoryVendorDetailsListDataModel = response.body().categoryVendorServiceDataModelList;

                    if (categoryVendorDetailsListDataModel.size() > 0) {
                        CategorySingleton.getInstance().categoryVendorServicesList = categoryVendorDetailsListDataModel;

                        categoryRecyclerViewAdapter = new CategoryServicesListAdapter(mContext);
                        categoryLinearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerViewCatgeoryList.setLayoutManager(categoryLinearLayoutManager);
                        recyclerViewCatgeoryList.setAdapter(categoryRecyclerViewAdapter);
                    }else{
                        Toast.makeText(getContext(),CategoryConstants.NO_SERVICES,Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                }
                if (pDialog != null)
                    pDialog.dismiss();
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<CategoryCommonResponseVendorServicesElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

            }
        });
    }

    private void populatRecyclerView() {
        if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
            getVendorServicesList();
        } else {
            CategoryCommonMethods.showToast(CategoryConstants.TOAST_CONNECTION_ERROR, mContext);
        }

        if(categoryRecyclerViewAdapter!=null)
        categoryRecyclerViewAdapter.notifyDataSetChanged();
    }
}
