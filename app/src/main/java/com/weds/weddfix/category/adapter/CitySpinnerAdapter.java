package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryCityNameDataModel;
import com.weds.weddfix.category.common.CategorySingleton;

/**
 * Created by KarthickRaja on 3/12/2017.
 */

public class CitySpinnerAdapter extends BaseAdapter {
    private Context mContext;

    public CitySpinnerAdapter(Context mContext) {
        this.mContext = mContext;

    }

    @Override
    public int getCount() {
        return CategorySingleton.getInstance().categoryHomeDataModel.categoryCityDataModel.size();
    }

    @Override
    public CategoryCityNameDataModel getItem(int position) {
        return CategorySingleton.getInstance().categoryHomeDataModel.categoryCityDataModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public String getCityId(int position) {
        return getItem(position).getId();
    }

    public String getCityName(int position) {
        return getItem(position).getCityName();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
    ViewHolder holder;
        LayoutInflater inflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view==null){
            holder=new ViewHolder();
            view=inflater.inflate(R.layout.custom_home_city_search_spinner,parent,false);
            holder.txtCityName =(TextView)view.findViewById(R.id.state_name);
            view.setTag(holder);
        }else{
            holder=(ViewHolder)view.getTag();
        }
        holder.txtCityName.setText(getCityName(position));

        return view;
    }

    private class ViewHolder {
        private TextView txtCityName;
    }
}
