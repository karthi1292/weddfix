package com.weds.weddfix.category.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorDetailsElementModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorDetailsDataModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Acer on 28-10-2016.
 */
public class CategoryAboutUsFragment extends Fragment {

    private CategoryStorePreference categoryStorePreference;
    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private Context mContext;
    private TextView txtVendorLocation, txtVendorCity, txtVendorCountry, txtVendorState, txtVendorAddress, txtVendorAbout;
    private String id;
    private String userId;
    private View view;
    private String vendorCity, vendorLocation, vendorAddress, vendorState, vendorCountry, vendorAbout, vendorPincode;

    public CategoryAboutUsFragment() {

    }

    @SuppressLint("ValidFragment")
    public CategoryAboutUsFragment(String id) {
        this.id = id;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.category_about_service, container, false);
        initWidgets();
        getVendorDetails();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
        categoryStorePreference = new CategoryStorePreference(mContext);
        userId = categoryStorePreference.getStringValue(CategoryConstants.USER_ID);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initWidgets() {

        txtVendorAddress = (TextView) view.findViewById(R.id.txt_service_address);
        txtVendorAbout = (TextView) view.findViewById(R.id.txt_service_abt);
        txtVendorCity = (TextView) view.findViewById(R.id.txt_service_city);
        txtVendorCountry = (TextView) view.findViewById(R.id.txt_service_country);
        txtVendorLocation = (TextView) view.findViewById(R.id.txt_service_street);
        txtVendorState = (TextView) view.findViewById(R.id.txt_service_state);

    }

    public void getVendorDetails() {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseVendorDetailsElementModel> call = apiService.getVendorDetails(id, categoryStorePreference.getStringValue(CategoryConstants.USER_ID));
        call.enqueue(new Callback<CategoryCommonResponseVendorDetailsElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseVendorDetailsElementModel> call, Response<CategoryCommonResponseVendorDetailsElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    CategoryVendorDetailsDataModel categoryVendorDetailsDataModel = response.body().categoryVendorDetailsDataModel;


                    vendorCity = categoryVendorDetailsDataModel.vendorCityName;
                    vendorLocation = categoryVendorDetailsDataModel.location;
                    vendorAddress = categoryVendorDetailsDataModel.address;
                    vendorState = categoryVendorDetailsDataModel.vendorStateName;
                    vendorCountry = categoryVendorDetailsDataModel.vendorCountryName;
                    vendorAbout = categoryVendorDetailsDataModel.aboutUs;
                    vendorPincode = categoryVendorDetailsDataModel.pincode;

                    loadVendorDetails(categoryVendorDetailsDataModel);

                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<CategoryCommonResponseVendorDetailsElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    private void loadVendorDetails(CategoryVendorDetailsDataModel categoryVendorDetailsDataModel) {
        txtVendorAbout.setText(categoryVendorDetailsDataModel.aboutUs);
        txtVendorAddress.setText(categoryVendorDetailsDataModel.address);
        txtVendorLocation.setText(categoryVendorDetailsDataModel.location);
        txtVendorCity.setText(categoryVendorDetailsDataModel.cityName + "," + categoryVendorDetailsDataModel.pincode);
        txtVendorState.setText(categoryVendorDetailsDataModel.stateName);
        txtVendorCountry.setText(categoryVendorDetailsDataModel.countryName);
    }
}
