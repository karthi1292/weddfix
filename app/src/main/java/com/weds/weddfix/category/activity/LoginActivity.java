package com.weds.weddfix.category.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.weds.weddfix.R;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseElementModel;
import com.weds.weddfix.category.apiresponse.CategoryUserLoginDetailsDataModel;
import com.weds.weddfix.category.apiresponse.CategoryUserProfileDataModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.dialog.CategoryForgotPasswordDialog;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;
import com.weds.weddfix.category.interfaces.CategoryTriggerWithString;
import com.weds.weddfix.matrimony.common.MtnyConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "jyvNLR1b9ZzIubHVcnw0GJ2fg";
    private static final String TWITTER_SECRET = "aOGmqNlmUzBurLiSSdXW0zlR5zHdcIovfoVPt7GGIHgEoIXYcg";
    private Button loginButton, signUpButton;
    private Button twitterButton;
    private TextView forgotPass;
    private EditText edEmail, edPassword;
    private String email, password, emailRegEx, urlFacebookButton;
    private Pattern pattern;
    private Context mContext;
    private CallbackManager callbackManager;
    private LoginButton fbButton;
    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private CategoryStorePreference categoryStorePreference;
    private CategoryForgotPasswordDialog alertForgotDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        initWidgets();

        callbackManager = CallbackManager.Factory.create();

        fbButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                GraphRequest request = GraphRequest.newMeRequest(
                        accessToken,
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                try {

                                    TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                                    String ip = mngr.getDeviceId();
                                    urlFacebookButton = CategoryApiClient.CATEGORY_BASE_URL + "" + object.getString("email") + "&profile=" + object.getString("profile") + "&imei=" + ip + "&device=100";
                                    fbLogin();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,profile,email");
                request.setParameters(parameters);
                request.executeAsync();

                if (profile != null) {

                    // Toast.makeText(Signin.this, mtny_home_profile.getName(), Toast.LENGTH_SHORT).show();

                } else {

                    // Toast.makeText(Signin.this, "mtny_home_profile empty", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onCancel() {
                // Toast.makeText(Signin.this, "cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                //  Toast.makeText(Signin.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        // Regex for a valid email address
        emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";
    }

    private void initWidgets() {
        twitterButton = (Button) findViewById(R.id.twitter_button);
        signUpButton = (Button) findViewById(R.id.sign_up_button);
        loginButton = (Button) findViewById(R.id.login_button);
        forgotPass = (TextView) findViewById(R.id.txt_forgot_pass);
        edEmail = (EditText) findViewById(R.id.ed_login_email);
        edPassword = (EditText) findViewById(R.id.ed_login_password);
        fbButton = (LoginButton) findViewById(R.id.facebook_login_button);

        loginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        forgotPass.setOnClickListener(this);

        mContext = this;
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
        categoryStorePreference = new CategoryStorePreference(mContext);

        fbButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        fbButton.setReadPermissions(Arrays.asList("email", "user_photos", "public_profile"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:

                if (CategoryCommonMethods.isNetworkConnectionAvailable(this)) {

                    email = edEmail.getText().toString();
                    if (email.trim().equals("")) {
                        CategoryCommonMethods.showToast(this, "Please Enter the email");
                        return;
                    }
                    pattern = Pattern.compile(emailRegEx);
                    Matcher matcher = pattern.matcher(email);
                    if (!matcher.find()) {
                        CategoryCommonMethods.showToast(this, "Please Enter the valid email");
                        return;
                    }
                    password = edPassword.getText().toString();
                    if (password.trim().equals("")) {
                        CategoryCommonMethods.showToast(this, "Please Enter the Password");
                        return;
                    }
                    userLogin();
                } else {
                    CategoryCommonMethods.showToast(LoginActivity.this, CategoryConstants.TOAST_CONNECTION_ERROR);
                }
                break;

            case R.id.sign_up_button:
                Intent signUpButton = new Intent(LoginActivity.this, CategoryRegisterActivity.class);
                startActivity(signUpButton);
                break;

            case R.id.txt_forgot_pass:

                alertForgotDialog = new CategoryForgotPasswordDialog(this, "", new CategoryTriggerWithString() {
                    @Override
                    public void initTrigger(boolean Trigger, String forgotPwdEmailId) {
                        if (forgotPwdEmailId != null) {
                            if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
                                sendForgotPassword(forgotPwdEmailId);
                            } else {
                                CategoryCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, LoginActivity.this);
                            }
                        } else {
                            CategoryCommonMethods.showToast("Please enter your Registered mailId", mContext);
                        }
                    }
                });
                alertForgotDialog.showDialog();
                break;
        }
    }

    public void userLogin() {
        pDialog = new CategoryActivityIndicator(this);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseElementModel> call = apiService.getCateUserProfileData(email, password);
        call.enqueue(new Callback<CategoryCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseElementModel> call, retrofit2.Response<CategoryCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().categoryUserProfileDataModel.status;
                    String message = response.body().categoryUserProfileDataModel.message;

                    if (status.equals(CategoryConstants.SUCCESS)) {

                        CategoryUserProfileDataModel categoryUserProfileDataModel = new CategoryUserProfileDataModel();
                        String baseUrl = response.body().categoryUserProfileDataModel.baseUrl;
                        categoryUserProfileDataModel.baseUrl = baseUrl;

                        CategoryUserLoginDetailsDataModel categoryUserLoginDetailsDataModel = new CategoryUserLoginDetailsDataModel();
                        String userId = response.body().categoryUserLoginDetailsDataModel.userId;
                        String email = response.body().categoryUserLoginDetailsDataModel.email;
                        String mobile = response.body().categoryUserLoginDetailsDataModel.mobile;
                        String role = response.body().categoryUserLoginDetailsDataModel.role;
                        String userStatus = response.body().categoryUserLoginDetailsDataModel.status;
                        String passwordHash = response.body().categoryUserLoginDetailsDataModel.passwordHash;
                        String userName = response.body().categoryUserLoginDetailsDataModel.userName;
                        String accountType = response.body().categoryUserLoginDetailsDataModel.accountType;
                        String verifyMobileNumber = response.body().categoryUserLoginDetailsDataModel.verifyMobileNumber;
                        String verifyEmailId = response.body().categoryUserLoginDetailsDataModel.verifyEmailId;

                        categoryUserLoginDetailsDataModel.userName = userName;
                        categoryUserLoginDetailsDataModel.userId = userId;
                        categoryUserLoginDetailsDataModel.email = email;
                        categoryUserLoginDetailsDataModel.mobile = mobile;

                        CategorySingleton.getInstance().categoryUserProfileDataModel = categoryUserProfileDataModel;

                        categoryStorePreference.setString(CategoryConstants.U_FNAME, userName);
                        categoryStorePreference.setString(CategoryConstants.U_EMAIL, email);
                        categoryStorePreference.setString(CategoryConstants.USER_ID, userId);
                        categoryStorePreference.setString(CategoryConstants.U_PHONE, mobile);
                        categoryStorePreference.setString(CategoryConstants.U_STATUS, userStatus);
                        categoryStorePreference.setBoolean(CategoryConstants.U_LOGGED, true);




                        Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(homeIntent);

                        if (pDialog != null)
                            pDialog.dismiss();

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CategoryCommonResponseElementModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());

            }
        });
    }


    private void sendForgotPassword(String registeredEmailId) {
        pDialog = new CategoryActivityIndicator(this);
        pDialog.setLoadingText("Sending");
        pDialog.show();

        Call<CategoryCommonResponseElementModel> call = apiService.sendCategoryForgotPassword(registeredEmailId);
        call.enqueue(new Callback<CategoryCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseElementModel> call, Response<CategoryCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().categoryForgotPwdCommonResponseModel.status;
                    String message = response.body().categoryForgotPwdCommonResponseModel.message;
                    if (status.equals(MtnyConstants.SUCCESS)) {
                        CategoryCommonMethods.showToast(message, LoginActivity.this);
                        alertForgotDialog.dismiss();
                    } else {
                        CategoryCommonMethods.showToast(message, LoginActivity.this);
                    }
                } else {
                    CategoryCommonMethods.showToast(CategoryConstants.TOAST_CONNECTION_ERROR, LoginActivity.this);
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<CategoryCommonResponseElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }


    private void fbLogin() {


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
