package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.category.activity.LoginActivity;

import java.util.List;

public class InboxHomeAdapter extends RecyclerView.Adapter<InboxHomeAdapter.ViewHolder> {


    Context context;
    List<String> inbox_list;
    List<Integer> img_list;

    public InboxHomeAdapter(Context context, List<String> inbox_list, List<Integer> img_list) {

        this.context = context;
        this.img_list = img_list;
        this.inbox_list = inbox_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custom_inbox_home, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.imageView.setImageResource(img_list.get(position));
        holder.textView.setText(inbox_list.get(position));

    }


    @Override
    public int getItemCount() {
        return inbox_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.img_custom_inbox_home);
            textView = (TextView) itemView.findViewById(R.id.txt_custom_inbox_home);

            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            if (position == 0) {

                Intent ii = new Intent(context, LoginActivity.class);
                context.startActivity(ii);
            }
            if (position == 1) {
                Intent inte = new Intent(context,LoginActivity.class );
                context.startActivity(inte);
            }
            if (position == 2) {
                Intent inte = new Intent(context,LoginActivity.class );
                context.startActivity(inte);
            }
            if (position == 4) {
                Intent inte = new Intent(context,LoginActivity.class );
                context.startActivity(inte);
            }
            if (position == 5) {
                Intent inte = new Intent(context,LoginActivity.class );
                context.startActivity(inte);
            }
        }
    }
}

