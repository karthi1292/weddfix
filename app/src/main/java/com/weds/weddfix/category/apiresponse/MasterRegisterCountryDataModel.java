package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;
import com.weds.weddfix.category.apiresponse.CategoryRegisterCountryDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class MasterRegisterCountryDataModel {

    @SerializedName("country")
    public List<CategoryRegisterCountryDataModel> categRegisterCountryDataModelList=new ArrayList<>();

    public MasterRegisterCountryDataModel() {
    }

    public List<CategoryRegisterCountryDataModel> getcategRegisterCityDataModelList() {
        return categRegisterCountryDataModelList;
    }

    public void setCategRegisterCountryDataModelList(List<CategoryRegisterCountryDataModel> categRegisterCityDataModelList) {
        this.categRegisterCountryDataModelList = categRegisterCityDataModelList;
    }



}
