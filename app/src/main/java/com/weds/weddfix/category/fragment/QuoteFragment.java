package com.weds.weddfix.category.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.timessquare.CalendarCellDecorator;
import com.squareup.timessquare.CalendarPickerView;
import com.squareup.timessquare.DefaultDayViewAdapter;
import com.weds.weddfix.R;
import com.weds.weddfix.category.model.Events;
import com.weds.weddfix.category.adapter.EventAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * Created by Acer on 31-10-2016.
 */
public class QuoteFragment extends Fragment {
    EditText fromdate, todate;
    Button button, frombutton, tobutton;
    CardView c1, c2, c3, c4;
    ProgressDialog progressDialog;
    Spinner spinner_main_cat, spinner_sub_cat, spinner_third_sub, spinner_fourth_sub;
    private int mYear, mMonth, mDay;
    private CalendarPickerView calendar;
    private String from = "";
    private String to = "";


    public QuoteFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_quote, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        c1 = (CardView) view.findViewById(R.id.card_product_post1);
        c2 = (CardView) view.findViewById(R.id.card_product_post2);
        c4 = (CardView) view.findViewById(R.id.card_product_post4);
        button = (Button) view.findViewById(R.id.btn_quote_fragment);
        spinner_main_cat = (Spinner) view.findViewById(R.id.quote_spinner);
        spinner_sub_cat = (Spinner) view.findViewById(R.id.quote_spinner1);
        spinner_third_sub = (Spinner) view.findViewById(R.id.quote_spinner2);
        spinner_fourth_sub = (Spinner) view.findViewById(R.id.quote_spinner3);
       /* frombutton = (Button) view.findViewById(R.id.fromdateb);
        tobutton = (Button) view.findViewById(R.id.todateb);
        fromdate = (EditText) view.findViewById(R.id.fromdate);
        todate = (EditText) view.findViewById(R.id.todate);*/

        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);

        calendar = (CalendarPickerView)view. findViewById(R.id.calendar_view);
        calendar.init(lastYear.getTime(), nextYear.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.SINGLE) //
                .withSelectedDate(new Date());
        initButtonListeners(nextYear, lastYear);
        /*frombutton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                fromdate();
            }
        });

        tobutton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                todate();
            }
        });*/


        final String[] select_qualification = {
                "Select Your Events", "Wedding", "Reception", "Roka", "Mehendhi"};

        ArrayList<Events> listVOs = new ArrayList<>();

        for (int i = 0; i < select_qualification.length; i++) {
            Events stateVO = new Events();
            stateVO.setTitle(select_qualification[i]);
            stateVO.setSelected(false);
            listVOs.add(stateVO);
        }
        EventAdapter myAdapter = new EventAdapter(getActivity(), 0,
                listVOs);
        spinner_fourth_sub.setAdapter(myAdapter);


        spinner_main_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_sub_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {


                } catch (IndexOutOfBoundsException e) {

                }


                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                //category_attributes.php?first_cat=1&second_cat=6
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                c4.setVisibility(View.GONE);

            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PriceFragment fragment = new PriceFragment();
                FragmentManager fragmentManager1 = getActivity().getSupportFragmentManager();
                fragmentManager1.beginTransaction().replace(R.id.category_list_frame_layout, fragment).commit();


            }
        });
        spinner_third_sub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                c4.setVisibility(View.GONE);

            }
        });
       /* spinner_fourth_sub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


        return view;
    }


   /* spinner_post_color.setAdapter(new SpinnerAdapter(getActivity(), list_colors));
    spinner_fourth_sub.setAdapter(new SpinnerAdapter(getActivity(), list_fourth_name_cat));

    spinner_third_sub.setAdapter(new SpinnerAdapter(getActivity(), list_third_name_cat));
    spinner_sub_cat.setAdapter(new SpinnerAdapter(getActivity(), list_second_name_cat));

    spinner_main_cat.setAdapter(new SpinnerAdapter(getActivity(), list_main_name_cat));

    recyclerView_optional.setAdapter(new Adapter_Product_post_optional(getActivity(), list_optinal_name.size(), attributesList));
*/

    class SpinnerAdapter extends ArrayAdapter<String> {

        Context context;
        TextView txt_spinner;
        List<String> list_main_cat;

        public SpinnerAdapter(Context context, List<String> list_main_cat) {
            super(context, R.layout.custom_spinner_layout, R.id.txt_custom_spinner, list_main_cat);
            this.context = context;
            this.list_main_cat = list_main_cat;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(context).inflate(R.layout.custom_spinner_layout, parent, false);

            txt_spinner = (TextView) convertView.findViewById(R.id.txt_custom_spinner);
            txt_spinner.setText(list_main_cat.get(position));

            return convertView;
        }
    }


   /* public void fromdate() {
        // Process to get Current Date
        final Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // Launch Date Picker Dialog
        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        fromdate.setText(year + "-"
                                + (monthOfYear + 1) + "-" + dayOfMonth);
                        from = year + "-"
                                + (monthOfYear + 1) + "-" + dayOfMonth;

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    public void todate() {
        // Process to get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // Launch Date Picker Dialog
        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                  new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        todate.setText(year + "-"
                                + (monthOfYear + 1) + "-" + dayOfMonth);

                        to = year + "-"
                                + (monthOfYear + 1) + "-" + dayOfMonth;

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }*/
    private void initButtonListeners(final Calendar nextYear, final Calendar lastYear) {


        calendar.setCustomDayView(new DefaultDayViewAdapter());
        Calendar today = Calendar.getInstance();
        ArrayList<Date> dates = new ArrayList<Date>();
        for (int i = 0; i < 5; i++) {
            today.add(Calendar.DAY_OF_MONTH, 3);
            dates.add(today.getTime());
        }
        calendar.setDecorators(Collections.<CalendarCellDecorator>emptyList());
        calendar.init(new Date(), nextYear.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE) //
                .withSelectedDates(dates);

    }
}


