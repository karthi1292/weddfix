package com.weds.weddfix.category.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.adapter.CategoryDetailsPagerAdapter;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseEnquiryElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorBookingElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorDetailsElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorWishListShortElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseVendorWishListUnShortElementModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorBookingDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorDetailsDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorDetailsPhotoGalleryDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorDetailsReviewsDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorDetailsVideoUrlsDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorWishListShortDataModel;
import com.weds.weddfix.category.apiresponse.CategoryVendorWishListUnShortDataModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.dialog.BookingDialog;
import com.weds.weddfix.category.dialog.ServiceReviewDialog;
import com.weds.weddfix.category.fragment.CategoryAboutUsFragment;
import com.weds.weddfix.category.fragment.CategoryGalleryFragment;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;
import com.weds.weddfix.category.interfaces.CategoryTriggerWithObject;
import com.weds.weddfix.category.model.CallbackBookModel;
import com.weds.weddfix.matrimony.common.MtnyConstants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView txtviewCompanyLocation, txtviewCompanyPrice, txtviewCustomerReviews;
    private FloatingActionButton fabBtnConfirmBooking, fabBtnAddToWishList;
    private String vendorId, categoryName, vendorCompanyName, vendorMobile, vendorPhone, vendorEmail, vendorName, shortListVendorId, unShortListVendorId, vendorCompanyImage, vendorProfilePic, reviewCount;
    private CategoryActivityIndicator pDialog;
    private CategoryRetrofitApiInterface apiService;
    private Context mContext;
    private ImageView imageCompany;
    private CategoryStorePreference categoryStorePreference;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private boolean categoryLoggedIn;
    // private EditText edEnquiryName, edEnquiryEmail, edEnquirySubject, edEnquiryMsg;
    private String enquiryName, enquiryEmail, enquirySubject, enquiryMsg;
    private CollapsingToolbarLayout collapsingToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_details);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        intWidgets();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
            getVendorDetails();
        } else {
            CategoryCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
        }
    }

    public void intWidgets() {

        mContext = this;
        vendorId = getIntent().getStringExtra(CategoryConstants.VENDOR_ID);
        categoryStorePreference = new CategoryStorePreference(mContext);
        categoryLoggedIn = categoryStorePreference.getBooleanValue(CategoryConstants.U_LOGGED);
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);

        viewPager = (ViewPager) findViewById(R.id.vendor_details_viewpager);
        fabBtnAddToWishList = (FloatingActionButton) findViewById(R.id.fab_btn_add_wishlist);
        fabBtnConfirmBooking = (FloatingActionButton) findViewById(R.id.fab_btn_confirm_booking);

      /*  recyclerViewFeedback = (RecyclerView) findViewById(R.vendorId.recycler_feedback);
        buttonVendorQuote = (Button) findViewById(R.vendorId.btn_hall_get_quote);
        buttonVendorShare = (Button) findViewById(R.vendorId.btn_hall_share);
        edEnquiryName = (EditText) findViewById(R.vendorId.edit_enquiry_name);
        edEnquiryEmail = (EditText) findViewById(R.vendorId.edit_enquiry_email);
        edEnquirySubject = (EditText) findViewById(R.vendorId.edit_enquiry_subject);
        edEnquiryMsg = (EditText) findViewById(R.vendorId.edit_enquiry_msg);
        buttonEnquiryMsg = (Button) findViewById(R.vendorId.btn_enquiry_msg);
*/
        txtviewCustomerReviews = (TextView) findViewById(R.id.txtview_customer_reviews);
        txtviewCompanyPrice = (TextView) findViewById(R.id.txtview_company_price);
        txtviewCompanyLocation = (TextView) findViewById(R.id.txtview_company_location);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        imageCompany = (ImageView) findViewById(R.id.imageview_company);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        // collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_tollbar);
        // collapsingToolbar.setTitleEnabled(false);

       /* buttonVendorQuote.setOnClickListener(this);
        buttonVendorShare.setOnClickListener(this);
        buttonEnquiryMsg.setOnClickListener(this);*/

        fabBtnConfirmBooking.setOnClickListener(this);
        fabBtnAddToWishList.setOnClickListener(this);
        txtviewCustomerReviews.setOnClickListener(this);

        //  edEnquiryName.setText(categoryStorePreference.getStringValue(CategoryConstants.U_FNAME));
        //   edEnquiryEmail.setText(categoryStorePreference.getStringValue(CategoryConstants.U_EMAIL));

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_btn_confirm_booking:
                BookingDialog bookingDialog = new BookingDialog(mContext, new CategoryTriggerWithObject() {
                    @Override
                    public void initTrigger(boolean Trigger, Object obj) {
                        CallbackBookModel callbackBookModel = (CallbackBookModel) obj;

                        if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
                            vendorBooking(callbackBookModel.sendEmail, callbackBookModel.needCallback);
                        } else {
                            CategoryCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
                        }
                    }
                });
                bookingDialog.showDialog();
                break;
            case R.id.fab_btn_add_wishlist:
                if (categoryLoggedIn) {

                    if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
                        likeWishlist();
                    } else {
                        CategoryCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
                    }
                } else {
                    Toast.makeText(mContext, "Please Login to continue", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.txtview_customer_reviews:
                ServiceReviewDialog serviceReviewDialog = new ServiceReviewDialog(mContext);
                serviceReviewDialog.showDialog();

                break;

          /*  case R.vendorId.btn_hall_get_quote:
                startActivity(new Intent(CategoryDetailsActivity.this, QuoteActivity.class));
                break;


             case R.vendorId.btn_enquiry_msg:
                if (CategoryCommonMethods.isNetworkConnectionAvailable(this)) {
                    if (edEnquiryName.getText().toString().isEmpty()) {

                        Toast.makeText(mContext, "Enter the Name", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edEnquiryEmail.getText().toString().isEmpty()) {
                        Toast.makeText(mContext, "Enter the Email", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edEnquirySubject.getText().toString().isEmpty()) {
                        Toast.makeText(mContext, "Enter the Subject", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edEnquiryMsg.getText().toString().isEmpty()) {
                        Toast.makeText(mContext, "Enter the Message", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    enquiryName = edEnquiryName.getText().toString();
                    enquiryEmail = edEnquiryEmail.getText().toString();
                    enquirySubject = edEnquirySubject.getText().toString();
                    enquiryMsg = edEnquiryMsg.getText().toString();


                    sendEnquiry();


                } else {
                    CategoryCommonMethods.showToast(this, "Please check the internet connection");
                }

                break;*/
        }
    }

    private void setupViewPager(ViewPager viewPager) {

        CategoryDetailsPagerAdapter adapter = new CategoryDetailsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CategoryGalleryFragment(), "Gallery");
        adapter.addFragment(new CategoryAboutUsFragment(vendorId), "About Us");

        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.category_detail_menu_items, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.category_share_menu) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Weddfix-Free android app.Useful for you & your family-http://www.weddfix.com/");
            sendIntent.setType("text/plain");
            Intent.createChooser(sendIntent, "Share via");
            startActivity(sendIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void getVendorDetails() {
        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseVendorDetailsElementModel> call = apiService.getVendorDetails(vendorId, categoryStorePreference.getStringValue(CategoryConstants.USER_ID));
        call.enqueue(new Callback<CategoryCommonResponseVendorDetailsElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseVendorDetailsElementModel> call, Response<CategoryCommonResponseVendorDetailsElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    CategoryVendorDetailsDataModel categoryVendorDetailsDataModel = response.body().categoryVendorDetailsDataModel;
                    ArrayList<CategoryVendorDetailsPhotoGalleryDataModel> categoryVendorDetailsPhotoGalleryDataModels = response.body().categoryVendorDetailsPhotoGalleryDataModel;
                    ArrayList<CategoryVendorDetailsVideoUrlsDataModel> categoryVendorDetailsVideoUrlsDataModels = response.body().categoryVendorDetailsVideoUrlsDataModel;
                    ArrayList<CategoryVendorDetailsReviewsDataModel> categoryVendorDetailsReviewsDataModels = response.body().categoryVendorDetailsReviewsDataModel;

                    //vId = categoryVendorDetailsDataModel.id;
                    categoryName = categoryVendorDetailsDataModel.categoryName;
                    vendorCompanyName = categoryVendorDetailsDataModel.companyName;
                    vendorMobile = categoryVendorDetailsDataModel.vendorMobile;
                    vendorPhone = categoryVendorDetailsDataModel.vendorPhone;
                    vendorEmail = categoryVendorDetailsDataModel.vendorEmail;
                    vendorName = categoryVendorDetailsDataModel.vendorFullName;
                    vendorCompanyImage = categoryVendorDetailsDataModel.fileName;
                    vendorProfilePic = categoryVendorDetailsDataModel.vendorProfilePicture;
                    reviewCount = String.valueOf(categoryVendorDetailsReviewsDataModels.size());

                    CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsDataModel = categoryVendorDetailsDataModel;
                    CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsPhotoGalleryDataModel = categoryVendorDetailsPhotoGalleryDataModels;
                    CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsVideoUrlsDataModel = categoryVendorDetailsVideoUrlsDataModels;
                    CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsReviewsDataModel = categoryVendorDetailsReviewsDataModels;

                    loadVendorDetails(categoryVendorDetailsDataModel);

                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<CategoryCommonResponseVendorDetailsElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }


    private void loadVendorDetails(CategoryVendorDetailsDataModel categoryVendorDetailsDataModel) {

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolbar.setTitle(vendorCompanyName);

        txtviewCompanyLocation.setText(categoryVendorDetailsDataModel.location);
        txtviewCompanyPrice.setText("Rs." + categoryVendorDetailsDataModel.price);

        if (!reviewCount.equals("0") || reviewCount.equals(""))
            txtviewCustomerReviews.setText(reviewCount + "reviews");
        else
            txtviewCustomerReviews.setText("No reviews");

        Glide.with(mContext).load(vendorCompanyImage).into(imageCompany);

        setupViewPager(viewPager);
    }

    private void likeWishlist() {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseVendorWishListShortElementModel> call = apiService.getWishListShortList(vendorId, categoryStorePreference.getStringValue(CategoryConstants.USER_ID));
        call.enqueue(new Callback<CategoryCommonResponseVendorWishListShortElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseVendorWishListShortElementModel> call, Response<CategoryCommonResponseVendorWishListShortElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    String status = response.body().categoryVendorWishListShortDataModel.status;
                    String message = response.body().categoryVendorWishListShortDataModel.message;
                    if (status.equals(CategoryConstants.SUCCESS)) {

                        CategoryVendorWishListShortDataModel categoryVendorWishListShortDataModel = response.body().categoryVendorWishListShortDataModel;
                        shortListVendorId = categoryVendorWishListShortDataModel.shortlistedVendorId;
                        String shortListId = categoryVendorWishListShortDataModel.shortlistedId;
                        categoryVendorWishListShortDataModel.shortlistedVendorId = shortListVendorId;
                        categoryVendorWishListShortDataModel.shortlistedId = shortListId;

                        fabBtnAddToWishList.setImageResource(R.drawable.added_wishlist);

                        if (pDialog != null)
                            pDialog.dismiss();

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CategoryCommonResponseVendorWishListShortElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    private void unLikeWishlist() {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseVendorWishListUnShortElementModel> call = apiService.getWishListUnShortList(shortListVendorId);
        call.enqueue(new Callback<CategoryCommonResponseVendorWishListUnShortElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseVendorWishListUnShortElementModel> call, Response<CategoryCommonResponseVendorWishListUnShortElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    String status = response.body().categoryVendorWishListUnShortDataModel.status;
                    String message = response.body().categoryVendorWishListUnShortDataModel.message;
                    if (status.equals(CategoryConstants.SUCCESS)) {

                        CategoryVendorWishListUnShortDataModel categoryVendorWishListUnShortDataModel = response.body().categoryVendorWishListUnShortDataModel;
                        unShortListVendorId = categoryVendorWishListUnShortDataModel.shortlistedVendorId;
                        String shortListId = categoryVendorWishListUnShortDataModel.shortlistedId;
                        categoryVendorWishListUnShortDataModel.shortlistedVendorId = unShortListVendorId;
                        categoryVendorWishListUnShortDataModel.shortlistedId = shortListId;

                        if (pDialog != null)
                            pDialog.dismiss();

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<CategoryCommonResponseVendorWishListUnShortElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    private void vendorBooking(String sendEmail, String needCallBack) {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseVendorBookingElementModel> call = apiService.getVendorBooking(vendorId, vendorCompanyName, vendorMobile, vendorPhone, vendorEmail, categoryStorePreference.getStringValue(CategoryConstants.U_WEDDING_DATE), categoryStorePreference.getStringValue(CategoryConstants.U_FNAME), categoryStorePreference.getStringValue(CategoryConstants.U_PHONE), categoryStorePreference.getStringValue(CategoryConstants.U_EMAIL), categoryStorePreference.getStringValue(CategoryConstants.USER_ID), sendEmail, needCallBack);
        call.enqueue(new Callback<CategoryCommonResponseVendorBookingElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseVendorBookingElementModel> call, Response<CategoryCommonResponseVendorBookingElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    String status = response.body().categoryVendorBookingDataModel.status;
                    String message = response.body().categoryVendorBookingDataModel.message;
                    if (status.equals(CategoryConstants.SUCCESS)) {

                        CategoryVendorBookingDataModel categoryVendorBookingDataModel = response.body().categoryVendorBookingDataModel;
                        String mobile = categoryVendorBookingDataModel.mobile;
                        String verifyMobileNum = categoryVendorBookingDataModel.verifyMobileNumber;
                        categoryVendorBookingDataModel.mobile = mobile;
                        categoryVendorBookingDataModel.verifyMobileNumber = verifyMobileNum;

                        if (pDialog != null)
                            pDialog.dismiss();

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        if (pDialog != null)
                            pDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CategoryCommonResponseVendorBookingElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }


    private void sendEnquiry() {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseEnquiryElementModel> call = apiService.getSendEnquiry(enquiryName, enquiryEmail, enquirySubject, enquiryMsg);
        call.enqueue(new Callback<CategoryCommonResponseEnquiryElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseEnquiryElementModel> call, Response<CategoryCommonResponseEnquiryElementModel> response) {

                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().categoryEnquiryDataModel.status;
                    String message = response.body().categoryEnquiryDataModel.message;

                    if (status.equals(CategoryConstants.SUCCESS)) {

                        Intent intent = new Intent(mContext, HomeActivity.class);
                        startActivity(intent);
                        finish();
                        if (pDialog != null)
                            pDialog.dismiss();
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override

            public void onFailure(Call<CategoryCommonResponseEnquiryElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
            }
        });
    }
}