package com.weds.weddfix.category.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.weds.weddfix.R;


/**
 * Created by Acer on 31-10-2016.
 */
public class PriceFragment extends Fragment {

    Button button;
    TextView ed_packageName, ed_packagePrice, ed_packageDescription;
    String pName, pPrice, pDescription;


    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;

    public PriceFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_price, container, false);


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        progressDialog = new ProgressDialog(getActivity());
        button = (Button) view.findViewById(R.id.btn_price_fragment);
        ed_packageName = (TextView) view.findViewById(R.id.ed_service_package_name);
        ed_packagePrice = (TextView) view.findViewById(R.id.ed_service_package_price);
        ed_packageDescription = (TextView) view.findViewById(R.id.ed_service_package_description);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pName = ed_packageName.getText().toString();
                pPrice = ed_packagePrice.getText().toString();
                pDescription = ed_packageDescription.getText().toString();

            }
        });


        return view;
    }


}
