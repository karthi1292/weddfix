package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 08-03-2017.
 */

public class CategoryOrderHistoryDataModel {

    @SerializedName("id")
    public String id;
    @SerializedName("categoryName")
    public String categoryName;
    @SerializedName("companyName")
    public String companyName;
    @SerializedName("address")
    public String address;
    @SerializedName("location")
    public String location;
    @SerializedName("cityName")
    public String cityName;
    @SerializedName("stateName")
    public String stateName;
    @SerializedName("countryName")
    public String countryName;
    @SerializedName("pincode")
    public String pincode;
    @SerializedName("price")
    public String price;
    @SerializedName("maxRating")
    public String maxRating;
    @SerializedName("maxUsersRating")
    public String maxUsersRating;
    @SerializedName("minRating")
    public String minRating;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("bookingTotalCount")
    public String bookingTotalCount;
    @SerializedName("bookingId")
    public String bookingId;
    @SerializedName("contactName")
    public String contactName;
    @SerializedName("contactEmail")
    public String contactEmail;
    @SerializedName("contactMobile")
    public String contactMobile;


}
