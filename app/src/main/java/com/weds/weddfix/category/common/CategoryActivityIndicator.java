package com.weds.weddfix.category.common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class CategoryActivityIndicator extends Dialog {

	TextView tv;
	LinearLayout lay;
	String loadingText = "Loading...";

	public CategoryActivityIndicator(Context context) {
		super(context);
		init(context);
	}

	public CategoryActivityIndicator(Context context, int theme) {
		super(context, theme);
	}

	private void init(Context context) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		if (android.os.Build.VERSION.SDK_INT >= 19) {
			getWindow().getDecorView().setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
							| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
							| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
							| View.SYSTEM_UI_FLAG_FULLSCREEN
							| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
							| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
		}
		getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		this.lay = new LinearLayout(context);
		this.lay.setOrientation(LinearLayout.VERTICAL);
		this.lay.addView(new ProgressBar(context), new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		this.tv = new TextView(context);
		this.setLoadingText(loadingText);
		this.tv.setTextColor(Color.WHITE);
		this.lay.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
		this.lay.addView(tv);
		this.addContentView(lay, new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		this.setCancelable(false);
	}

	public void setLoadingText(String loadingText) {
		this.tv.setText(loadingText);
	}
}
