package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 07-03-2017.
 */

public class CategoryUserProfileDetailModel {

    @SerializedName("id")
    public String userId;
    @SerializedName("fullName")
    public String fullName;
    @SerializedName("email")
    public String email;
    @SerializedName("passwordHash")
    public String passwordHash;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("countryId")
    public String countryId;
    @SerializedName("stateId")
    public String stateId;
    @SerializedName("cityId")
    public String cityId;
    @SerializedName("pincode")
    public String pincode;
    @SerializedName("description")
    public String description;
    @SerializedName("weddingDateStr")
    public String weddingDate;
    @SerializedName("profilePictureId")
    public String profilePictureId;
    @SerializedName("country")
    public String country;
    @SerializedName("state")
    public String state;
    @SerializedName("city")
    public String city;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("websiteUrl")
    public String websiteUrl;
    @SerializedName("facebookUrl")
    public String facebookUrl;
    @SerializedName("twitterUrl")
    public String twitterUrl;
    @SerializedName("linkedinUrl")
    public String linkedinUrl;
    @SerializedName("pinterestUrl")
    public String pinterestUrl;
    @SerializedName("instagramUrl")
    public String instagramUrl;

}
