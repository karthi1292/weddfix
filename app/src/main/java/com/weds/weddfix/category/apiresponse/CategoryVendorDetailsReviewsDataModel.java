package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 03-03-2017.
 */

public class CategoryVendorDetailsReviewsDataModel {

    @SerializedName("id")
    public String id;
    @SerializedName("maxRating")
    public String maxRating;
    @SerializedName("profile")
    public String name;
    @SerializedName("email")
    public String email;
    @SerializedName("reviewTitle")
    public String reviewTitle;
    @SerializedName("review")
    public String review;
    @SerializedName("createdDate")
    public String createdDate;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("minRating")
    public String minRating;


}
