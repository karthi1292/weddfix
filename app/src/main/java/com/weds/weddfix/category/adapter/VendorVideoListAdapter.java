package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.youtube.player.YouTubePlayer;
import com.weds.weddfix.R;
import com.weds.weddfix.category.activity.YouTubePlayerActivity;
import com.weds.weddfix.category.common.CategorySingleton;
import com.weds.weddfix.category.enums.Orientation;
import com.weds.weddfix.category.youtube.YoutubeUrlParser;

/**
 * Created by Acer on 03-11-2016.
 */
public class VendorVideoListAdapter extends RecyclerView.Adapter<VendorVideoListAdapter.MyViewHolder> {

    private Context mContext;
    private YouTubePlayer.PlayerStyle playerStyle;
    private Orientation orientation;
    private boolean showAudioUi;
    private boolean showFadeAnim;

    public VendorVideoListAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_vendor_customer_videos, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        String videoThumnail = CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsVideoUrlsDataModel.get(position).categoryVideoImage;
        String videoUrl = CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsVideoUrlsDataModel.get(position).categoryVideoUrl;

        if (videoUrl != null) {

            final String videoId = YoutubeUrlParser.getVideoId(videoUrl);
            playerStyle = YouTubePlayer.PlayerStyle.DEFAULT;
            orientation = Orientation.AUTO;
            showAudioUi = true;
            showFadeAnim = true;

            Glide.with(mContext).load(videoThumnail)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.wed_logo).into(holder.videoThumbnail);

            holder.videoThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playVideo(videoId);
                }
            });
        }
    }

    @Override
    public int getItemCount() {

        return (null != CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsVideoUrlsDataModel ? CategorySingleton.getInstance().categoryCommonResponseVendorDetailsElementModel.categoryVendorDetailsVideoUrlsDataModel.size() : 0);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView videoThumbnail;


        public MyViewHolder(View itemView) {
            super(itemView);
            videoThumbnail = (ImageView) itemView.findViewById(R.id.vendor_cus_img);
        }
    }

    private void playVideo(String videoId) {
        if (videoId != null) {
            Intent intent = new Intent(mContext, YouTubePlayerActivity.class);
            intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, videoId);
            intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, playerStyle);
            intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, orientation);
            intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, showAudioUi);
            intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);
            if (showFadeAnim) {
                intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_ENTER, R.anim.abc_fade_in);
                intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_EXIT, R.anim.abc_fade_out);
            } else {
                intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_ENTER, R.anim.abc_popup_enter);
                intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_EXIT, R.anim.abc_popup_exit);
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        } else {
            Toast.makeText(mContext.getApplicationContext(), "Video does not exist!", Toast.LENGTH_SHORT).show();
        }
    }
}