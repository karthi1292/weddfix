package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rameshmuthu on 03-03-2017.
 */

public class CategoryCommonResponseVendorDetailsElementModel {

    @SerializedName("vendorDetails")
    public CategoryVendorDetailsDataModel categoryVendorDetailsDataModel;

    @SerializedName("photoGallery")
    public ArrayList<CategoryVendorDetailsPhotoGalleryDataModel> categoryVendorDetailsPhotoGalleryDataModel;

    @SerializedName("reviews")
    public ArrayList<CategoryVendorDetailsReviewsDataModel> categoryVendorDetailsReviewsDataModel;

    @SerializedName("youtubeVideoUrls")
    public ArrayList<CategoryVendorDetailsVideoUrlsDataModel> categoryVendorDetailsVideoUrlsDataModel;

}
