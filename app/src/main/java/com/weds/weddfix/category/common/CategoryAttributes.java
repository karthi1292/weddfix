package com.weds.weddfix.category.common;

import java.util.ArrayList;

/**
 * Created by accer on 30/10/16.
 */

public class CategoryAttributes {

    private String name;
    private ArrayList<String> values, ids;

    public CategoryAttributes() {
    }

    public CategoryAttributes(String name, ArrayList<String> values, ArrayList<String> ids) {
        this.name = name;
        this.values = values;
        this.ids = ids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getValues() {
        return values;
    }

    public void setValues(ArrayList<String> values) {
        this.values = values;
    }

    public ArrayList<String> getIds() {
        return ids;
    }

    public void setIds(ArrayList<String> ids) {
        this.ids = ids;
    }
}
