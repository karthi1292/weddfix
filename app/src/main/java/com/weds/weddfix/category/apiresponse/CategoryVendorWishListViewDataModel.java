package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 03-03-2017.
 */

public class CategoryVendorWishListViewDataModel {

    @SerializedName("id")
    public String id;
    @SerializedName("categoryName")
    public String categoryName;
    @SerializedName("companyName")
    public String companyName;
    @SerializedName("address")
    public String address;
    @SerializedName("location")
    public String location;
    @SerializedName("cityName")
    public String cityName;
    @SerializedName("stateName")
    public String stateName;
    @SerializedName("countryName")
    public String countryName;
    @SerializedName("pincode")
    public String pincode;
    @SerializedName("price")
    public String price;
    @SerializedName("maxRating")
    public String maxRating;
    @SerializedName("maxUsersRating")
    public String maxUsersRating;
    @SerializedName("minRating")
    public String minRating;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("shortlistedTotalCount")
    public String shortlistedTotalCount;
    @SerializedName("shortlistedId")
    public String shortlistedId;





}
