package com.weds.weddfix.category.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.category.common.CategoryAttributes;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Acer on 25-06-2016.
 */
public class QuoteOptionalAdapter extends RecyclerView.Adapter<QuoteOptionalAdapter.ViewHolder> {

    Context context;
    int i;
    String b;
    //List<String> l_name, l_spinner_items;
    //HashMap<Integer, List<String>> hashMap;
    private ArrayList<CategoryAttributes> attributes;
    private int size;

    public QuoteOptionalAdapter(Context context, int size, ArrayList<CategoryAttributes> attributes) {

        this.context = context;
        /*this.l_name = l_name;
        this.l_spinner_items = l_spinner_items;
        this.hashMap = hashMap;*/
        this.attributes = attributes;
        this.size = size;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custom_quote_optional, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final CategoryAttributes attr = attributes.get(position);
        holder.txt_name.setText(attr.getName());

        Log.d("ramesh", attributes.get(position).getValues() + " Adapter POSTION");
        holder.spinner_items.setAdapter(new SpinnerAdapter(context, attr.getValues()));
        holder.spinner_items.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String s;

                s = attr.getIds().get(position);
                s = "," + s;

                b = b + s;
                //s= s.concat(","+s);

                if (size == 0) {

                } else {

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public int getItemCount() {

        Log.d("ramesh", "OPTIONAL SIZE: " + size);

        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_name;
        Spinner spinner_items;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_name = (TextView) itemView.findViewById(R.id.txt_custom_product_post_optional);
            spinner_items = (Spinner) itemView.findViewById(R.id.spinner_custom_product_post_optional);
        }
    }

    class SpinnerAdapter extends ArrayAdapter<String> {

        Context context;
        TextView txt_spinner;
        List<String> list_main_cat;


        public SpinnerAdapter(Context context, List<String> list_main_cat) {
            super(context, R.layout.custom_spinner_layout, R.id.txt_custom_spinner, list_main_cat);
            this.context = context;
            this.list_main_cat = list_main_cat;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(context).inflate(R.layout.custom_spinner_layout, parent, false);

            txt_spinner = (TextView) convertView.findViewById(R.id.txt_custom_spinner);
            txt_spinner.setText(list_main_cat.get(position));

            return convertView;
        }
    }
}
