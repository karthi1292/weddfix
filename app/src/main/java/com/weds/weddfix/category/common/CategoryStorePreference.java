package com.weds.weddfix.category.common;


import android.content.Context;
import android.content.SharedPreferences;

public class CategoryStorePreference {
    private Context myContext;
    private SharedPreferences myPreferences;
    private SharedPreferences.Editor myEditor;

    public CategoryStorePreference(Context context) {
        myContext = context;
        myPreferences = myContext.getSharedPreferences(CategoryConstants.MyPREFERENCESNAME, Context.MODE_PRIVATE);
        myEditor = myPreferences.edit();
    }

    public void setString(String key, String value) {
        myEditor.putString(key, value);
        myEditor.commit();
    }

    public void setInt(String key, int value) {
        myEditor.putInt(key, value);
        myEditor.commit();

    }

    public int getIntValue(String key ,int i) {
        return myPreferences.getInt(key, i);
    }

    public String getStringValue(String key) {
        String aName = "";
        aName = myPreferences.getString(key, null);
        return aName == null ? "" : aName;
    }


    public void setBoolean(String key, boolean value) {
        myEditor.putBoolean(key, value);
        myEditor.commit();
    }

    public boolean getBooleanValue(String key) {
        return myPreferences.getBoolean(key, false);
    }


    public void clearEditor(String name) {
        SharedPreferences sharedPrefs = myContext.getSharedPreferences(name, myContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.clear();
        editor.commit();
    }
}
