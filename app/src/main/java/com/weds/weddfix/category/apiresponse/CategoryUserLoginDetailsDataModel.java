package com.weds.weddfix.category.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 17-02-2017.
 */

public class CategoryUserLoginDetailsDataModel {

    @SerializedName("userId")
    public String userId;
    @SerializedName("email")
    public String email;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("role")
    public String role;
    @SerializedName("status")
    public String status;
    @SerializedName("passwordHash")
    public String passwordHash;
    @SerializedName("userName")
    public String userName;
    @SerializedName("accountType")
    public String accountType;
    @SerializedName("verifyMobileNumber")
    public String verifyMobileNumber;
    @SerializedName("verifyEmailId")
    public String verifyEmailId;
    @SerializedName("verifiedMobileNumber")
    public String verifiedMobileNumber;
    @SerializedName("verifiedEmailId")
    public String verifiedEmailId;
    @SerializedName("myPlanId")
    public String myPlanId;


}
