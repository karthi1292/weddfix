package com.weds.weddfix.category.common;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;



public class CategoryBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            if (!CategoryCommonMethods.hasPermissions(this, CategoryConstants.PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, CategoryConstants.PERMISSIONS, 1);
            }
        }
    }

    public void putToast(String message) {
        Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
    }

}
