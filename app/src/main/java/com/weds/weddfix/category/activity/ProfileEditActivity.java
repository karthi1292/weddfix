package com.weds.weddfix.category.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.weds.weddfix.R;
import com.weds.weddfix.category.adapter.CatgCitySpinnerAdapter;
import com.weds.weddfix.category.adapter.CatgCountrySpinnerAdapter;
import com.weds.weddfix.category.adapter.CatgStateSpinnerAdapter;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseElementModel;
import com.weds.weddfix.category.apiresponse.CategoryCommonResponseProfileUpdateElementModel;
import com.weds.weddfix.category.common.CategoryActivityIndicator;
import com.weds.weddfix.category.common.CategoryApiClient;
import com.weds.weddfix.category.common.CategoryCommonMethods;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.category.common.CategoryStorePreference;
import com.weds.weddfix.category.interfaces.CategoryRetrofitApiInterface;
import com.weds.weddfix.matrimony.common.MtnyCommonMethods;
import com.weds.weddfix.matrimony.common.MtnyConstants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileEditActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private Toolbar toolbar;
    private CircleImageView profileEditImage;
    private CategoryRetrofitApiInterface apiService;
    private EditText editName, editEmail, editPhone, editPinCode, editWeddDate;
    private CategoryActivityIndicator pDialog;
    private CategoryStorePreference categoryStorePreference;
    private CatgCitySpinnerAdapter catgCitySpinnerAdapter;
    private CatgStateSpinnerAdapter catgStateSpinnerAdapter;
    private CatgCountrySpinnerAdapter catgCountrySpinnerAdapter;
    String filePath = null;
    private String cityId, countryId, stateId;
    private int day, month, year;
    String url_dp_refresh;
    Uri fileUri;
    private String name,email,phone,pinCode,weddDate;
    private AppCompatSpinner citySpinner, countrySpinner, stateSpinner;
    private FloatingActionButton fab;
    ProgressDialog progressDialog;
    private Bitmap bitmap;
    private ImageButton weddCalander;
    private String KEY_IMAGE = "image";
    private String KEY_NAME = "name";
    private int PICK_IMAGE_REQUEST = 1;
    private Context mContext;
    private Calendar calendar;
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            editWeddDate.setText(selectedDay + " / " + (selectedMonth + 1) + " / "
                    + selectedYear);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);


        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        mContext = this;
        intWidgets();
        categoryStorePreference = new CategoryStorePreference(mContext);
        apiService = CategoryApiClient.getCategoryClient().create(CategoryRetrofitApiInterface.class);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        editName.setText(categoryStorePreference.getStringValue(CategoryConstants.U_FNAME));
        editEmail.setText(categoryStorePreference.getStringValue(CategoryConstants.U_EMAIL));
        editPhone.setText(categoryStorePreference.getStringValue(CategoryConstants.U_PHONE));
        editPinCode.setText(categoryStorePreference.getStringValue(CategoryConstants.U_PINCODE));
        editWeddDate.setText(categoryStorePreference.getStringValue(CategoryConstants.U_WEDDING_DATE));
        categoryStorePreference.getStringValue(CategoryConstants.U_CITY_ID);
        categoryStorePreference.getStringValue(CategoryConstants.U_COUNTRY_ID);
        String uProfilePic = categoryStorePreference.getStringValue(CategoryConstants.U_PRO_PIC);

        Glide.with(mContext).load(uProfilePic).placeholder(R.drawable.profile_placeholder).into((ImageView) findViewById(R.id.img_profile_upload));
        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(mContext).clearDiskCache();
            }
        }).start();


    }

    private void intWidgets() {

        countrySpinner = (AppCompatSpinner) findViewById(R.id.spinner_profile_edit_country);
        stateSpinner = (AppCompatSpinner) findViewById(R.id.spinner_profile_edit_state);
        citySpinner = (AppCompatSpinner) findViewById(R.id.spinner_profile_edit_city);
        editName = (EditText) findViewById(R.id.ed_profile_edit_name);
        editEmail = (EditText) findViewById(R.id.ed_profile_edit_email);
        editPhone = (EditText) findViewById(R.id.ed_profile_edit_phone);
        editPinCode = (EditText) findViewById(R.id.ed_profile_edit_pincode);
        editWeddDate = (EditText) findViewById(R.id.ed_profile_edit_wedd_date);
        weddCalander= (ImageButton) findViewById(R.id.image_button_edit_wedd_cal);
        fab = (FloatingActionButton) findViewById(R.id.fab_profile_edit_submit_update);
        profileEditImage = (CircleImageView) findViewById(R.id.img_profile_upload);


        editEmail.setEnabled(false);
        fab.setOnClickListener(this);
        weddCalander.setOnClickListener(this);
        profileEditImage.setOnClickListener(this);

    }
    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }

    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    private void startDialog() {

        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                mContext);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(intent, PICK_IMAGE_REQUEST);

                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        captureImage();
                    }
                });
        myAlertDialog.show();
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory().getAbsolutePath(),
                CategoryConstants.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ramesh", "Oops! Failed create "
                        + CategoryConstants.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
       /* String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());*/
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "avatar_" + categoryStorePreference.getStringValue(CategoryConstants.USER_ID) + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 2909);
                if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {

                    Uri pickedImage = data.getData();
                    // Let's read picked image path using content resolver
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
                    cursor.moveToFirst();
                    String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
                    fileUri = Uri.parse(imagePath);
                    Log.d("ramesh", fileUri + " gallery");
                    Log.d("ramesh", imagePath + " img_gallery");
                    bitmap = decodeSampledBitmapFromResource(imagePath, 100, 100);
                    profileEditImage.setImageBitmap(decodeSampledBitmapFromResource(imagePath, 100, 100));

                    // new Uploadtoserver().execute();
                    // Now we need to set the GUI ImageView data with data read from the picked file.


                    // At the end remember to close the cursor or you will end with the RuntimeException!
                    cursor.close();
                }
                if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                    if (resultCode == RESULT_OK) {

                        // successfully captured the image
                        // launching upload activity
                        Log.d("ramesh", fileUri.getPath() + " camera Image");
                        bitmap = decodeSampledBitmapFromResource(fileUri.getPath(), 100, 100);
                        // new Uploadtoserver().execute();

                        profileEditImage.setImageBitmap(bitmap);


                    } else if (resultCode == RESULT_CANCELED) {

                        // user cancelled Image capture
                        Toast.makeText(getApplicationContext(),
                                "User cancelled image capture", Toast.LENGTH_SHORT)
                                .show();

                    } else {
                        // failed to capture image
                        Toast.makeText(getApplicationContext(),
                                "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                                .show();
                    }

                }
            } else {

            }
        } else {

        }
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 2909: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");
                } else {
                    Log.e("Permission", "Denied");
                }
                return;
            }
        }
    }


    private void profileUpdate() {

        pDialog = new CategoryActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<CategoryCommonResponseProfileUpdateElementModel> call = apiService.getProfileUpdate(categoryStorePreference.getStringValue(CategoryConstants.USER_ID), categoryStorePreference.getStringValue(CategoryConstants.U_WEDDING_DATE), categoryStorePreference.getStringValue(CategoryConstants.U_FNAME), categoryStorePreference.getStringValue(CategoryConstants.U_PHONE), categoryStorePreference.getStringValue(CategoryConstants.U_CITY_ID), categoryStorePreference.getStringValue(CategoryConstants.U_STATE_ID), categoryStorePreference.getStringValue(CategoryConstants.U_COUNTRY_ID), categoryStorePreference.getStringValue(CategoryConstants.U_PINCODE));
        call.enqueue(new Callback<CategoryCommonResponseProfileUpdateElementModel>() {
            @Override
            public void onResponse(Call<CategoryCommonResponseProfileUpdateElementModel> call, Response<CategoryCommonResponseProfileUpdateElementModel> response) {

                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().categoryProfileUpdateDataModel.status;
                    String message = response.body().categoryProfileUpdateDataModel.message;

                    if (status.equals(CategoryConstants.SUCCESS)) {


                        Intent intent = new Intent(mContext, HomeActivity.class);
                        startActivity(intent);
                        finish();
                        if (pDialog != null)
                            pDialog.dismiss();
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override

            public void onFailure(Call<CategoryCommonResponseProfileUpdateElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
            }
        });


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_profile_edit_submit_update:

                name = editName.getText().toString();
                email = editEmail.getText().toString();
                phone = editPhone.getText().toString();
                pinCode = editPinCode.getText().toString();
                weddDate=editWeddDate.getText().toString();

                if (name.length() > 0 && email.length() > 0 && phone.length() > 0 && pinCode.length() > 0&& weddDate.length() > 0) {
                    if (countryId != null && stateId != null && cityId != null) {

                        if (editPhone.getText().toString().length() == 10) {
                            if (CategoryCommonMethods.isNetworkConnectionAvailable(mContext)) {
                                profileUpdate();
                            } else {
                                MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
                            }
                        } else {
                            Toast.makeText(mContext, "Mobile No is Invalid", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.img_profile_upload:
                startDialog();
                break;

            case R.id.image_button_edit_wedd_cal:
                showDialog(0);
                break;
        }
    }
}
