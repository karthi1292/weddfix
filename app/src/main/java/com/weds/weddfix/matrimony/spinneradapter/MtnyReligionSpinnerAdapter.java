package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterReligionDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyReligionSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterReligionDataModel> mtnyRegisterReligionDataModelList;

    public MtnyReligionSpinnerAdapter(Context context, List<MtnyRegisterReligionDataModel> religionDataModelList) {
        this.mContext = context;
        this.mtnyRegisterReligionDataModelList = religionDataModelList;
    }

    @Override
    public int getCount() {
        return mtnyRegisterReligionDataModelList.size();
    }

    @Override
    public MtnyRegisterReligionDataModel getItem(int position) {
        return mtnyRegisterReligionDataModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getReligionId(int position) {
        return getItem(position).getId();
    }

    public String getReligionName(int position) {
        return getItem(position).getReligionName();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.common_spinner, parent, false);
            holder = new ViewHolder();
            holder.txtReligionName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtReligionName.setText(getReligionName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtReligionName;
    }
}
