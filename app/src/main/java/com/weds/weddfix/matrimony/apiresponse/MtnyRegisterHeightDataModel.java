package com.weds.weddfix.matrimony.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/22/2017.
 */
public class MtnyRegisterHeightDataModel implements Parcelable{
    @SerializedName("id")
    public String id;
    @SerializedName("feetInches")
    public String feetInches;

    public MtnyRegisterHeightDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFeetInches() {
        return feetInches;
    }

    public void setFeetInches(String feetInches) {
        this.feetInches = feetInches;
    }

    protected MtnyRegisterHeightDataModel(Parcel in) {
        id = in.readString();
        feetInches = in.readString();
    }

    public static final Creator<MtnyRegisterHeightDataModel> CREATOR = new Creator<MtnyRegisterHeightDataModel>() {
        @Override
        public MtnyRegisterHeightDataModel createFromParcel(Parcel in) {
            return new MtnyRegisterHeightDataModel(in);
        }

        @Override
        public MtnyRegisterHeightDataModel[] newArray(int size) {
            return new MtnyRegisterHeightDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(feetInches);
    }
}
