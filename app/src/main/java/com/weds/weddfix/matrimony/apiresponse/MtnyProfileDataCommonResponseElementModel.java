package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by KarthickRaja on 2/3/2017.
 */

public class MtnyProfileDataCommonResponseElementModel {
    @SerializedName("newMatches")
    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyMatchesProfileList;
    @SerializedName("whoViewedMyProfile")
    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyVwdMyProfileList;
    @SerializedName("viewedAndNotContacted")
    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyVwdNotContactedProfileList;
    @SerializedName("recentlyViewedProfiles")
    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyRecentVwdList;
    @SerializedName("shortlistedProfiles")
    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyShortListedProfileList;

}
