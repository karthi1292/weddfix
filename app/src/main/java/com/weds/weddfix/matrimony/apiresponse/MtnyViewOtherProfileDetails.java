package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/24/2017.
 */

public class MtnyViewOtherProfileDetails {
    @SerializedName("viewProfileDetails")
    public MtnyProfileDetails mtnyViewOtherProfileDetails;
    @SerializedName("accountValidity")
    public String accValidity;
}
