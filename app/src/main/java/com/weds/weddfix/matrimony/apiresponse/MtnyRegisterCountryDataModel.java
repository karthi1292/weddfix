package com.weds.weddfix.matrimony.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/22/2017.
 */
public class MtnyRegisterCountryDataModel implements Parcelable {
    @SerializedName("id")
    public String id;
    @SerializedName("countryName")
    public String countryName;

    public MtnyRegisterCountryDataModel() {
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected MtnyRegisterCountryDataModel(Parcel in) {
        id = in.readString();
        countryName = in.readString();
    }

    public static final Creator<MtnyRegisterCountryDataModel> CREATOR = new Creator<MtnyRegisterCountryDataModel>() {
        @Override
        public MtnyRegisterCountryDataModel createFromParcel(Parcel in) {
            return new MtnyRegisterCountryDataModel(in);
        }

        @Override
        public MtnyRegisterCountryDataModel[] newArray(int size) {
            return new MtnyRegisterCountryDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(countryName);
    }
}
