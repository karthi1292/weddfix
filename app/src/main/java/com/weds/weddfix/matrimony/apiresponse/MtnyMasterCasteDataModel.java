package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KarthickRaja on 1/29/2017.
 */

public class MtnyMasterCasteDataModel {
    @SerializedName("caste")
    public List<MtnyRegisterCasteDataModel> mtnyRegisterCasteDataModelList=new ArrayList<>();

    public MtnyMasterCasteDataModel() {
    }

    public List<MtnyRegisterCasteDataModel> getMtnyRegisterCasteDataModelList() {
        return mtnyRegisterCasteDataModelList;
    }

    public void setMtnyRegisterCasteDataModelList(List<MtnyRegisterCasteDataModel> mtnyRegisterCasteDataModelList) {
        this.mtnyRegisterCasteDataModelList = mtnyRegisterCasteDataModelList;
    }
}
