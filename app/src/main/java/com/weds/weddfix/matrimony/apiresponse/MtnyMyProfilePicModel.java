package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/1/2017.
 */
public class MtnyMyProfilePicModel {
    @SerializedName("id")
    public String id;
    @SerializedName("photoType")
    public String photoType;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("showMyProfilePicture")
    public String showMyProfilePicture;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getShowMyProfilePicture() {
        return showMyProfilePicture;
    }

    public void setShowMyProfilePicture(String showMyProfilePicture) {
        this.showMyProfilePicture = showMyProfilePicture;
    }
}
