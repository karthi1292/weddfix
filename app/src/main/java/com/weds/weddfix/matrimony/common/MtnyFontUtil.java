package com.weds.weddfix.matrimony.common;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by CIPL307 on 9/28/2015.
 */
public class MtnyFontUtil {
    public static Typeface typeface;
    public static String helveticaFontPath = "fonts/Helvetica.ttf";

    //to get Helvetica Regular font typeface
    public static Typeface getHelvetica(Context mContext){
        return Typeface.createFromAsset(mContext.getAssets(), helveticaFontPath);
    }

}
