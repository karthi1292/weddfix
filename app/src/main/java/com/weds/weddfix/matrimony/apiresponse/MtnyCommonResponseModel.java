package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/17/2017.
 */

public class MtnyCommonResponseModel {
    @SerializedName("status")
    public String status;
    @SerializedName("mtny_message")
    public String message;
    @SerializedName("userId")
    public String userId;
}
