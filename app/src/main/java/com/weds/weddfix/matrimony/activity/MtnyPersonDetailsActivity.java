package com.weds.weddfix.matrimony.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDetails;
import com.weds.weddfix.matrimony.apiresponse.MtnyViewOtherProfileDetails;
import com.weds.weddfix.matrimony.common.MtnyActivityIndicator;
import com.weds.weddfix.matrimony.common.MtnyApiClient;
import com.weds.weddfix.matrimony.common.MtnyCommonMethods;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;
import com.weds.weddfix.matrimony.interfaces.MtnyRetrofitApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MtnyPersonDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbar;
    private Button btnUpgrade;
    private MtnyRetrofitApiInterface apiService;
    private MtnyActivityIndicator pDialog;
    private Context mContext;
    private MtnyStorePreference mtnyStorePreference;
    private String userId, viewOtherProfileId, viewOtherUserId;
    private TextView nameTextView, dobTextView, ageTextView, genderTextView, maritialStatusTextView, htTextView, wtTextView, bodyTypeTextView, complexionTextView, physicalStatusTextView, smokeTextView, drinkTextView, foodTextView, profileCreatedByTextView, religionTextView, casteTextView, subCasteTextView, mthrTonqueTextView, cityTextView, stateTextView, countryTextView,
            educationTextView, occTextView, employedInTextView, monthlyIncomeTextView, familyStatusTextView, familyTypeTextView, familyValuesTextView, fatherStatusTextView, mothrStatusTextView, nofBrothersTextView, brothersMarriedTextView, nofSisTextView, sistersMarriedTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mtny_activity_person_details);
        mContext = this;

        apiService = MtnyApiClient.getClient().create(MtnyRetrofitApiInterface.class);
        mtnyStorePreference = new MtnyStorePreference(mContext);

        userId = mtnyStorePreference.getStringValue(MtnyConstants.USER_ID);

        Intent intent = getIntent();
        viewOtherUserId = intent.getStringExtra(MtnyConstants.VIEW_OTHER_USER_ID);
        viewOtherProfileId = intent.getStringExtra(MtnyConstants.VIEW_OTHER_PROFILE_ID);

        initWidgets();

        if (MtnyCommonMethods.isNetworkConnectionAvailable(mContext)) {
            getViewProfileDetails();
        } else {
            MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
        }

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_upgrade:
                Intent intent = new Intent(this, MtnyUpgradeActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void initWidgets() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_tollbar);
        btnUpgrade = (Button) findViewById(R.id.button_upgrade);

        nameTextView = (TextView) findViewById(R.id.name_textview);
        dobTextView = (TextView) findViewById(R.id.dob_textview);
        ageTextView = (TextView) findViewById(R.id.age_textview);
        genderTextView = (TextView) findViewById(R.id.gender_textview);
        maritialStatusTextView = (TextView) findViewById(R.id.maritial_status_textview);
        htTextView = (TextView) findViewById(R.id.ht_textview);
        wtTextView = (TextView) findViewById(R.id.wt_textview);
        bodyTypeTextView = (TextView) findViewById(R.id.body_type_textview);
        complexionTextView = (TextView) findViewById(R.id.complexion_textview);
        physicalStatusTextView = (TextView) findViewById(R.id.physical_status_textview);
        smokeTextView = (TextView) findViewById(R.id.smoke_textview);
        drinkTextView = (TextView) findViewById(R.id.drink_textview);
        foodTextView = (TextView) findViewById(R.id.food_textview);
        profileCreatedByTextView = (TextView) findViewById(R.id.profile_created_by_textview);
        religionTextView = (TextView) findViewById(R.id.religion_textview);
        casteTextView = (TextView) findViewById(R.id.caste_textview);
        subCasteTextView = (TextView) findViewById(R.id.sub_caste_textview);
        mthrTonqueTextView = (TextView) findViewById(R.id.mother_tongue_textview);
        cityTextView = (TextView) findViewById(R.id.city_textview);
        stateTextView = (TextView) findViewById(R.id.state_textview);
        countryTextView = (TextView) findViewById(R.id.country_textview);
        educationTextView = (TextView) findViewById(R.id.education_textview);
        occTextView = (TextView) findViewById(R.id.occupation_textview);
        employedInTextView = (TextView) findViewById(R.id.employed_in_textView);
        monthlyIncomeTextView = (TextView) findViewById(R.id.monthly_income_textview);
        familyStatusTextView = (TextView) findViewById(R.id.family_status_textview);
        familyTypeTextView = (TextView) findViewById(R.id.family_type_textview);
        familyValuesTextView = (TextView) findViewById(R.id.family_values_textview);
        fatherStatusTextView = (TextView) findViewById(R.id.family_status_textview);
        mothrStatusTextView = (TextView) findViewById(R.id.mothers_status_textview);
        nofBrothersTextView = (TextView) findViewById(R.id.no_of_brothers_textview);
        brothersMarriedTextView = (TextView) findViewById(R.id.brothers_married_textview);
        nofSisTextView = (TextView) findViewById(R.id.no_of_sisters_textview);
        sistersMarriedTextView = (TextView) findViewById(R.id.sisters_married_textview);

        btnUpgrade.setOnClickListener(this);

        collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.white));
    }

    public void getViewProfileDetails() {
        pDialog = new MtnyActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<MtnyViewOtherProfileDetails> call = apiService.getViewOtherProfileDetails(viewOtherUserId, viewOtherProfileId, userId);
        call.enqueue(new Callback<MtnyViewOtherProfileDetails>() {
            @Override
            public void onResponse(Call<MtnyViewOtherProfileDetails> call, Response<MtnyViewOtherProfileDetails> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MtnyProfileDetails mtnyProfileDetails = response.body().mtnyViewOtherProfileDetails;
                    loadProfileDetails(mtnyProfileDetails);

                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyViewOtherProfileDetails> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    public void loadProfileDetails(MtnyProfileDetails mtnyProfileDetails) {
        toolbar.setTitle(mtnyProfileDetails.fullName);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        nameTextView.setText(mtnyProfileDetails.fullName);
        dobTextView.setText(mtnyProfileDetails.dob);
        ageTextView.setText(mtnyProfileDetails.age);
        genderTextView.setText(mtnyProfileDetails.gender);
        maritialStatusTextView.setText(mtnyProfileDetails.maritalStatus);
        htTextView.setText(mtnyProfileDetails.height);
        wtTextView.setText(mtnyProfileDetails.weight);
        bodyTypeTextView.setText(mtnyProfileDetails.bodyType);
        complexionTextView.setText(mtnyProfileDetails.complexion);
        physicalStatusTextView.setText(mtnyProfileDetails.physicalStatus);
        smokeTextView.setText(mtnyProfileDetails.smoking);
        drinkTextView.setText(mtnyProfileDetails.drinking);
        foodTextView.setText(mtnyProfileDetails.food);
        profileCreatedByTextView.setText(mtnyProfileDetails.profileFor);
        religionTextView.setText(mtnyProfileDetails.religion);
        casteTextView.setText(mtnyProfileDetails.caste);
        subCasteTextView.setText(mtnyProfileDetails.subCaste);
        mthrTonqueTextView.setText(mtnyProfileDetails.motherTongue);
        cityTextView.setText(mtnyProfileDetails.city);
        stateTextView.setText(mtnyProfileDetails.state);
        countryTextView.setText(mtnyProfileDetails.country);
        educationTextView.setText(mtnyProfileDetails.education);
        occTextView.setText(mtnyProfileDetails.occupation);
        employedInTextView.setText(mtnyProfileDetails.employedIn);
        monthlyIncomeTextView.setText(mtnyProfileDetails.monthlyIncome);
        familyStatusTextView.setText(mtnyProfileDetails.familyStatus);
        familyTypeTextView.setText(mtnyProfileDetails.familyType);
        familyValuesTextView.setText(mtnyProfileDetails.familyValues);
        fatherStatusTextView.setText(mtnyProfileDetails.fathersStatus);
        mothrStatusTextView.setText(mtnyProfileDetails.mothersStatus);
        nofBrothersTextView.setText(mtnyProfileDetails.numberOfBrothers);
        brothersMarriedTextView.setText(mtnyProfileDetails.brothersMarried);
        nofSisTextView.setText(mtnyProfileDetails.numberOfSisters);
        sistersMarriedTextView.setText(mtnyProfileDetails.sistersMarried);
    }
}
