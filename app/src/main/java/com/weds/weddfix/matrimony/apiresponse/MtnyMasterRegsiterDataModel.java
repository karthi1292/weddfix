package com.weds.weddfix.matrimony.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KarthickRaja on 1/22/2017.
 */

public class MtnyMasterRegsiterDataModel implements Parcelable {

    @SerializedName("mtny_home_profile")
    public List<MtnyRegisterProfileDataModel> profileDataModelList = new ArrayList<>();
    @SerializedName("gender")
    public List<MtnyRegisterGenderDataModel> genderDataModelList = new ArrayList<>();
    @SerializedName("maritalStatus")
    public List<MtnyRegisterMaritialStatusDataModel> maritalStatusDataModelList = new ArrayList<>();
    @SerializedName("height")
    public List<MtnyRegisterHeightDataModel> heightDataModelList = new ArrayList<>();
    @SerializedName("education")
    public List<MtnyRegisterEducationDataModel> educationDataModelList = new ArrayList<>();
    @SerializedName("religion")
    public List<MtnyRegisterReligionDataModel> religionDataModelList = new ArrayList<>();
    @SerializedName("occupation")
    public List<MtnyRegisterOccupationDataModel> occupationDataModelList = new ArrayList<>();
    @SerializedName("country")
    public List<MtnyRegisterCountryDataModel> countryDataModelList = new ArrayList<>();
    @SerializedName("motherTongue")
    public List<MtnyRegisterMthrTongueDataModel> mthrTongueDataModelList=new ArrayList<>();


    public MtnyMasterRegsiterDataModel() {

    }

    public List<MtnyRegisterMthrTongueDataModel> getMthrTongueDataModelList() {
        return mthrTongueDataModelList;
    }

    public void setMthrTongueDataModelList(List<MtnyRegisterMthrTongueDataModel> mthrTongueDataModelList) {
        this.mthrTongueDataModelList = mthrTongueDataModelList;
    }

    public List<MtnyRegisterProfileDataModel> getProfileDataModelList() {
        return profileDataModelList;
    }

    public void setProfileDataModelList(List<MtnyRegisterProfileDataModel> profileDataModelList) {
        this.profileDataModelList = profileDataModelList;
    }

    public List<MtnyRegisterGenderDataModel> getGenderDataModelList() {
        return genderDataModelList;
    }

    public void setGenderDataModelList(List<MtnyRegisterGenderDataModel> genderDataModelList) {
        this.genderDataModelList = genderDataModelList;
    }

    public List<MtnyRegisterMaritialStatusDataModel> getMaritalStatusDataModelList() {
        return maritalStatusDataModelList;
    }

    public void setMaritalStatusDataModelList(List<MtnyRegisterMaritialStatusDataModel> maritalStatusDataModelList) {
        this.maritalStatusDataModelList = maritalStatusDataModelList;
    }

    public List<MtnyRegisterHeightDataModel> getHeightDataModelList() {
        return heightDataModelList;
    }

    public void setHeightDataModelList(List<MtnyRegisterHeightDataModel> heightDataModelList) {
        this.heightDataModelList = heightDataModelList;
    }

    public List<MtnyRegisterEducationDataModel> getEducationDataModelList() {
        return educationDataModelList;
    }

    public void setEducationDataModelList(List<MtnyRegisterEducationDataModel> educationDataModelList) {
        this.educationDataModelList = educationDataModelList;
    }

    public List<MtnyRegisterReligionDataModel> getReligionDataModelList() {
        return religionDataModelList;
    }

    public void setReligionDataModelList(List<MtnyRegisterReligionDataModel> religionDataModelList) {
        this.religionDataModelList = religionDataModelList;
    }

    public List<MtnyRegisterOccupationDataModel> getOccupationDataModelList() {
        return occupationDataModelList;
    }

    public void setOccupationDataModelList(List<MtnyRegisterOccupationDataModel> occupationDataModelList) {
        this.occupationDataModelList = occupationDataModelList;
    }

    public List<MtnyRegisterCountryDataModel> getCountryDataModelList() {
        return countryDataModelList;
    }

    public void setCountryDataModelList(List<MtnyRegisterCountryDataModel> countryDataModelList) {
        this.countryDataModelList = countryDataModelList;
    }


    protected MtnyMasterRegsiterDataModel(Parcel in) {
        profileDataModelList = in.createTypedArrayList(MtnyRegisterProfileDataModel.CREATOR);
        //  genderDataModelList = in.createTypedArrayList(MtnyRegisterGenderDataModel.CREATOR);
        maritalStatusDataModelList = in.createTypedArrayList(MtnyRegisterMaritialStatusDataModel.CREATOR);
        heightDataModelList = in.createTypedArrayList(MtnyRegisterHeightDataModel.CREATOR);
        educationDataModelList = in.createTypedArrayList(MtnyRegisterEducationDataModel.CREATOR);
        religionDataModelList = in.createTypedArrayList(MtnyRegisterReligionDataModel.CREATOR);
        occupationDataModelList = in.createTypedArrayList(MtnyRegisterOccupationDataModel.CREATOR);
        countryDataModelList = in.createTypedArrayList(MtnyRegisterCountryDataModel.CREATOR);
    }

    public static final Creator<MtnyMasterRegsiterDataModel> CREATOR = new Creator<MtnyMasterRegsiterDataModel>() {
        @Override
        public MtnyMasterRegsiterDataModel createFromParcel(Parcel in) {
            return new MtnyMasterRegsiterDataModel(in);
        }

        @Override
        public MtnyMasterRegsiterDataModel[] newArray(int size) {
            return new MtnyMasterRegsiterDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(profileDataModelList);
        //  dest.writeTypedList(genderDataModelList);
        dest.writeTypedList(maritalStatusDataModelList);
        dest.writeTypedList(heightDataModelList);
        dest.writeTypedList(educationDataModelList);
        dest.writeTypedList(religionDataModelList);
        dest.writeTypedList(occupationDataModelList);
        dest.writeTypedList(countryDataModelList);
    }
}