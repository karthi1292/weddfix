package com.weds.weddfix.matrimony.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.activity.MatrimonyHomeActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class MtnyRegisterReligiousFragment extends Fragment {


    private View view;
    private Button submitButton;

    public MtnyRegisterReligiousFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.mtny_fragment_register_religious, container, false);

        submitButton = (Button) view.findViewById(R.id.submit_button);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent matrimonyIntent = new Intent(getActivity(), MatrimonyHomeActivity.class);
                startActivity(matrimonyIntent);

                Toast.makeText(getActivity(), "Registered Successfully", Toast.LENGTH_LONG).show();
                getActivity().finish();
            }
        });
        return view;
    }

}
