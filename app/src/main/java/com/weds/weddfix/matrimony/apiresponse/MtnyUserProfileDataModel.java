package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyUserProfileDataModel {

    @SerializedName("login")
    public MtnyCommonResponseModel mtnyLoginCommonResponseModel;
    @SerializedName("loginDetails")
    public MtnyLoginDetails mtnyLoginDetails;
    @SerializedName("baseUrl")
    public String baseUrl;
    @SerializedName("myPersonalDetails")
    public MtnyProfileDetails mtnyProfileDetails;
    @SerializedName("myPartnerPreferenceDetails")
    public MtnyMyPartnerPreferenceModel mtnyMyPartnerPreferenceModel;

    public ArrayList<MtnyMyProfilePicModel> getMtnyMyProfilePicModelList() {
        return mtnyMyProfilePicModelList;
    }

    public void setMtnyMyProfilePicModelList(ArrayList<MtnyMyProfilePicModel> mtnyMyProfilePicModelList) {
        this.mtnyMyProfilePicModelList = mtnyMyProfilePicModelList;
    }

    @SerializedName("profilePictures")
    public ArrayList<MtnyMyProfilePicModel> mtnyMyProfilePicModelList;


    public MtnyMyPartnerPreferenceModel getMtnyMyPartnerPreferenceModel() {
        return mtnyMyPartnerPreferenceModel;
    }

    public void setMtnyMyPartnerPreferenceModel(MtnyMyPartnerPreferenceModel mtnyMyPartnerPreferenceModel) {
        this.mtnyMyPartnerPreferenceModel = mtnyMyPartnerPreferenceModel;
    }

    public MtnyProfileDetails getMtnyProfileDetails() {
        return mtnyProfileDetails;
    }

    public void setMtnyProfileDetails(MtnyProfileDetails mtnyProfileDetails) {
        this.mtnyProfileDetails = mtnyProfileDetails;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
