package com.weds.weddfix.matrimony.common;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;




public class MtnyBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            if (!MtnyCommonMethods.hasPermissions(this, MtnyConstants.PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, MtnyConstants.PERMISSIONS, 1);
            }
        }
    }



}
