package com.weds.weddfix.matrimony.activity;

import android.content.pm.ActivityInfo;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.adapter.MtnyViewPagerAdapter;
import com.weds.weddfix.matrimony.fragment.MtnyUpgradeDiamondFragment;
import com.weds.weddfix.matrimony.fragment.MtnyUpgradeGoldFragment;
import com.weds.weddfix.matrimony.fragment.MtnyUpgradeSilverFragment;

public class MtnyUpgradeActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private Toolbar toolbar;
    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mtny_activity_upgrade);

        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.pager_upgrade);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setViewPager();
        tabLayout.setupWithViewPager(pager);
    }


    public void setViewPager() {
        MtnyViewPagerAdapter mtnyViewPagerAdapter = new MtnyViewPagerAdapter(getSupportFragmentManager());
        mtnyViewPagerAdapter.addFragment(new MtnyUpgradeSilverFragment(), "Silver");
        mtnyViewPagerAdapter.addFragment(new MtnyUpgradeGoldFragment(), "Gold");
        mtnyViewPagerAdapter.addFragment(new MtnyUpgradeDiamondFragment(), "Diamond");

        pager.setAdapter(mtnyViewPagerAdapter);
    }
}
