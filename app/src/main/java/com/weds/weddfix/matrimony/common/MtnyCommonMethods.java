package com.weds.weddfix.matrimony.common;


import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;



/**
 * Created by CIPL307 on 9/28/2015.
 */
public class MtnyCommonMethods {
    //To check network availability
    public static boolean isNetworkConnectionAvailable(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean val = false;


        final android.net.NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mobile != null) {
            if (mobile.isAvailable() && mobile.isConnected()) {
                val = true;
            }
        }

        final android.net.NetworkInfo wifi = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifi.isAvailable() && wifi.isConnected()) {

            val = true;
        }
        return val;
    }

    public static void showToast(String message,Context mContext) {
        Toast.makeText(mContext, "" + message, Toast.LENGTH_SHORT).show();
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

}
