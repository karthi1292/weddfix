package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/22/2017.
 */

public class MtnyLoginDetails {
    @SerializedName("userId")
    public String userId;
    @SerializedName("profileId")
    public String profileId;
    @SerializedName("email")
    public String email;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("genderId")
    public String genderId;
    @SerializedName("role")
    public String role;
    @SerializedName("status")
    public String status;
    @SerializedName("passwordHash")
    public String passwordHash;
    @SerializedName("userName")
    public String userName;
    @SerializedName("religionId")
    public String religionId;
    @SerializedName("accountType")
    public String accountType;
    @SerializedName("verifyMobileNumber")
    public String verifyMobileNumber;
    @SerializedName("verifyEmailId")
    public String verifyEmailId;
    @SerializedName("verifyedMobileNumber")
    public String verifyedMobileNumber;
    @SerializedName("verifyedEmailId")
    public String verifyedEmailId;
    @SerializedName("emailCount")
    public String emailCount;
    @SerializedName("mobileCount")
    public String mobileCount;
    @SerializedName("videoCallCount")
    public String videoCallCount;
    @SerializedName("partnerPreferenceId")
    public String partnerPreferenceId;
    @SerializedName("myPlanId")
    public String myPlanId;

}

