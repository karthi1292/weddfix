package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KarthickRaja on 1/29/2017.
 */

public class MtnyMasterCityDataModel {
    @SerializedName("city")
    public List<MtnyRegisterCityDataModel> mtnyRegisterCityDataModelList=new ArrayList<>();

    public MtnyMasterCityDataModel() {
    }

    public List<MtnyRegisterCityDataModel> getMtnyRegisterCityDataModelList() {
        return mtnyRegisterCityDataModelList;
    }

    public void setMtnyRegisterCityDataModelList(List<MtnyRegisterCityDataModel> mtnyRegisterCityDataModelList) {
        this.mtnyRegisterCityDataModelList = mtnyRegisterCityDataModelList;
    }
}
