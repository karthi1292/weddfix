package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/29/2017.
 */

public class MtnyRegisterCityDataModel {
    @SerializedName("id")
    public String id;
    @SerializedName("cityName")
    public String cityName;

    public MtnyRegisterCityDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
