package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterMaritialStatusDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyMaritialStatusSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterMaritialStatusDataModel> maritalStatusDataList;
    public MtnyMaritialStatusSpinnerAdapter(Context context, List<MtnyRegisterMaritialStatusDataModel> maritalStatusDataModelList) {
        this.mContext=context;
        this.maritalStatusDataList=maritalStatusDataModelList;
    }

    @Override
    public int getCount() {
        return maritalStatusDataList.size();
    }

    @Override
    public MtnyRegisterMaritialStatusDataModel getItem(int position) {
        return maritalStatusDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getMaritialStatusId(int position){
        return getItem(position).getId();
    }

    public String getMaritialStatus(int position){
        return getItem(position).getMaritalStatus();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.common_spinner,parent,false);
            holder=new ViewHolder();
            holder.txtMaritialStatus=(TextView)convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        }else{
            holder=(ViewHolder)convertView.getTag();
        }
        holder.txtMaritialStatus.setText(getMaritialStatus(position));
        return convertView;
    }

    private class ViewHolder{
        private TextView txtMaritialStatus;
    }
}
