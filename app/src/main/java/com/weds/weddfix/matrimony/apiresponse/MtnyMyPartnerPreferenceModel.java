package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/1/2017.
 */

public class MtnyMyPartnerPreferenceModel {
    @SerializedName("fromAge")
    public String fromAge;
    @SerializedName("toAge")
    public String toAge;
    @SerializedName("maritalStatus")
    public String maritalStatus;
    @SerializedName("bodyType")
    public String bodyType;
    @SerializedName("complexion")
    public String complexion;
    @SerializedName("fromHeight")
    public String fromHeight;
    @SerializedName("toHeight")
    public String toHeight;
    @SerializedName("food")
    public String food;
    @SerializedName("religion")
    public String religion;
    @SerializedName("caste")
    public String caste;
    @SerializedName("motherTongue")
    public String motherTongue;
    @SerializedName("education")
    public String education;
    @SerializedName("occupation")
    public String occupation;
    @SerializedName("country")
    public String country;
    @SerializedName("state")
    public String state;
    @SerializedName("city")
    public String city;
    @SerializedName("gender")
    public String gender;

    public String getFromAge() {
        return fromAge;
    }

    public void setFromAge(String fromAge) {
        this.fromAge = fromAge;
    }

    public String getToAge() {
        return toAge;
    }

    public void setToAge(String toAge) {
        this.toAge = toAge;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getComplexion() {
        return complexion;
    }

    public void setComplexion(String complexion) {
        this.complexion = complexion;
    }

    public String getFromHeight() {
        return fromHeight;
    }

    public void setFromHeight(String fromHeight) {
        this.fromHeight = fromHeight;
    }

    public String getToHeight() {
        return toHeight;
    }

    public void setToHeight(String toHeight) {
        this.toHeight = toHeight;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getMotherTongue() {
        return motherTongue;
    }

    public void setMotherTongue(String motherTongue) {
        this.motherTongue = motherTongue;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
