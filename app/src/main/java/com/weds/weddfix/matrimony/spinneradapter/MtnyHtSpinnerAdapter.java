package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterHeightDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyHtSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterHeightDataModel> mHeightDataModelList;

    public MtnyHtSpinnerAdapter(Context context, List<MtnyRegisterHeightDataModel> heightDataModelList) {
        this.mContext = context;
        this.mHeightDataModelList = heightDataModelList;
    }

    @Override
    public int getCount() {
        return mHeightDataModelList.size();
    }

    @Override
    public MtnyRegisterHeightDataModel getItem(int position) {
        return mHeightDataModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getHtId(int position) {
        return getItem(position).getId();
    }

    public String getHtFeetInches(int position) {
        return getItem(position).getFeetInches();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.common_spinner, parent, false);
            holder = new ViewHolder();
            holder.txtHtFt = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtHtFt.setText(getHtFeetInches(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtHtFt;
    }
}
