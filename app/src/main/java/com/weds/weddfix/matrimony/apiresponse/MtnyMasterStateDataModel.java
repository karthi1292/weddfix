package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KarthickRaja on 1/29/2017.
 */

public class MtnyMasterStateDataModel {
    @SerializedName("state")
    public List<MtnyRegsiterStateDataModel> mtnyRegsiterStateDataModelList=new ArrayList<>();

    public MtnyMasterStateDataModel() {
    }

    public List<MtnyRegsiterStateDataModel> getMtnyRegsiterStateDataModelList() {
        return mtnyRegsiterStateDataModelList;
    }

    public void setMtnyRegsiterStateDataModelList(List<MtnyRegsiterStateDataModel> mtnyRegsiterStateDataModelList) {
        this.mtnyRegsiterStateDataModelList = mtnyRegsiterStateDataModelList;
    }
}
