package com.weds.weddfix.matrimony.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/22/2017.
 */
public class MtnyRegisterReligionDataModel implements Parcelable {
    @SerializedName("id")
    public String id;
    @SerializedName("religionName")
    public String religionName;

    public MtnyRegisterReligionDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReligionName() {
        return religionName;
    }

    public void setReligionName(String religionName) {
        this.religionName = religionName;
    }


    protected MtnyRegisterReligionDataModel(Parcel in) {
        id = in.readString();
        religionName = in.readString();
    }

    public static final Creator<MtnyRegisterReligionDataModel> CREATOR = new Creator<MtnyRegisterReligionDataModel>() {
        @Override
        public MtnyRegisterReligionDataModel createFromParcel(Parcel in) {
            return new MtnyRegisterReligionDataModel(in);
        }

        @Override
        public MtnyRegisterReligionDataModel[] newArray(int size) {
            return new MtnyRegisterReligionDataModel[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(religionName);
    }
}
