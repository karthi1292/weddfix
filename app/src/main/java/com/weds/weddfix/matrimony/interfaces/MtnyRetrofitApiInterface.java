package com.weds.weddfix.matrimony.interfaces;

import com.weds.weddfix.matrimony.apiresponse.MtnyCommonResponseElementModel;

import com.weds.weddfix.matrimony.apiresponse.MtnyCommonResponseModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterCasteDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterCityDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterRegsiterDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterStateDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMessagesCommonElementModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDataCommonResponseElementModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDetails;
import com.weds.weddfix.matrimony.apiresponse.MtnyRootRecentVwdProfileModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRootShortlstdProfileModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRootVwdAndNtContactedModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRootWhoVwdMyProfileModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyUserProfileDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyViewOtherProfileDetails;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by KarthickRaja on 1/22/2017.
 */

public interface MtnyRetrofitApiInterface {
    @GET("register.json")
    Call<MtnyMasterRegsiterDataModel> getAllMtnyMasterRegisterData();

    @GET("register.json")
    Call<MtnyMasterCityDataModel> getAllMtnyCityMasterRegisterData(@Query("state_Id") String stateId);

    @GET("register.json")
    Call<MtnyMasterStateDataModel> getAllMtnyStateMasterRegisterData(@Query("country_Id") String countryId);

    @GET("register.json")
    Call<MtnyMasterCasteDataModel> getAllMtnyCasteMasterRegisterData(@Query("religion_Id") String casteId);

    @GET("login.json")
    Call<MtnyCommonResponseElementModel> sendForgotPassword(@Query("forgotPasswordEmailId") String forgotPasswordEmailId);

    @GET("matches.json?find=newMatches")
    Call<MtnyProfileDataCommonResponseElementModel> getNewMatchesData(@Query("userId") String userId);

    @GET("matches.json?find=whoViewedMyProfile")
    Call<MtnyProfileDataCommonResponseElementModel> getWhoVwdMyProfileData(@Query("myProfileId") String myProfileId);

    @GET("matches.json?find=viewedAndNotContacted")
    Call<MtnyProfileDataCommonResponseElementModel> getVwdAndNtContactedData(@Query("userId") String userId);

    @GET("matches.json?find=recentlyViewedProfiles")
    Call<MtnyProfileDataCommonResponseElementModel> getRecentVwdProfileData(@Query("userId") String userId);

    @GET("matches.json?find=shortlistedProfiles")
    Call<MtnyProfileDataCommonResponseElementModel> getShortlistedProfilesData(@Query("userId") String userId);

    @GET("matches.json?save=shortlist")
    Call<MtnyCommonResponseElementModel> shortlistProfile(@Query("userId") String userId, @Query("shorlistedProfileId") String unShortlistedProfileId);

    @GET("matches.json?save=unShortlist")
    Call<MtnyCommonResponseElementModel> unShortlistProfile(@Query("userId") String userId,@Query("unShorlistedProfileId") String unShortlistedProfileId);

    @GET("matches.json?save=sendInterest")
    Call<MtnyCommonResponseElementModel> sendInterestProfile(@Query("userId") String userId,@Query("sendInterestProfileId") String sendInterestProfileId);

    @GET("matches.json?find=inbox")
    Call<MtnyMessagesCommonElementModel> getInboxMessages(@Query("profileId") String profileId);

    @GET("matches.json?find=accepted")
    Call<MtnyMessagesCommonElementModel> getAcceptedMessages(@Query("userId") String userId);

    @GET("matches.json?find=notInterested")
    Call<MtnyMessagesCommonElementModel> getNotInterestedMessages(@Query("userId") String userId);

    @GET("matches.json?find=sent")
    Call<MtnyMessagesCommonElementModel> getSentMessages(@Query("userId") String userId);

    @GET("matches.json?find=viewProfile")
    Call<MtnyViewOtherProfileDetails> getViewOtherProfileDetails(@Query("viewUserId") String viewOtherUserId, @Query("viewProfileId") String viewOtherProfileId,@Query("userId") String userId);


    @FormUrlEncoded
    @POST("register.json?save=register")
    Call<MtnyCommonResponseElementModel> registerBioData(@Field("dateOfBirth") String dateOfBirth, @Field("userProfileId") String userProfileId, @Field("fullName") String fullName, @Field("genderId") String genderId, @Field("maritalStatusId") String maritalStatusId, @Field("email") String email, @Field("password") String password, @Field("mobile") String mobile, @Field("heightId") String heightId, @Field("educationId") String educationId, @Field("occupationId") String occupationId, @Field("religionId") String religionId, @Field("cityId") String cityId, @Field("stateId") String stateId, @Field("countryId") String countryId, @Field("aboutYou") String aboutYou);

    @FormUrlEncoded
    @POST("register.json?save=partnerpreference")
    Call<MtnyCommonResponseElementModel> registerPartnerPrefData(@Field("userId") String userId, @Field("fromAge") String fromAge, @Field("toAge") String toAge, @Field("maritalStatusId") String maritalStatusId, @Field("bodyType") String bodyType, @Field("complextion") String complextion, @Field("fromHeightId") String fromHeightId, @Field("toHeightId") String toHeightId, @Field("food") String food, @Field("educationId") String educationId, @Field("occupationId") String occupationId, @Field("religionId") String religionId, @Field("cityId") String cityId, @Field("stateId") String stateId, @Field("countryId") String countryId, @Field("casteId") String casteId, @Field("motherTongueId") String motherTongueId);

    @FormUrlEncoded
    @POST("login.json")
    Call<MtnyUserProfileDataModel> getUserProfileData(@Field("username") String userName, @Field("password") String password);

}
