package com.weds.weddfix.matrimony.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.category.common.CategoryConstants;
import com.weds.weddfix.matrimony.activity.MtnyLoginActivity;
import com.weds.weddfix.matrimony.apiresponse.MtnyCommonResponseElementModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterCasteDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterCityDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterStateDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterCasteDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterCityDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegsiterStateDataModel;
import com.weds.weddfix.matrimony.common.MtnyActivityIndicator;
import com.weds.weddfix.matrimony.common.MtnyApiClient;
import com.weds.weddfix.matrimony.common.MtnyCommonMethods;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnySingleton;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;
import com.weds.weddfix.matrimony.interfaces.MtnyRetrofitApiInterface;
import com.weds.weddfix.matrimony.spinneradapter.MtnyCasteSpinnerAdpater;
import com.weds.weddfix.matrimony.spinneradapter.MtnyCitySpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyCountrySpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyHighestEducationSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyHtSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyMaritialStatusSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyMthrTonqueSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyOccupationSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyReligionSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyStateSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MtnyRegisterPartnerPrefFragment extends Fragment implements OnItemSelectedListener {

    private View view;
    private Button submitButton;
    private EditText fromAgeEditText, toAgeEditText;
    private AppCompatSpinner mrtlStatusSpinner, bodyTypeSpinner, complexionSpinner, foodSpinner, casteSpinner, mthrTongueSpinner, fromHtSpinner, toHtSpinner, educationSpinner, occupationSpinner, religionSpinner, countrySpinner, stateSpinner, citySpinner;
    private Context mContext;
    private MtnyRetrofitApiInterface apiService;
    private MtnyActivityIndicator pDialog;
    private MtnyHtSpinnerAdapter mtnyHtSpinnerAdapter;
    private MtnyHighestEducationSpinnerAdapter mtnyHighestEducationSpinnerAdapter;
    private MtnyMaritialStatusSpinnerAdapter mtnyMaritialStatusSpinnerAdapter;
    private MtnyOccupationSpinnerAdapter mtnyOccupationSpinnerAdapter;
    private MtnyReligionSpinnerAdapter mtnyReligionSpinnerAdapter;
    private MtnyCountrySpinnerAdapter mtnyCountrySpinnerAdapter;
    private MtnyStateSpinnerAdapter mtnyStateSpinnerAdapter;
    private MtnyCitySpinnerAdapter mtnyCitySpinnerAdapter;
    private MtnyCasteSpinnerAdpater mtnyCasteSpinnerAdpater;
    private MtnyMthrTonqueSpinnerAdapter mtnyMthrTonqueSpinnerAdapter;
    private String fromAge, toAge, bodyType, complexion, food, mrtlStatusId, fromHtId, toHtId, religionId, casteId, mthrTongueId, highEducationId, occupationId, countryId, stateId, cityId;
    private ArrayList<String> bodyTypeArrayList, complexionArrayList, foodArrayList;
    private MtnyStorePreference mtnyStorePreference;
    private String userId;

    public MtnyRegisterPartnerPrefFragment() {
        // Required empty public constructor
        apiService = MtnyApiClient.getClient().create(MtnyRetrofitApiInterface.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.mtny_fragment_register_partner_pref, container, false);
        mtnyStorePreference.setString(MtnyConstants.USER_ID, userId);

        initWidgets();

        loadSpinnerValues();

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bodyType = bodyTypeSpinner.getSelectedItem().toString();
                complexion = complexionSpinner.getSelectedItem().toString();
                food = foodSpinner.getSelectedItem().toString();
                fromAge = fromAgeEditText.getText().toString();
                toAge = toAgeEditText.getText().toString();

                if (fromAgeEditText.length() > 0 && toAgeEditText.length() > 0) {
                    if (bodyType != null && complexion != null && food != null && mrtlStatusId != null && fromHtId != null && toHtId != null && religionId != null && casteId != null && mthrTongueId != null && highEducationId != null && occupationId != null && countryId != null && stateId != null && cityId != null) {
                        if (!bodyType.equals("Select") && !complexion.equals("Select") && !food.equals("Select") && !mrtlStatusId.equals("-1") && !fromHtId.equals("-1") && !toHtId.equals("-1") && !religionId.equals("-1") && !casteId.equals("-1") && !mthrTongueId.equals("-1") && !highEducationId.equals("-1") && !occupationId.equals("-1") && !countryId.equals("-1") && !stateId.equals("-1") && !cityId.equals("-1")) {
                           if(MtnyCommonMethods.isNetworkConnectionAvailable(mContext)){
                               sendRegistrationPartnerData();
                           }else{
                               MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR,mContext);
                           }
                        } else {
                            Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }

    public void sendRegistrationPartnerData() {
        pDialog = new MtnyActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<MtnyCommonResponseElementModel> call = apiService.registerPartnerPrefData(userId, fromAge, toAge, mrtlStatusId, bodyType, complexion, fromHtId, toHtId, food, highEducationId, occupationId, religionId, cityId, stateId, countryId, casteId, mthrTongueId);
        call.enqueue(new Callback<MtnyCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<MtnyCommonResponseElementModel> call, Response<MtnyCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().mtnyRegPartnerCommonResponseModel.status;
                    String message = response.body().mtnyRegPartnerCommonResponseModel.message;

                    if (status.equals(CategoryConstants.SUCCESS)) {
                        String userId = response.body().mtnyRegPartnerCommonResponseModel.userId;

                        Intent matrimonyLoginIntent = new Intent(getActivity(), MtnyLoginActivity.class);
                        startActivity(matrimonyLoginIntent);

                        getActivity().finish();

                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, CategoryConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyCommonResponseElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });

    }

    private void initWidgets() {

        submitButton = (Button) view.findViewById(R.id.submit_button);

        mrtlStatusSpinner = (AppCompatSpinner) view.findViewById(R.id.maritial_spinner);
        fromHtSpinner = (AppCompatSpinner) view.findViewById(R.id.from_ht_spinner);
        toHtSpinner = (AppCompatSpinner) view.findViewById(R.id.to_ht_spinner);
        religionSpinner = (AppCompatSpinner) view.findViewById(R.id.religion_spinner);
        educationSpinner = (AppCompatSpinner) view.findViewById(R.id.highest_edu_spinner);
        occupationSpinner = (AppCompatSpinner) view.findViewById(R.id.occupation_spinner);
        countrySpinner = (AppCompatSpinner) view.findViewById(R.id.country_spinner);
        stateSpinner = (AppCompatSpinner) view.findViewById(R.id.state_spinner);
        citySpinner = (AppCompatSpinner) view.findViewById(R.id.city_spinner);
        bodyTypeSpinner = (AppCompatSpinner) view.findViewById(R.id.body_type_spinner);
        complexionSpinner = (AppCompatSpinner) view.findViewById(R.id.complexion_spinner);
        foodSpinner = (AppCompatSpinner) view.findViewById(R.id.food_spinner);
        casteSpinner = (AppCompatSpinner) view.findViewById(R.id.caste_spinner);
        mthrTongueSpinner = (AppCompatSpinner) view.findViewById(R.id.mthr_tongue_spinner);

        fromAgeEditText = (EditText) view.findViewById(R.id.from_age_edittxt);
        toAgeEditText = (EditText) view.findViewById(R.id.to_age_edittxt);

        mrtlStatusSpinner.setOnItemSelectedListener(this);
        fromHtSpinner.setOnItemSelectedListener(this);
        toHtSpinner.setOnItemSelectedListener(this);
        religionSpinner.setOnItemSelectedListener(this);
        educationSpinner.setOnItemSelectedListener(this);
        occupationSpinner.setOnItemSelectedListener(this);
        countrySpinner.setOnItemSelectedListener(this);
        stateSpinner.setOnItemSelectedListener(this);
        citySpinner.setOnItemSelectedListener(this);
        casteSpinner.setOnItemSelectedListener(this);
        mthrTongueSpinner.setOnItemSelectedListener(this);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mtnyStorePreference = new MtnyStorePreference(mContext);
        userId = mtnyStorePreference.getStringValue(MtnyConstants.USER_ID);
    }


    public void loadSpinnerValues() {

        bodyTypeArrayList = new ArrayList<>();
        bodyTypeArrayList.add("Select");
        bodyTypeArrayList.add("Slim");
        bodyTypeArrayList.add("Average");
        bodyTypeArrayList.add("Athletic");
        bodyTypeArrayList.add("Heavy");

        complexionArrayList = new ArrayList<>();
        complexionArrayList.add("Select");
        complexionArrayList.add("Very Fair");
        complexionArrayList.add("Fair");
        complexionArrayList.add("Wheatish");
        complexionArrayList.add("Wheatish Brown");
        complexionArrayList.add("Dark");

        foodArrayList = new ArrayList<>();
        foodArrayList.add("Select");
        foodArrayList.add("Vegetarian");
        foodArrayList.add("Non-Vegetarian");
        foodArrayList.add("Eggetarian");


        ArrayAdapter<String> bodyTypeAdapter = new ArrayAdapter<String>(mContext, R.layout.common_spinner, R.id.state_name, bodyTypeArrayList);
        bodyTypeSpinner.setAdapter(bodyTypeAdapter);

        ArrayAdapter<String> complexionAdapter = new ArrayAdapter<String>(mContext, R.layout.common_spinner, R.id.state_name, complexionArrayList);
        complexionSpinner.setAdapter(complexionAdapter);

        ArrayAdapter<String> foodAdapter = new ArrayAdapter<String>(mContext, R.layout.common_spinner, R.id.state_name, foodArrayList);
        foodSpinner.setAdapter(foodAdapter);

        mtnyMaritialStatusSpinnerAdapter = new MtnyMaritialStatusSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.maritalStatusDataModelList);
        mrtlStatusSpinner.setAdapter(mtnyMaritialStatusSpinnerAdapter);

        mtnyHtSpinnerAdapter = new MtnyHtSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.heightDataModelList);
        fromHtSpinner.setAdapter(mtnyHtSpinnerAdapter);

        mtnyHtSpinnerAdapter = new MtnyHtSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.heightDataModelList);
        toHtSpinner.setAdapter(mtnyHtSpinnerAdapter);

        mtnyHighestEducationSpinnerAdapter = new MtnyHighestEducationSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.educationDataModelList);
        educationSpinner.setAdapter(mtnyHighestEducationSpinnerAdapter);

        mtnyOccupationSpinnerAdapter = new MtnyOccupationSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.occupationDataModelList);
        occupationSpinner.setAdapter(mtnyOccupationSpinnerAdapter);

        mtnyReligionSpinnerAdapter = new MtnyReligionSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.religionDataModelList);
        religionSpinner.setAdapter(mtnyReligionSpinnerAdapter);

        mtnyCountrySpinnerAdapter = new MtnyCountrySpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.countryDataModelList);
        countrySpinner.setAdapter(mtnyCountrySpinnerAdapter);

        mtnyMthrTonqueSpinnerAdapter = new MtnyMthrTonqueSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.mthrTongueDataModelList);
        mthrTongueSpinner.setAdapter(mtnyMthrTonqueSpinnerAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int viewId = parent.getId();
        switch (viewId) {
            case R.id.maritial_spinner:
                mrtlStatusId = mtnyMaritialStatusSpinnerAdapter.getMaritialStatusId(position);
                break;
            case R.id.body_type_spinner:
                break;
            case R.id.from_ht_spinner:
                fromHtId = mtnyHtSpinnerAdapter.getHtId(position);
                break;
            case R.id.to_ht_spinner:
                toHtId = mtnyHtSpinnerAdapter.getHtId(position);
                break;
            case R.id.highest_edu_spinner:
                highEducationId = mtnyHighestEducationSpinnerAdapter.getEducationId(position);
                break;
            case R.id.occupation_spinner:
                occupationId = mtnyOccupationSpinnerAdapter.getOccupationId(position);
                break;
            case R.id.religion_spinner:
                religionId = mtnyReligionSpinnerAdapter.getReligionId(position);
                getAllMasterRegisterCasteData(religionId);
                break;
            case R.id.country_spinner:
                countryId = mtnyCountrySpinnerAdapter.getCountryId(position);
                getAllRegisterMasterStateData(countryId);
                break;
            case R.id.state_spinner:
                stateId = mtnyStateSpinnerAdapter.getStateId(position);
                getAllMasterRegsiterCityData(stateId);
                break;
            case R.id.city_spinner:
                cityId = mtnyCitySpinnerAdapter.getCityId(position);
                break;
            case R.id.mthr_tongue_spinner:
                mthrTongueId = mtnyMthrTonqueSpinnerAdapter.getMthrTongueId(position);
                break;
            case R.id.caste_spinner:
                casteId = mtnyCasteSpinnerAdpater.getCasteId(position);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void getAllRegisterMasterStateData(String countryId) {
        Call<MtnyMasterStateDataModel> call = apiService.getAllMtnyStateMasterRegisterData(countryId);
        call.enqueue(new Callback<MtnyMasterStateDataModel>() {
            @Override
            public void onResponse(Call<MtnyMasterStateDataModel> call, Response<MtnyMasterStateDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MtnyMasterStateDataModel mtnyMasterStateDataModel = new MtnyMasterStateDataModel();

                    List<MtnyRegsiterStateDataModel> stateDataModelList = response.body().mtnyRegsiterStateDataModelList;

                    MtnyRegsiterStateDataModel mtnyRegsiterStateDataModel = new MtnyRegsiterStateDataModel();
                    mtnyRegsiterStateDataModel.id = "-1";
                    mtnyRegsiterStateDataModel.stateName = "Select";

                    mtnyMasterStateDataModel.mtnyRegsiterStateDataModelList.add(mtnyRegsiterStateDataModel);
                    mtnyMasterStateDataModel.mtnyRegsiterStateDataModelList.addAll(stateDataModelList);

                    MtnySingleton.getInstance().mtnyMasterStateDataModel = mtnyMasterStateDataModel;

                    mtnyStateSpinnerAdapter = new MtnyStateSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterStateDataModel.mtnyRegsiterStateDataModelList);
                    stateSpinner.setAdapter(mtnyStateSpinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<MtnyMasterStateDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }

    public void getAllMasterRegsiterCityData(String stateId) {
        Call<MtnyMasterCityDataModel> call = apiService.getAllMtnyCityMasterRegisterData(stateId);
        call.enqueue(new Callback<MtnyMasterCityDataModel>() {
            @Override
            public void onResponse(Call<MtnyMasterCityDataModel> call, Response<MtnyMasterCityDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    MtnyMasterCityDataModel mtnyMasterCityDataModel = new MtnyMasterCityDataModel();
                    List<MtnyRegisterCityDataModel> cityDataModelList = response.body().mtnyRegisterCityDataModelList;

                    MtnyRegisterCityDataModel mtnyRegisterCityDataModel = new MtnyRegisterCityDataModel();
                    mtnyRegisterCityDataModel.id = "-1";
                    mtnyRegisterCityDataModel.cityName = "Select";

                    mtnyMasterCityDataModel.mtnyRegisterCityDataModelList.add(mtnyRegisterCityDataModel);
                    mtnyMasterCityDataModel.mtnyRegisterCityDataModelList.addAll(cityDataModelList);

                    MtnySingleton.getInstance().mtnyMasterCityDataModel = mtnyMasterCityDataModel;

                    mtnyCitySpinnerAdapter = new MtnyCitySpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterCityDataModel.mtnyRegisterCityDataModelList);
                    citySpinner.setAdapter(mtnyCitySpinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<MtnyMasterCityDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }

    public void getAllMasterRegisterCasteData(String religionId) {
        Call<MtnyMasterCasteDataModel> call = apiService.getAllMtnyCasteMasterRegisterData(religionId);
        call.enqueue(new Callback<MtnyMasterCasteDataModel>() {
            @Override
            public void onResponse(Call<MtnyMasterCasteDataModel> call, Response<MtnyMasterCasteDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MtnyMasterCasteDataModel mtnyMasterCasteDataModel = new MtnyMasterCasteDataModel();

                    List<MtnyRegisterCasteDataModel> casteDataModelList = response.body().mtnyRegisterCasteDataModelList;

                    MtnyRegisterCasteDataModel mtnyRegisterCasteDataModel = new MtnyRegisterCasteDataModel();
                    mtnyRegisterCasteDataModel.id = "-1";
                    mtnyRegisterCasteDataModel.casteName = "Select";

                    mtnyMasterCasteDataModel.mtnyRegisterCasteDataModelList.add(mtnyRegisterCasteDataModel);
                    mtnyMasterCasteDataModel.mtnyRegisterCasteDataModelList.addAll(casteDataModelList);

                    MtnySingleton.getInstance().mtnyMasterCasteDataModel = mtnyMasterCasteDataModel;

                    mtnyCasteSpinnerAdpater = new MtnyCasteSpinnerAdpater(mContext, MtnySingleton.getInstance().mtnyMasterCasteDataModel.mtnyRegisterCasteDataModelList);
                    casteSpinner.setAdapter(mtnyCasteSpinnerAdpater);
                }
            }

            @Override
            public void onFailure(Call<MtnyMasterCasteDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }
}
