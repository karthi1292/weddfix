package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/1/2017.
 */
//This class is commonly  used for Login API and OtherProfileAPI
public class MtnyProfileDetails {
    @SerializedName("profileFor")
    public String profileFor;
    @SerializedName("fullName")
    public String fullName;
    @SerializedName("gender")
    public String gender;
    @SerializedName("age")
    public String age;
    @SerializedName("dob")
    public String dob;
    @SerializedName("email")
    public String email;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("religion")
    public String religion;
    @SerializedName("motherTongue")
    public String motherTongue;
    @SerializedName("profileId")
    public String profileId;
    @SerializedName("maritalStatus")
    public String maritalStatus;
    @SerializedName("caste")
    public String caste;
    @SerializedName("subCaste")
    public String subCaste;
    @SerializedName("country")
    public String country;
    @SerializedName("state")
    public String state;
    @SerializedName("city")
    public String city;
    @SerializedName("height")
    public String height;
    @SerializedName("weight")
    public String weight;
    @SerializedName("bodyType")
    public String bodyType;
    @SerializedName("complexion")
    public String complexion;
    @SerializedName("physicalStatus")
    public String physicalStatus;
    @SerializedName("education")
    public String education;
    @SerializedName("occupation")
    public String occupation;
    @SerializedName("employedIn")
    public String employedIn;
    @SerializedName("currency")
    public String currency;
    @SerializedName("monthlyIncome")
    public String monthlyIncome;
    @SerializedName("food")
    public String food;
    @SerializedName("smoking")
    public String smoking;
    @SerializedName("drinking")
    public String drinking;
    @SerializedName("familyStatus")
    public String familyStatus;
    @SerializedName("familyType")
    public String familyType;
    @SerializedName("familyValues")
    public String familyValues;
    @SerializedName("fathersStatus")
    public String fathersStatus;
    @SerializedName("mothersStatus")
    public String mothersStatus;
    @SerializedName("numberOfBrothers")
    public String numberOfBrothers;
    @SerializedName("brothersMarried")
    public String brothersMarried;
    @SerializedName("numberOfSisters")
    public String numberOfSisters;
    @SerializedName("sistersMarried")
    public String sistersMarried;
    @SerializedName("aboutYou")
    public String aboutYou;
    @SerializedName("profilePictureId")
    public String profilePictureId;
    @SerializedName("userId")
    public String userId;
    @SerializedName("profileUserId")
    public String profileUserId;
    @SerializedName("accepted")
    public boolean accepted;
    @SerializedName("notInterested")
    public boolean notInterested;
    @SerializedName("dontshowalreadyViewed")
    public boolean dontshowalreadyViewed;
    @SerializedName("contactRequested")
    public boolean contactRequested;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfileUserId() {
        return profileUserId;
    }

    public void setProfileUserId(String profileUserId) {
        this.profileUserId = profileUserId;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isNotInterested() {
        return notInterested;
    }

    public void setNotInterested(boolean notInterested) {
        this.notInterested = notInterested;
    }

    public boolean isDontshowalreadyViewed() {
        return dontshowalreadyViewed;
    }

    public void setDontshowalreadyViewed(boolean dontshowalreadyViewed) {
        this.dontshowalreadyViewed = dontshowalreadyViewed;
    }

    public boolean isContactRequested() {
        return contactRequested;
    }

    public void setContactRequested(boolean contactRequested) {
        this.contactRequested = contactRequested;
    }



    public String getProfileFor() {
        return profileFor;
    }

    public void setProfileFor(String profileFor) {
        this.profileFor = profileFor;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getMotherTongue() {
        return motherTongue;
    }

    public void setMotherTongue(String motherTongue) {
        this.motherTongue = motherTongue;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getSubCaste() {
        return subCaste;
    }

    public void setSubCaste(String subCaste) {
        this.subCaste = subCaste;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getComplexion() {
        return complexion;
    }

    public void setComplexion(String complexion) {
        this.complexion = complexion;
    }

    public String getPhysicalStatus() {
        return physicalStatus;
    }

    public void setPhysicalStatus(String physicalStatus) {
        this.physicalStatus = physicalStatus;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmployedIn() {
        return employedIn;
    }

    public void setEmployedIn(String employedIn) {
        this.employedIn = employedIn;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(String monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getDrinking() {
        return drinking;
    }

    public void setDrinking(String drinking) {
        this.drinking = drinking;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public String getFamilyType() {
        return familyType;
    }

    public void setFamilyType(String familyType) {
        this.familyType = familyType;
    }

    public String getFamilyValues() {
        return familyValues;
    }

    public void setFamilyValues(String familyValues) {
        this.familyValues = familyValues;
    }

    public String getFathersStatus() {
        return fathersStatus;
    }

    public void setFathersStatus(String fathersStatus) {
        this.fathersStatus = fathersStatus;
    }

    public String getMothersStatus() {
        return mothersStatus;
    }

    public void setMothersStatus(String mothersStatus) {
        this.mothersStatus = mothersStatus;
    }

    public String getNumberOfBrothers() {
        return numberOfBrothers;
    }

    public void setNumberOfBrothers(String numberOfBrothers) {
        this.numberOfBrothers = numberOfBrothers;
    }

    public String getBrothersMarried() {
        return brothersMarried;
    }

    public void setBrothersMarried(String brothersMarried) {
        this.brothersMarried = brothersMarried;
    }

    public String getNumberOfSisters() {
        return numberOfSisters;
    }

    public void setNumberOfSisters(String numberOfSisters) {
        this.numberOfSisters = numberOfSisters;
    }

    public String getSistersMarried() {
        return sistersMarried;
    }

    public void setSistersMarried(String sistersMarried) {
        this.sistersMarried = sistersMarried;
    }

    public String getAboutYou() {
        return aboutYou;
    }

    public void setAboutYou(String aboutYou) {
        this.aboutYou = aboutYou;
    }

    public String getProfilePictureId() {
        return profilePictureId;
    }

    public void setProfilePictureId(String profilePictureId) {
        this.profilePictureId = profilePictureId;
    }
}
