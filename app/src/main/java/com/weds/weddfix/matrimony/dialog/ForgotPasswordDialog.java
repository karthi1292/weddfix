package com.weds.weddfix.matrimony.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.common.MtnyCommonMethods;
import com.weds.weddfix.matrimony.interfaces.MtnyTriggerWithString;

/**
 * Created by KarthickRaja on 2/19/2017.
 */

public class ForgotPasswordDialog extends Dialog implements View.OnClickListener {
    private EditText emailIdEditText;
    private Context mContext;
    private Dialog dialog;
    private Button btnSubmit;
    private MtnyTriggerWithString trigger;
    private String message;

    public ForgotPasswordDialog(Context context, String msg, MtnyTriggerWithString trig) {
        super(context);
        // TODO Auto-generated constructor stub
        mContext = context;
        trigger = trig;
        message = msg;
    }

    public void showDialog() {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View decorView = dialog.getWindow().getDecorView();
        final int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            decorView.setSystemUiVisibility(uiOptions);

            decorView.setOnSystemUiVisibilityChangeListener
                    (new View.OnSystemUiVisibilityChangeListener() {
                        @Override
                        public void onSystemUiVisibilityChange(int visibility) {
                            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                                // TODO: The system bars are visible. Make any desired
                                decorView.setSystemUiVisibility(uiOptions);
                            }
                        }
                    });
        }
        WindowManager.LayoutParams WMLP = dialog.getWindow().getAttributes();
        WMLP.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(WMLP);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        dialog.setContentView(R.layout.dialog_forgot_pwd_alert);
        btnSubmit = (Button) dialog.findViewById(R.id.dialog_submit_btn);
        emailIdEditText = (EditText) dialog.findViewById(R.id.edittext_mail_id);

        btnSubmit.setOnClickListener(this);
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.dialog_submit_btn:
                if(emailIdEditText.length()>0){
                    trigger.initTrigger(true,emailIdEditText.getText().toString());
                }else{
                    MtnyCommonMethods.showToast("Please Enter your regsitered mailId",mContext);
                }
                break;
        }
    }

    public void dismiss(){
        if(dialog!=null){
            dialog.dismiss();
        }
    }
}
