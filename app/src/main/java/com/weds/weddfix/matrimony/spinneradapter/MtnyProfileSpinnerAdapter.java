package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterProfileDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/23/2017.
 */

public class MtnyProfileSpinnerAdapter extends BaseAdapter {
    Context mContext;
    List<MtnyRegisterProfileDataModel> mtnyRegisterProfileDataModelArrayList;

    public MtnyProfileSpinnerAdapter(Context context, List<MtnyRegisterProfileDataModel> mtnyRegisterProfileDataModelList) {
        this.mContext = context;
        this.mtnyRegisterProfileDataModelArrayList = mtnyRegisterProfileDataModelList;
    }

    @Override
    public int getCount() {
        return mtnyRegisterProfileDataModelArrayList.size();
    }

    @Override
    public MtnyRegisterProfileDataModel getItem(int position) {
        return mtnyRegisterProfileDataModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getProfileId(int position) {
        return getItem(position).getId();
    }

    public String getProfileFor(int position) {
        return getItem(position).getProfileFor();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view==null){
            holder=new ViewHolder();
            view=inflater.inflate(R.layout.common_spinner,parent,false);
            holder.txtProfileName=(TextView)view.findViewById(R.id.state_name);
            view.setTag(holder);
        }else{
            holder=(ViewHolder)view.getTag();
        }
        holder.txtProfileName.setText(getProfileFor(position));

        return view;
    }

    private class ViewHolder {
        private TextView txtProfileName;
    }
}
