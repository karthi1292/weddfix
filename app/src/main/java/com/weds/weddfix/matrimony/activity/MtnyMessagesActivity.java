package com.weds.weddfix.matrimony.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.adapter.MtnyViewPagerAdapter;
import com.weds.weddfix.matrimony.fragment.MtnyMsgAcceptFragment;
import com.weds.weddfix.matrimony.fragment.MtnyMsgInboxFragment;
import com.weds.weddfix.matrimony.fragment.MtnyMsgNotInterestedFragment;
import com.weds.weddfix.matrimony.fragment.MtnyMsgSentFragment;



public class MtnyMessagesActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private Toolbar toolbar;
    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mtny_activity_messages);

        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.pager_messages);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolbar.setTitle(getString(R.string.messages));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setViewPager();
        tabLayout.setupWithViewPager(pager);
    }

    public void setViewPager() {
        MtnyViewPagerAdapter mtnyViewPagerAdapter = new MtnyViewPagerAdapter(getSupportFragmentManager());
        mtnyViewPagerAdapter.addFragment(new MtnyMsgInboxFragment(), "InboxActivity");
        mtnyViewPagerAdapter.addFragment(new MtnyMsgAcceptFragment(), "Accepted");
        mtnyViewPagerAdapter.addFragment(new MtnyMsgNotInterestedFragment(), "Not Interested");
        mtnyViewPagerAdapter.addFragment(new MtnyMsgSentFragment(), "Sent");
        pager.setAdapter(mtnyViewPagerAdapter);
    }
}
