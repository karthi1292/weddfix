package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/29/2017.
 */

public class MtnyRegsiterStateDataModel {
    @SerializedName("id")
    public String id;
    @SerializedName("stateName")
    public String stateName;

    public MtnyRegsiterStateDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
