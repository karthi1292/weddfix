package com.weds.weddfix.matrimony.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyCommonResponseElementModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterCityDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterCityDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterMthrTongueDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterStateDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegsiterStateDataModel;
import com.weds.weddfix.matrimony.common.MtnyActivityIndicator;
import com.weds.weddfix.matrimony.common.MtnyApiClient;
import com.weds.weddfix.matrimony.common.MtnyCommonMethods;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnySingleton;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;
import com.weds.weddfix.matrimony.interfaces.MtnyRetrofitApiInterface;
import com.weds.weddfix.matrimony.spinneradapter.MtnyCitySpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyCountrySpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyGenderSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyHighestEducationSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyHtSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyMaritialStatusSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyOccupationSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyProfileSpinnerAdapter;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterCountryDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterEducationDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterGenderDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterHeightDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterMaritialStatusDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterOccupationDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterProfileDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterReligionDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterRegsiterDataModel;
import com.weds.weddfix.matrimony.spinneradapter.MtnyReligionSpinnerAdapter;
import com.weds.weddfix.matrimony.spinneradapter.MtnyStateSpinnerAdapter;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MtnyRegisterBasicsFragment extends Fragment implements OnItemSelectedListener {
    private View view;
    private Button submitButton;
    private AppCompatSpinner profileSpinner, genderSpinner, maritialStatusSpinner, heightSpinner, educationSpinner, occupationSpinner, religionSpinner, countrySpinner, stateSpinner, citySpinner;
    private Context mContext;
    private MtnyRetrofitApiInterface apiService;
    private MtnyActivityIndicator pDialog;
    private MtnyProfileSpinnerAdapter mtnyProfileSpinnerAdapter;
    private MtnyGenderSpinnerAdapter mtnyGenderSpinnerAdapter;
    private MtnyHtSpinnerAdapter mtnyHtSpinnerAdapter;
    private MtnyHighestEducationSpinnerAdapter mtnyHighestEducationSpinnerAdapter;
    private MtnyMaritialStatusSpinnerAdapter mtnyMaritialStatusSpinnerAdapter;
    private MtnyOccupationSpinnerAdapter mtnyOccupationSpinnerAdapter;
    private MtnyReligionSpinnerAdapter mtnyReligionSpinnerAdapter;
    private MtnyCountrySpinnerAdapter mtnyCountrySpinnerAdapter;
    private MtnyStateSpinnerAdapter mtnyStateSpinnerAdapter;
    private MtnyCitySpinnerAdapter mtnyCitySpinnerAdapter;
    private String profileId, genderId, mrtlStatusId, htId, highEducationId, occupationId, religionId, countryId, stateId, cityId;
    private EditText nameEditTxt, dobEditTxt, emailEditTxt, pwdEditTxt, confirmPwdEditTxt, mobileEditTxt, abtYouEditText;
    private MtnyStorePreference mtnyStorePreference;

    public MtnyRegisterBasicsFragment() {
        // Required empty public constructor
        apiService = MtnyApiClient.getClient().create(MtnyRetrofitApiInterface.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.mtny_fragment_register_basics, container, false);
        submitButton = (Button) view.findViewById(R.id.submit_button);
        profileSpinner = (AppCompatSpinner) view.findViewById(R.id.profile_spinner);
        genderSpinner = (AppCompatSpinner) view.findViewById(R.id.gender_spinner);
        maritialStatusSpinner = (AppCompatSpinner) view.findViewById(R.id.maritial_spinner);
        heightSpinner = (AppCompatSpinner) view.findViewById(R.id.height_spinner);
        educationSpinner = (AppCompatSpinner) view.findViewById(R.id.highest_edu_spinner);
        occupationSpinner = (AppCompatSpinner) view.findViewById(R.id.occupation_spinner);
        religionSpinner = (AppCompatSpinner) view.findViewById(R.id.religion_spinner);
        countrySpinner = (AppCompatSpinner) view.findViewById(R.id.country_spinner);
        stateSpinner = (AppCompatSpinner) view.findViewById(R.id.state_spinner);
        citySpinner = (AppCompatSpinner) view.findViewById(R.id.city_spinner);
        nameEditTxt = (EditText) view.findViewById(R.id.name_edittext);
        dobEditTxt = (EditText) view.findViewById(R.id.dob_edittext);
        emailEditTxt = (EditText) view.findViewById(R.id.email_edittext);
        pwdEditTxt = (EditText) view.findViewById(R.id.pwd_edittext);
        confirmPwdEditTxt = (EditText) view.findViewById(R.id.re_type_pwd_edittext);
        mobileEditTxt = (EditText) view.findViewById(R.id.mobile_edittext);
        abtYouEditText = (EditText) view.findViewById(R.id.abt_you_edittext);

        profileSpinner.setOnItemSelectedListener(this);
        genderSpinner.setOnItemSelectedListener(this);
        maritialStatusSpinner.setOnItemSelectedListener(this);
        heightSpinner.setOnItemSelectedListener(this);
        educationSpinner.setOnItemSelectedListener(this);
        occupationSpinner.setOnItemSelectedListener(this);
        religionSpinner.setOnItemSelectedListener(this);
        countrySpinner.setOnItemSelectedListener(this);
        stateSpinner.setOnItemSelectedListener(this);
        citySpinner.setOnItemSelectedListener(this);

        if (MtnyCommonMethods.isNetworkConnectionAvailable(mContext)) {
            getAllRegisterData();
        } else {
            MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nameEditTxt.length() > 0 && dobEditTxt.length() > 0 && emailEditTxt.length() > 0 && pwdEditTxt.length() > 0 && confirmPwdEditTxt.length() > 0 && mobileEditTxt.length() > 0 && abtYouEditText.length() > 0) {
                    if (profileId != null && mrtlStatusId != null && htId != null && highEducationId != null && occupationId != null && religionId != null && countryId != null && stateId != null && cityId != null) {
                        if (!profileId.equals("-1") && !mrtlStatusId.equals("-1") && !htId.equals("-1") && !highEducationId.equals("-1") && !occupationId.equals("-1") && !religionId.equals("-1") && !countryId.equals("-1") && !stateId.equals("-1") && !cityId.equals("-1")) {
                            if (pwdEditTxt.getText().toString().equals(confirmPwdEditTxt.getText().toString())) {
                                if (mobileEditTxt.getText().toString().length() == 10) {
                                    if (MtnyCommonMethods.isNetworkConnectionAvailable(mContext)) {
                                        sendRegistrationBioData();
                                    } else {
                                        MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
                                    }
                                } else {
                                    Toast.makeText(mContext, "Mobile No is Invalid", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(mContext, "Password and Confirm Password are not matching", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }


    public void sendRegistrationBioData() {
        pDialog = new MtnyActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<MtnyCommonResponseElementModel> call = apiService.registerBioData(dobEditTxt.getText().toString(), profileId, nameEditTxt.getText().toString(), genderId, mrtlStatusId, emailEditTxt.getText().toString(), pwdEditTxt.getText().toString(), mobileEditTxt.getText().toString(), htId, highEducationId, occupationId, religionId, cityId, stateId, countryId, abtYouEditText.getText().toString());
        call.enqueue(new Callback<MtnyCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<MtnyCommonResponseElementModel> call, Response<MtnyCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().mtnyRegBioCommonResponseModel.status;
                    String message = response.body().mtnyRegBioCommonResponseModel.message;

                    if (status.equals(MtnyConstants.SUCCESS)) {

                        String userId = response.body().mtnyRegBioCommonResponseModel.userId;
                        mtnyStorePreference.setString(MtnyConstants.USER_ID, userId);

                        MtnyRegisterPartnerPrefFragment mtnyRegisterPartnerPrefFragment = new MtnyRegisterPartnerPrefFragment();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.register_frame, mtnyRegisterPartnerPrefFragment).commit();

                        Toast.makeText(mContext, "Bio Data Saved Successfully", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, MtnyConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyCommonResponseElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mtnyStorePreference = new MtnyStorePreference(mContext);
    }

    private void getAllRegisterData() {
        pDialog = new MtnyActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<MtnyMasterRegsiterDataModel> call = apiService.getAllMtnyMasterRegisterData();
        call.enqueue(new retrofit2.Callback<MtnyMasterRegsiterDataModel>() {
            @Override
            public void onResponse(Call<MtnyMasterRegsiterDataModel> call, Response<MtnyMasterRegsiterDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MtnyMasterRegsiterDataModel mtnyMasterRegsiterDataModel = new MtnyMasterRegsiterDataModel();

                    List<MtnyRegisterProfileDataModel> profileDataModelList = response.body().getProfileDataModelList();
                    List<MtnyRegisterGenderDataModel> genderDataModelList = response.body().getGenderDataModelList();
                    List<MtnyRegisterMaritialStatusDataModel> maritialStatusDataModelList = response.body().getMaritalStatusDataModelList();
                    List<MtnyRegisterHeightDataModel> heightDataModelList = response.body().getHeightDataModelList();
                    List<MtnyRegisterEducationDataModel> educationDataModelList = response.body().getEducationDataModelList();
                    List<MtnyRegisterReligionDataModel> religionDataModelList = response.body().getReligionDataModelList();
                    List<MtnyRegisterOccupationDataModel> occupationDataModelList = response.body().getOccupationDataModelList();
                    List<MtnyRegisterCountryDataModel> countryDataModelList = response.body().getCountryDataModelList();
                    List<MtnyRegisterMthrTongueDataModel> mthrTongueDataModelList = response.body().getMthrTongueDataModelList();

                    MtnyRegisterProfileDataModel mtnyRegisterProfileDataModel = new MtnyRegisterProfileDataModel();
                    mtnyRegisterProfileDataModel.id = "-1";
                    mtnyRegisterProfileDataModel.profileFor = "Select";
                    mtnyMasterRegsiterDataModel.profileDataModelList.add(mtnyRegisterProfileDataModel);
                    mtnyMasterRegsiterDataModel.profileDataModelList.addAll(profileDataModelList);

                    MtnyRegisterGenderDataModel mtnyRegisterGenderDataModel = new MtnyRegisterGenderDataModel();
                    mtnyRegisterGenderDataModel.id = "-1";
                    mtnyRegisterGenderDataModel.genderName = "Select";
                    mtnyMasterRegsiterDataModel.genderDataModelList.add(mtnyRegisterGenderDataModel);
                    mtnyMasterRegsiterDataModel.genderDataModelList.addAll(genderDataModelList);

                    MtnyRegisterMaritialStatusDataModel mtnyRegisterMaritialStatusDataModel = new MtnyRegisterMaritialStatusDataModel();
                    mtnyRegisterMaritialStatusDataModel.id = "-1";
                    mtnyRegisterMaritialStatusDataModel.maritalStatus = "Select";
                    mtnyMasterRegsiterDataModel.maritalStatusDataModelList.add(mtnyRegisterMaritialStatusDataModel);
                    mtnyMasterRegsiterDataModel.maritalStatusDataModelList.addAll(maritialStatusDataModelList);

                    MtnyRegisterHeightDataModel mtnyRegisterHeightDataModel = new MtnyRegisterHeightDataModel();
                    mtnyRegisterHeightDataModel.id = "-1";
                    mtnyRegisterHeightDataModel.feetInches = "Select";
                    mtnyMasterRegsiterDataModel.heightDataModelList.add(mtnyRegisterHeightDataModel);
                    mtnyMasterRegsiterDataModel.heightDataModelList.addAll(heightDataModelList);

                    MtnyRegisterEducationDataModel mtnyRegisterEducationDataModel = new MtnyRegisterEducationDataModel();
                    mtnyRegisterEducationDataModel.id = "-1";
                    mtnyRegisterEducationDataModel.educationName = "Select";
                    mtnyMasterRegsiterDataModel.educationDataModelList.add(mtnyRegisterEducationDataModel);
                    mtnyMasterRegsiterDataModel.educationDataModelList.addAll(educationDataModelList);

                    MtnyRegisterReligionDataModel mtnyRegisterReligionDataModel = new MtnyRegisterReligionDataModel();
                    mtnyRegisterReligionDataModel.id = "-1";
                    mtnyRegisterReligionDataModel.religionName = "Select";
                    mtnyMasterRegsiterDataModel.religionDataModelList.add(mtnyRegisterReligionDataModel);
                    mtnyMasterRegsiterDataModel.religionDataModelList.addAll(religionDataModelList);

                    MtnyRegisterOccupationDataModel mtnyRegisterOccupationDataModel = new MtnyRegisterOccupationDataModel();
                    mtnyRegisterOccupationDataModel.id = "-1";
                    mtnyRegisterOccupationDataModel.occupationName = "Select";
                    mtnyMasterRegsiterDataModel.occupationDataModelList.add(mtnyRegisterOccupationDataModel);
                    mtnyMasterRegsiterDataModel.occupationDataModelList.addAll(occupationDataModelList);

                    MtnyRegisterCountryDataModel mtnyRegisterCountryDataModel = new MtnyRegisterCountryDataModel();
                    mtnyRegisterCountryDataModel.id = "-1";
                    mtnyRegisterCountryDataModel.countryName = "Select";
                    mtnyMasterRegsiterDataModel.countryDataModelList.add(mtnyRegisterCountryDataModel);
                    mtnyMasterRegsiterDataModel.countryDataModelList.addAll(countryDataModelList);

                    MtnyRegisterMthrTongueDataModel mtnyRegisterMthrTongueDataModel = new MtnyRegisterMthrTongueDataModel();
                    mtnyRegisterMthrTongueDataModel.id = "-1";
                    mtnyRegisterMthrTongueDataModel.motherTongueName = "Select";
                    mtnyMasterRegsiterDataModel.mthrTongueDataModelList.add(mtnyRegisterMthrTongueDataModel);
                    mtnyMasterRegsiterDataModel.mthrTongueDataModelList.addAll(mthrTongueDataModelList);

                    MtnySingleton.getInstance().mtnyMasterRegsiterDataModel = mtnyMasterRegsiterDataModel;
                    loadSpinnerValues();

                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyMasterRegsiterDataModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    public void getAllRegisterMasterStateData(String countryId) {
        Call<MtnyMasterStateDataModel> call = apiService.getAllMtnyStateMasterRegisterData(countryId);
        call.enqueue(new Callback<MtnyMasterStateDataModel>() {
            @Override
            public void onResponse(Call<MtnyMasterStateDataModel> call, Response<MtnyMasterStateDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MtnyMasterStateDataModel mtnyMasterStateDataModel = new MtnyMasterStateDataModel();

                    List<MtnyRegsiterStateDataModel> stateDataModelList = response.body().mtnyRegsiterStateDataModelList;

                    MtnyRegsiterStateDataModel mtnyRegsiterStateDataModel = new MtnyRegsiterStateDataModel();
                    mtnyRegsiterStateDataModel.id = "-1";
                    mtnyRegsiterStateDataModel.stateName = "Select";

                    mtnyMasterStateDataModel.mtnyRegsiterStateDataModelList.add(mtnyRegsiterStateDataModel);
                    mtnyMasterStateDataModel.mtnyRegsiterStateDataModelList.addAll(stateDataModelList);

                    MtnySingleton.getInstance().mtnyMasterStateDataModel = mtnyMasterStateDataModel;

                    mtnyStateSpinnerAdapter = new MtnyStateSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterStateDataModel.mtnyRegsiterStateDataModelList);
                    stateSpinner.setAdapter(mtnyStateSpinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<MtnyMasterStateDataModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    public void getAllMasterRegsiterCityData(String stateId) {
        Call<MtnyMasterCityDataModel> call = apiService.getAllMtnyCityMasterRegisterData(stateId);
        call.enqueue(new Callback<MtnyMasterCityDataModel>() {
            @Override
            public void onResponse(Call<MtnyMasterCityDataModel> call, Response<MtnyMasterCityDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    MtnyMasterCityDataModel mtnyMasterCityDataModel = new MtnyMasterCityDataModel();
                    List<MtnyRegisterCityDataModel> cityDataModelList = response.body().mtnyRegisterCityDataModelList;

                    MtnyRegisterCityDataModel mtnyRegisterCityDataModel = new MtnyRegisterCityDataModel();
                    mtnyRegisterCityDataModel.id = "-1";
                    mtnyRegisterCityDataModel.cityName = "Select";

                    mtnyMasterCityDataModel.mtnyRegisterCityDataModelList.add(mtnyRegisterCityDataModel);
                    mtnyMasterCityDataModel.mtnyRegisterCityDataModelList.addAll(cityDataModelList);

                    MtnySingleton.getInstance().mtnyMasterCityDataModel = mtnyMasterCityDataModel;

                    mtnyCitySpinnerAdapter = new MtnyCitySpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterCityDataModel.mtnyRegisterCityDataModelList);
                    citySpinner.setAdapter(mtnyCitySpinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<MtnyMasterCityDataModel> call, Throwable t) {

            }
        });
    }

    public void loadSpinnerValues() {

        mtnyProfileSpinnerAdapter = new MtnyProfileSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.profileDataModelList);
        profileSpinner.setAdapter(mtnyProfileSpinnerAdapter);

        mtnyGenderSpinnerAdapter = new MtnyGenderSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.genderDataModelList);
        genderSpinner.setAdapter(mtnyGenderSpinnerAdapter);

        mtnyMaritialStatusSpinnerAdapter = new MtnyMaritialStatusSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.maritalStatusDataModelList);
        maritialStatusSpinner.setAdapter(mtnyMaritialStatusSpinnerAdapter);

        mtnyHtSpinnerAdapter = new MtnyHtSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.heightDataModelList);
        heightSpinner.setAdapter(mtnyHtSpinnerAdapter);

        mtnyHighestEducationSpinnerAdapter = new MtnyHighestEducationSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.educationDataModelList);
        educationSpinner.setAdapter(mtnyHighestEducationSpinnerAdapter);

        mtnyOccupationSpinnerAdapter = new MtnyOccupationSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.occupationDataModelList);
        occupationSpinner.setAdapter(mtnyOccupationSpinnerAdapter);

        mtnyReligionSpinnerAdapter = new MtnyReligionSpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.religionDataModelList);
        religionSpinner.setAdapter(mtnyReligionSpinnerAdapter);

        mtnyCountrySpinnerAdapter = new MtnyCountrySpinnerAdapter(mContext, MtnySingleton.getInstance().mtnyMasterRegsiterDataModel.countryDataModelList);
        countrySpinner.setAdapter(mtnyCountrySpinnerAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int viewId = parent.getId();
        switch (viewId) {
            case R.id.profile_spinner:
                profileId = mtnyProfileSpinnerAdapter.getProfileId(position);
                break;
            case R.id.gender_spinner:
                genderId = mtnyGenderSpinnerAdapter.getGenderId(position);
                break;
            case R.id.maritial_spinner:
                mrtlStatusId = mtnyMaritialStatusSpinnerAdapter.getMaritialStatusId(position);
                break;
            case R.id.height_spinner:
                htId = mtnyHtSpinnerAdapter.getHtId(position);
                break;
            case R.id.highest_edu_spinner:
                highEducationId = mtnyHighestEducationSpinnerAdapter.getEducationId(position);
                break;
            case R.id.occupation_spinner:
                occupationId = mtnyOccupationSpinnerAdapter.getOccupationId(position);
                break;
            case R.id.religion_spinner:
                religionId = mtnyReligionSpinnerAdapter.getReligionId(position);
                break;
            case R.id.country_spinner:
                countryId = mtnyCountrySpinnerAdapter.getCountryId(position);
                getAllRegisterMasterStateData(countryId);
                break;
            case R.id.state_spinner:
                stateId = mtnyStateSpinnerAdapter.getStateId(position);
                getAllMasterRegsiterCityData(stateId);
                break;
            case R.id.city_spinner:
                cityId = mtnyCitySpinnerAdapter.getCityId(position);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
