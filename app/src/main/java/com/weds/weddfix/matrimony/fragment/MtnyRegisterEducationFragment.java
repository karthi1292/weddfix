package com.weds.weddfix.matrimony.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.weds.weddfix.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MtnyRegisterEducationFragment extends Fragment {

    private View view;
    private Button submitButton;

    public MtnyRegisterEducationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.mtny_fragment_register_education, container, false);

        submitButton = (Button) view.findViewById(R.id.submit_button);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MtnyRegisterReligiousFragment mtnyRegisterReligiousFragment = new MtnyRegisterReligiousFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.register_frame, mtnyRegisterReligiousFragment).commit();
            }
        });
        return view;
    }

}
