package com.weds.weddfix.matrimony.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/22/2017.
 */
public class MtnyRegisterGenderDataModel implements Parcelable {
    @SerializedName("id")
    public String id;
    @SerializedName("genderName")
    public String genderName;

    public MtnyRegisterGenderDataModel() {
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    protected MtnyRegisterGenderDataModel(Parcel in) {
        id = in.readString();
        genderName = in.readString();
    }

    public static final Creator<MtnyRegisterGenderDataModel> CREATOR = new Creator<MtnyRegisterGenderDataModel>() {
        @Override
        public MtnyRegisterGenderDataModel createFromParcel(Parcel in) {
            return new MtnyRegisterGenderDataModel(in);
        }

        @Override
        public MtnyRegisterGenderDataModel[] newArray(int size) {
            return new MtnyRegisterGenderDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(genderName);
    }
}
