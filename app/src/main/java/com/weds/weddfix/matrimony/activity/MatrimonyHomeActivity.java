package com.weds.weddfix.matrimony.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.adapter.MtnyViewPagerAdapter;
import com.weds.weddfix.matrimony.fragment.MtnyHomeMatchesFragment;
import com.weds.weddfix.matrimony.fragment.MtnyRecentlyVwdProfileFragment;
import com.weds.weddfix.matrimony.fragment.MtnyShortlistedProfileFragment;
import com.weds.weddfix.matrimony.fragment.MtnyViewedMyProfileFragment;
import com.weds.weddfix.matrimony.fragment.MtnyVwdAndNotContactedProfileFragment;

public class MatrimonyHomeActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private Toolbar toolbar;
    private ViewPager pager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mtny_activity_home);

        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.pager);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setViewPager();
        tabLayout.setupWithViewPager(pager);
    }

    public void setViewPager() {
        MtnyViewPagerAdapter mtnyViewPagerAdapter = new MtnyViewPagerAdapter(getSupportFragmentManager());
        mtnyViewPagerAdapter.addFragment(new MtnyHomeMatchesFragment(), "Matches");
        mtnyViewPagerAdapter.addFragment(new MtnyViewedMyProfileFragment(), "Viewed");
        mtnyViewPagerAdapter.addFragment(new MtnyVwdAndNotContactedProfileFragment(), "Vwd & Not Contacted");
        mtnyViewPagerAdapter.addFragment(new MtnyShortlistedProfileFragment(), "Shortlisted");
        mtnyViewPagerAdapter.addFragment(new MtnyRecentlyVwdProfileFragment(), "Recently Viwed");
        pager.setAdapter(mtnyViewPagerAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.matrimony_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.messages:
                Intent mtnyMessagesActivityIntent = new Intent(MatrimonyHomeActivity.this, MtnyMessagesActivity.class);
                startActivity(mtnyMessagesActivityIntent);
                break;

            case R.id.profile:
                Intent mtnyEditProfileIntent = new Intent(MatrimonyHomeActivity.this, MtnyProfileSettingsActivity.class);
                startActivity(mtnyEditProfileIntent);
                break;
        }
        return true;
    }
}
