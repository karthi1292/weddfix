package com.weds.weddfix.matrimony.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.activity.MtnyPersonDetailsActivity;
import com.weds.weddfix.matrimony.apiresponse.MtnyCommonResponseElementModel;
import com.weds.weddfix.matrimony.common.MtnyActivityIndicator;
import com.weds.weddfix.matrimony.common.MtnyApiClient;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnySingleton;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;
import com.weds.weddfix.matrimony.interfaces.MtnyRetrofitApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by KarthickRaja on 11/7/2016.
 */

public class MtnyShrtedProfileRecyclerViewAdapter extends RecyclerView.Adapter<MtnyShrtedProfileRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private MtnyRetrofitApiInterface webService;
    private MtnyStorePreference mntyStorePreference;
    private String userId;
    private MtnyActivityIndicator pDialog;

    public MtnyShrtedProfileRecyclerViewAdapter(Context context) {
        this.mContext = context;
        webService = MtnyApiClient.getClient().create(MtnyRetrofitApiInterface.class);

        mntyStorePreference = new MtnyStorePreference(mContext);
        userId = mntyStorePreference.getStringValue(MtnyConstants.USER_ID);
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.mtny_shrted_recyclerview_adapter, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String nameAndMaritialStatus= MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).fullName + ", " + MtnySingleton.getInstance().mtnyHomeMatchesList.get(position).maritalStatus;
        String ageAndHt=MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).age + ", " + MtnySingleton.getInstance().mtnyHomeMatchesList.get(position).height;
        String religion=MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).religion + ", ";
        String education=MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).education;
        String country=MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).country;

        holder.shortlistRelativeLayout.setTag(position);
        holder.sendInterestImageView.setTag(position);
        holder.bioDataLinearLayout.setTag(position);
        holder.nameTextView.setText(nameAndMaritialStatus);
        holder.ageTextView.setText(ageAndHt);
        holder.religionTextView.setText(religion);
        holder.educationTextView.setText(education);
        holder.locationTextView.setText(country);

        boolean shortlisted = MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).shortlisted;
        if (shortlisted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.shortlistTextView.setTextColor(mContext.getColor(R.color.gray));
            } else {
                holder.shortlistTextView.setTextColor(mContext.getResources().getColor(R.color.gray));
            }
            holder.shortlistTextView.setText("ShortListed");
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.shortlistTextView.setTextColor(mContext.getColor(R.color.colorPrimary));
            } else {
                holder.shortlistTextView.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            }
            holder.shortlistTextView.setText("ShortList");
        }
        boolean sendInterested = MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).sendInterested;
        if (sendInterested) {

        }
    }


    @Override
    public int getItemCount() {
        return MtnySingleton.getInstance().mtnyShortlistedProfileList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView profileImageView;
        private TextView nameTextView,ageTextView,religionTextView,educationTextView,locationTextView,shortlistTextView;
        private ImageView shortlistImageView;
        private RelativeLayout shortlistRelativeLayout;
        private LinearLayout bioDataLinearLayout;
        private ImageView sendInterestImageView;
        private Animation loadZoomAnimation;

        public ViewHolder(View view) {
            super(view);
            profileImageView = (ImageView) view.findViewById(R.id.profile_imageview);
            nameTextView = (TextView) view.findViewById(R.id.name_textview);
            ageTextView = (TextView) view.findViewById(R.id.age_textview);

            religionTextView = (TextView) view.findViewById(R.id.religion_textview);
            educationTextView = (TextView) view.findViewById(R.id.education_textview);
            locationTextView = (TextView) view.findViewById(R.id.location_textview);

            shortlistTextView=(TextView)view.findViewById(R.id.shortlist_textview);
            shortlistImageView=(ImageView)view.findViewById(R.id.shortlist_imageview);

            sendInterestImageView = (ImageView) view.findViewById(R.id.send_interest_imageview);
            shortlistRelativeLayout = (RelativeLayout) view.findViewById(R.id.shortlist_relative_layout);
            bioDataLinearLayout =(LinearLayout) view.findViewById(R.id.bio_data_linear_layout);

            loadZoomAnimation = AnimationUtils.loadAnimation(mContext, R.anim.zoom);

            sendInterestImageView.setOnClickListener(this);
            shortlistRelativeLayout.setOnClickListener(this);
            bioDataLinearLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = 0;
            switch (v.getId()){
                case R.id.send_interest_imageview:
                    position = (Integer) v.getTag();
                    v.startAnimation(loadZoomAnimation);
                    sendInterestProfile(position);
                    break;
                case R.id.shortlist_relative_layout:
                    position = (Integer) v.getTag();
                    boolean shortlisted = MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).shortlisted;

                    if (shortlisted)
                        unShortlistProfile(position);
                    else
                        shortlistProfile(position);

                    shortlisted = MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).shortlisted;
                    if (shortlisted) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            shortlistTextView.setTextColor(mContext.getColor(R.color.colorPrimary));
                        } else {
                            shortlistTextView.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                        }
                        shortlistTextView.setText("ShortList");
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            shortlistTextView.setTextColor(mContext.getColor(R.color.gray));
                        } else {
                            shortlistTextView.setTextColor(mContext.getResources().getColor(R.color.gray));
                        }
                        shortlistTextView.setText("ShortListed");
                    }
                    break;
                case R.id.bio_data_linear_layout:
                    position = (Integer) v.getTag();
                    String viewOtherUserId = MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).userId;
                    String viewOtherProfileId = MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).profileId;

                    Intent mtnyPersonDetailsActivityIntent = new Intent(mContext, MtnyPersonDetailsActivity.class);
                    mtnyPersonDetailsActivityIntent.putExtra(MtnyConstants.VIEW_OTHER_USER_ID, viewOtherUserId);
                    mtnyPersonDetailsActivityIntent.putExtra(MtnyConstants.VIEW_OTHER_PROFILE_ID, viewOtherProfileId);
                    mContext.startActivity(mtnyPersonDetailsActivityIntent);
                    break;
            }
        }
    }
    public void shortlistProfile(final int position) {
        pDialog = new MtnyActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();
        String  profileId=MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).profileId;
        Call<MtnyCommonResponseElementModel> call = webService.shortlistProfile(userId, profileId);
        call.enqueue(new Callback<MtnyCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<MtnyCommonResponseElementModel> call, Response<MtnyCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().mtnyShortListResponse.status;
                    String message = response.body().mtnyShortListResponse.message;
                    if (status.equals(MtnyConstants.SUCCESS)) {
                        if (message.equals("Shortlisted Successfully.")) {
                            MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).shortlisted = true;
                            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        }
                    }

                } else {
                    Toast.makeText(mContext, MtnyConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyCommonResponseElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    public void unShortlistProfile(final int position) {
        pDialog = new MtnyActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        String  profileId=MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).profileId;

        Call<MtnyCommonResponseElementModel> call = webService.unShortlistProfile(userId, profileId);
        call.enqueue(new Callback<MtnyCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<MtnyCommonResponseElementModel> call, Response<MtnyCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().mtnyUnShortlistResponse.status;
                    String message = response.body().mtnyUnShortlistResponse.message;
                    if (status.equals(MtnyConstants.SUCCESS)) {
                        if (message.equals("UnShortlisted Successfully.")) {
                            MtnySingleton.getInstance().mtnyShortlistedProfileList.get(position).shortlisted = false;
                            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        }
                    }

                } else {
                    Toast.makeText(mContext, MtnyConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyCommonResponseElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    public void sendInterestProfile(int position) {
        pDialog = new MtnyActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        String profileId = MtnySingleton.getInstance().mtnyHomeMatchesList.get(position).profileId;

        Call<MtnyCommonResponseElementModel> call = webService.sendInterestProfile(userId, profileId);
        call.enqueue(new Callback<MtnyCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<MtnyCommonResponseElementModel> call, Response<MtnyCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().mtnySendInterestResponse.status;
                    String message = response.body().mtnySendInterestResponse.message;
                    if (status.equals(MtnyConstants.SUCCESS)) {
                        if (message.equals("Interest Sent Successfully.")) {
                            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(mContext, MtnyConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyCommonResponseElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }
}
