package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterGenderDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/26/2017.
 */

public class MtnyGenderSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterGenderDataModel> mtnyRegisterGenderDataModelArrayList;

    public MtnyGenderSpinnerAdapter(Context context, List<MtnyRegisterGenderDataModel> genderDataModelList) {
        this.mContext = context;
        this.mtnyRegisterGenderDataModelArrayList = genderDataModelList;

    }

    @Override
    public int getCount() {
        return mtnyRegisterGenderDataModelArrayList.size();
    }

    @Override
    public MtnyRegisterGenderDataModel getItem(int position) {
        return mtnyRegisterGenderDataModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getGender(int position) {
        return getItem(position).getGenderName();
    }

    public String getGenderId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, parent, false);
            holder.txtGender = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtGender.setText(getGender(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtGender;
    }
}
