package com.weds.weddfix.matrimony.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyCommonResponseElementModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyLoginDetails;
import com.weds.weddfix.matrimony.apiresponse.MtnyMyPartnerPreferenceModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDetails;
import com.weds.weddfix.matrimony.apiresponse.MtnyUserProfileDataModel;
import com.weds.weddfix.matrimony.common.MtnyActivityIndicator;
import com.weds.weddfix.matrimony.common.MtnyApiClient;
import com.weds.weddfix.matrimony.common.MtnyCommonMethods;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnySingleton;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;
import com.weds.weddfix.matrimony.dialog.ForgotPasswordDialog;
import com.weds.weddfix.matrimony.interfaces.MtnyRetrofitApiInterface;
import com.weds.weddfix.matrimony.interfaces.MtnyTriggerWithString;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyLoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button loginButton, signUpButton;
    private TextView forgotPass;
    private ForgotPasswordDialog alertForgotDialog;
    private EditText edEmail, edPassword;
    private String email, password;
    private Pattern pattern;
    private String emailRegEx;
    private MtnyRetrofitApiInterface apiService;
    private MtnyActivityIndicator pDialog;
    private Context mContext;
    private MtnyStorePreference mtnyStorePreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.mtny_activity_login);
        mContext = this;
        apiService = MtnyApiClient.getClient().create(MtnyRetrofitApiInterface.class);
        mtnyStorePreference = new MtnyStorePreference(mContext);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        signUpButton = (Button) findViewById(R.id.sign_up_button);
        loginButton = (Button) findViewById(R.id.login_button);
        forgotPass = (TextView) findViewById(R.id.txt_forgot_pass);
        edEmail = (EditText) findViewById(R.id.ed_login_email);
        edPassword = (EditText) findViewById(R.id.ed_login_password);

        loginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        forgotPass.setOnClickListener(this);

        // Regex for a valid email address
        emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                email = edEmail.getText().toString();
                if (email.trim().equals("")) {
                    MtnyCommonMethods.showToast("Please Enter the email", this);
                    return;
                }
                pattern = Pattern.compile(emailRegEx);
                Matcher matcher = pattern.matcher(email);
                if (!matcher.find()) {
                    MtnyCommonMethods.showToast("Please Enter the valid email", this);
                    return;
                }
                password = edPassword.getText().toString();
                if (password.trim().equals("")) {
                    MtnyCommonMethods.showToast("Please Enter the Password", this);
                    return;
                }
                if (MtnyCommonMethods.isNetworkConnectionAvailable(this)) {
                    userLogin();
                } else {
                    MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, this);
                }
                break;
            case R.id.sign_up_button:
                Intent signUpButton = new Intent(MtnyLoginActivity.this, MatrimonyRegisterActivity.class);
                startActivity(signUpButton);
                break;
            case R.id.txt_forgot_pass:
                alertForgotDialog = new ForgotPasswordDialog(this, "", new MtnyTriggerWithString() {
                    @Override
                    public void initTrigger(boolean Trigger, String forgotPwdEmailId) {
                        if (forgotPwdEmailId != null) {
                            if (MtnyCommonMethods.isNetworkConnectionAvailable(mContext)) {
                                sendForgotPassword(forgotPwdEmailId);
                            } else {
                                MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, MtnyLoginActivity.this);
                            }
                        } else {
                            MtnyCommonMethods.showToast("Please enter your Registered mailId", MtnyLoginActivity.this);
                        }
                    }
                });
                alertForgotDialog.showDialog();
                break;
        }
    }

    private void sendForgotPassword(String registeredEmailId) {
        pDialog = new MtnyActivityIndicator(this);
        pDialog.setLoadingText("Sending");
        pDialog.show();

        Call<MtnyCommonResponseElementModel> call = apiService.sendForgotPassword(registeredEmailId);
        call.enqueue(new Callback<MtnyCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<MtnyCommonResponseElementModel> call, Response<MtnyCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().mtnyForgotPwdCommonResponseModel.status;
                    String message = response.body().mtnyForgotPwdCommonResponseModel.message;
                    if (status.equals(MtnyConstants.SUCCESS)) {
                        MtnyCommonMethods.showToast(message, MtnyLoginActivity.this);
                        alertForgotDialog.dismiss();
                    } else {
                        MtnyCommonMethods.showToast(message, MtnyLoginActivity.this);
                    }
                } else {
                    MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, MtnyLoginActivity.this);
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyCommonResponseElementModel> call, Throwable t) {
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }

    private void releseWindowToTouch() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void userLogin() {
        pDialog = new MtnyActivityIndicator(this);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<MtnyUserProfileDataModel> call = apiService.getUserProfileData(email, password);
        call.enqueue(new Callback<MtnyUserProfileDataModel>() {
            @Override
            public void onResponse(Call<MtnyUserProfileDataModel> call, retrofit2.Response<MtnyUserProfileDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().mtnyLoginCommonResponseModel.status;

                    if (status.equals(MtnyConstants.SUCCESS)) {
                        mtnyStorePreference.setBoolean(MtnyConstants.LOGGED_IN,true);
                        MtnyUserProfileDataModel mtnyUserProfileDataModel = new MtnyUserProfileDataModel();

                        String baseUrl = response.body().getBaseUrl();
                        MtnyProfileDetails mtnyProfileDetails = response.body().mtnyProfileDetails;
                        MtnyMyPartnerPreferenceModel mtnyMyPartnerPreferenceModel = response.body().mtnyMyPartnerPreferenceModel;
                        MtnyLoginDetails mtnyLoginDetails=response.body().mtnyLoginDetails;

                        mtnyUserProfileDataModel.baseUrl = baseUrl;
                        mtnyUserProfileDataModel.mtnyProfileDetails = mtnyProfileDetails;

                        String userId = mtnyLoginDetails.userId;
                        String profileId=mtnyLoginDetails.profileId;

                        mtnyStorePreference.setString(MtnyConstants.USER_ID, userId);
                        mtnyStorePreference.setString(MtnyConstants.PROFILE_ID,profileId);

                        mtnyUserProfileDataModel.mtnyMyPartnerPreferenceModel = mtnyMyPartnerPreferenceModel;

                        MtnySingleton.getInstance().mtnyUserProfileDataModel = mtnyUserProfileDataModel;

                        Intent homeIntent = new Intent(MtnyLoginActivity.this, MatrimonyHomeActivity.class);
                        startActivity(homeIntent);
                        finish();
                    } else {
                        MtnyCommonMethods.showToast("Entered Mail or Password is incorrect", MtnyLoginActivity.this);
                    }
                    if (pDialog != null)
                        pDialog.dismiss();
                } else {
                    MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, MtnyLoginActivity.this);
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<MtnyUserProfileDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }
}
