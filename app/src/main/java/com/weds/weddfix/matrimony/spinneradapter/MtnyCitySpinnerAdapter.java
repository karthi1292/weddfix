package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterCityDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyCitySpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterCityDataModel> cityDataModelArrayList;

    public MtnyCitySpinnerAdapter(Context context, List<MtnyRegisterCityDataModel> mtnyRegisterCityDataModelList) {
        this.mContext=context;
        this.cityDataModelArrayList=mtnyRegisterCityDataModelList;
    }

    @Override
    public int getCount() {
        return cityDataModelArrayList.size();
    }

    @Override
    public MtnyRegisterCityDataModel getItem(int position) {
        return cityDataModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getCityName(int position) {
        return getItem(position).getCityName();
    }

    public String getCityId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, parent, false);
            holder.txtCityName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtCityName.setText(getCityName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtCityName;
    }
}
