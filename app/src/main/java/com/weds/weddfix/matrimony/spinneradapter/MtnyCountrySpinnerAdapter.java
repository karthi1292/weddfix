package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterCountryDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyCountrySpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterCountryDataModel> mCountryDataModelList;

    public MtnyCountrySpinnerAdapter(Context context, List<MtnyRegisterCountryDataModel> countryDataModelList) {
        this.mContext=context;
        this.mCountryDataModelList=countryDataModelList;
    }

    @Override
    public int getCount() {
        return mCountryDataModelList.size();
    }

    @Override
    public MtnyRegisterCountryDataModel getItem(int position) {
        return mCountryDataModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getCountryName(int position){
        return getItem(position).getCountryName();
    }
    public String getCountryId(int position){
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    ViewHolder holder;
        LayoutInflater inflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.common_spinner,parent,false);
            holder.txtCountryName=(TextView)convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        }
        else{
            holder=(ViewHolder)convertView.getTag();
        }
        holder.txtCountryName.setText(getCountryName(position));
        return convertView;
    }

    private class ViewHolder{
        private TextView txtCountryName;
    }
}
