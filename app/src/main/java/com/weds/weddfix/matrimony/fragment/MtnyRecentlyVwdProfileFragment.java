package com.weds.weddfix.matrimony.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.adapter.MtnyRecentlyVwdRecyclerViewAdapter;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDataCommonResponseElementModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDataCommonResponseModel;
import com.weds.weddfix.matrimony.common.MtnyActivityIndicator;
import com.weds.weddfix.matrimony.common.MtnyApiClient;
import com.weds.weddfix.matrimony.common.MtnyCommonMethods;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnySingleton;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;
import com.weds.weddfix.matrimony.interfaces.MtnyRetrofitApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by KarthickRaja on 2/21/2017.
 */

public class MtnyRecentlyVwdProfileFragment extends Fragment{
        private View view;
        private RecyclerView recyclerView;
        private Context mContext;
        private MtnyRetrofitApiInterface service;
        private MtnyActivityIndicator pDialog;
        private MtnyStorePreference mtnyStorePreference;
        private String userId;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            view = inflater.inflate(R.layout.mtny_fragment_recently_vwd, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recently_vwd_recycler_view);

            if (MtnyCommonMethods.isNetworkConnectionAvailable(mContext)) {
                getAllRecentlyVwdProfile();
            } else {
                MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
            }

            return view;
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);

            mContext = context;
            mtnyStorePreference = new MtnyStorePreference(mContext);
            userId = mtnyStorePreference.getStringValue(MtnyConstants.USER_ID);
            service = MtnyApiClient.getClient().create(MtnyRetrofitApiInterface.class);
        }


        public void getAllRecentlyVwdProfile() {
            pDialog = new MtnyActivityIndicator(mContext);
            pDialog.setLoadingText("Loading....");
            pDialog.show();
            Call<MtnyProfileDataCommonResponseElementModel> call = service.getRecentVwdProfileData(userId);
            call.enqueue(new Callback<MtnyProfileDataCommonResponseElementModel>() {
                @Override
                public void onResponse(Call<MtnyProfileDataCommonResponseElementModel> call, Response<MtnyProfileDataCommonResponseElementModel> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        ArrayList<MtnyProfileDataCommonResponseModel> mtnyRecentVwdProfileList = response.body().mtnyRecentVwdList;
                        if (mtnyRecentVwdProfileList.size() > 0) {
                            MtnySingleton.getInstance().mtnyRecentlyVwdProfileList = mtnyRecentVwdProfileList;

                            MtnyRecentlyVwdRecyclerViewAdapter mtnyRecentlyVwdRecyclerViewAdapter = new MtnyRecentlyVwdRecyclerViewAdapter(mContext);
                            LinearLayoutManager mtnyLinearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                            recyclerView.setLayoutManager(mtnyLinearLayoutManager);
                            recyclerView.setAdapter(mtnyRecentlyVwdRecyclerViewAdapter);
                        }
                    } else {
                        Toast.makeText(mContext, MtnyConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    }
                    if (pDialog != null)
                        pDialog.dismiss();
                }

                @Override
                public void onFailure(Call<MtnyProfileDataCommonResponseElementModel> call, Throwable t) {
                    Log.d("Failure", t.getMessage());
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            });
        }

}
