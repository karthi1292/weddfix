package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterOccupationDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyOccupationSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterOccupationDataModel> mOccupationDataModelList;

    public MtnyOccupationSpinnerAdapter(Context context, List<MtnyRegisterOccupationDataModel> occupationDataModelList) {
        this.mContext=context;
        this.mOccupationDataModelList=occupationDataModelList;
    }

    @Override
    public int getCount() {
        return mOccupationDataModelList.size();
    }

    @Override
    public MtnyRegisterOccupationDataModel getItem(int position) {
        return mOccupationDataModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getOccupationId(int position){
        return getItem(position).getId();
    }

    public String getOccupationName(int position){
        return getItem(position).getOccupationName();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.common_spinner, parent, false);
            holder = new ViewHolder();
            holder.txtOccupationName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtOccupationName.setText(getOccupationName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtOccupationName;
    }
}
