package com.weds.weddfix.matrimony.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyMessagesCommonElementModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMessagesCommonResponseModel;
import com.weds.weddfix.matrimony.common.MtnyActivityIndicator;
import com.weds.weddfix.matrimony.common.MtnyApiClient;
import com.weds.weddfix.matrimony.common.MtnyCommonMethods;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;
import com.weds.weddfix.matrimony.interfaces.MtnyRetrofitApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MtnyMsgNotInterestedFragment extends Fragment {


    private View view;
    private MtnyRetrofitApiInterface apiService;
    private MtnyStorePreference mtnyStorePreference;
    private Context mContext;
    private String profileId;
    private MtnyActivityIndicator pDialog;

    public MtnyMsgNotInterestedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.mtny_fragment_msg_not_interested, container, false);

        if (MtnyCommonMethods.isNetworkConnectionAvailable(mContext)) {
            getNotInterestedData();
        } else {
            MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
        }
        return view;
    }

    public void initWidgets() {

    }

    public void getNotInterestedData() {
        pDialog = new MtnyActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<MtnyMessagesCommonElementModel> call = apiService.getInboxMessages(profileId);
        call.enqueue(new Callback<MtnyMessagesCommonElementModel>() {
            @Override
            public void onResponse(Call<MtnyMessagesCommonElementModel> call, Response<MtnyMessagesCommonElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ArrayList<MtnyMessagesCommonResponseModel> mtnyNotInterestedMsgesModelList = response.body().mtnyNotInterestedMessagesModelList;
                    if (mtnyNotInterestedMsgesModelList.size() > 0) {

                    }
                } else {
                    Toast.makeText(mContext, MtnyConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyMessagesCommonElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        apiService = MtnyApiClient.getClient().create(MtnyRetrofitApiInterface.class);
        mtnyStorePreference = new MtnyStorePreference(mContext);
        profileId = mtnyStorePreference.getStringValue(MtnyConstants.PROFILE_ID);
    }
}
