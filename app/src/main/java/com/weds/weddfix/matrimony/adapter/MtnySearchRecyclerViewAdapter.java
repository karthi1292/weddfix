package com.weds.weddfix.matrimony.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.activity.MtnyPersonDetailsActivity;

/**
 * Created by KarthickRaja on 11/7/2016.
 */

public class MtnySearchRecyclerViewAdapter extends RecyclerView.Adapter<MtnySearchRecyclerViewAdapter.ViewHolder> {

    private Context mContext;

    public MtnySearchRecyclerViewAdapter(Context context) {
        this.mContext = context;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.mtny_matches_recyclerview_adapter, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // viewHolder.detailsTextView.setText("Karthi");
    }


    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView profileImageView;
        private TextView nameTextView,ageTextView,religionTextView,educationTextView,locationTextView,shortlistTextView;
        private ImageView shortlistImageView;
        private RelativeLayout shortlistRelativeLayout;
        private LinearLayout bioDataLinearLayout;
        private ImageView sendInterestImageView;
        private Animation loadZoomAnimation;

        public ViewHolder(View view) {
            super(view);
            profileImageView = (ImageView) view.findViewById(R.id.profile_imageview);
            nameTextView = (TextView) view.findViewById(R.id.name_textview);
            ageTextView = (TextView) view.findViewById(R.id.age_textview);

            religionTextView = (TextView) view.findViewById(R.id.religion_textview);
            educationTextView = (TextView) view.findViewById(R.id.education_textview);
            locationTextView = (TextView) view.findViewById(R.id.location_textview);

            shortlistTextView=(TextView)view.findViewById(R.id.shortlist_textview);
            shortlistImageView=(ImageView)view.findViewById(R.id.shortlist_imageview);

            sendInterestImageView = (ImageView) view.findViewById(R.id.send_interest_imageview);
            shortlistRelativeLayout = (RelativeLayout) view.findViewById(R.id.shortlist_relative_layout);
            bioDataLinearLayout =(LinearLayout) view.findViewById(R.id.bio_data_linear_layout);

            loadZoomAnimation = AnimationUtils.loadAnimation(mContext, R.anim.zoom);

            sendInterestImageView.setOnClickListener(this);
            shortlistRelativeLayout.setOnClickListener(this);
            bioDataLinearLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.send_interest_imageview:
                    v.startAnimation(loadZoomAnimation);
                    break;
                case R.id.shortlist_relative_layout:
                    shortlistTextView.setTextColor(mContext.getColor(R.color.gray));
                    break;
                case R.id.bio_data_linear_layout:
                    Intent mtnyPersonDetailsActivityIntent = new Intent(mContext, MtnyPersonDetailsActivity.class);
                    mContext.startActivity(mtnyPersonDetailsActivityIntent);
                    break;
            }
        }
    }
}
