package com.weds.weddfix.matrimony.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/22/2017.
 */
public class MtnyRegisterEducationDataModel implements Parcelable {
    @SerializedName("id")
    public String id;
    @SerializedName("educationName")
    public String educationName;

    public MtnyRegisterEducationDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }

    protected MtnyRegisterEducationDataModel(Parcel in) {
        id = in.readString();
        educationName = in.readString();
    }

    public static final Creator<MtnyRegisterEducationDataModel> CREATOR = new Creator<MtnyRegisterEducationDataModel>() {
        @Override
        public MtnyRegisterEducationDataModel createFromParcel(Parcel in) {
            return new MtnyRegisterEducationDataModel(in);
        }

        @Override
        public MtnyRegisterEducationDataModel[] newArray(int size) {
            return new MtnyRegisterEducationDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(educationName);
    }
}
