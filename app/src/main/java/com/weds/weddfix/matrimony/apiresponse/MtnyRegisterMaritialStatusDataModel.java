package com.weds.weddfix.matrimony.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/22/2017.
 */
public class MtnyRegisterMaritialStatusDataModel implements Parcelable {
    @SerializedName("id")
    public String id;
    @SerializedName("maritalStatusName")
    public String maritalStatus;


    public MtnyRegisterMaritialStatusDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    protected MtnyRegisterMaritialStatusDataModel(Parcel in) {
        id = in.readString();
        maritalStatus = in.readString();
    }

    public static final Creator<MtnyRegisterMaritialStatusDataModel> CREATOR = new Creator<MtnyRegisterMaritialStatusDataModel>() {
        @Override
        public MtnyRegisterMaritialStatusDataModel createFromParcel(Parcel in) {
            return new MtnyRegisterMaritialStatusDataModel(in);
        }

        @Override
        public MtnyRegisterMaritialStatusDataModel[] newArray(int size) {
            return new MtnyRegisterMaritialStatusDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(maritalStatus);
    }
}
