package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/29/2017.
 */

public class MtnyRegisterMthrTongueDataModel{
    @SerializedName("id")
    public String id;
    @SerializedName("motherTongueName")
    public String motherTongueName;

    public MtnyRegisterMthrTongueDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMotherTongueName() {
        return motherTongueName;
    }

    public void setMotherTongueName(String motherTongueName) {
        this.motherTongueName = motherTongueName;
    }
}
