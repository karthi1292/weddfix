package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegsiterStateDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyStateSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegsiterStateDataModel> mStateDataModelArrayList;


    public MtnyStateSpinnerAdapter(Context context, List<MtnyRegsiterStateDataModel> mtnyRegsiterStateDataModelList) {
        this.mContext=context;
        this.mStateDataModelArrayList=mtnyRegsiterStateDataModelList;
    }

    @Override
    public int getCount() {
        return mStateDataModelArrayList.size();
    }

    @Override
    public MtnyRegsiterStateDataModel getItem(int position) {
        return mStateDataModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getStateName(int position) {
        return getItem(position).getStateName();
    }

    public String getStateId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, parent, false);
            holder.txtStateName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtStateName.setText(getStateName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtStateName;
    }
}
