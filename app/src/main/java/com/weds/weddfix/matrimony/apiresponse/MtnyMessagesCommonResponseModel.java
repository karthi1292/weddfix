package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/27/2017.
 */

public class MtnyMessagesCommonResponseModel {
    @SerializedName("userId")
    public String userId;
    @SerializedName("fullName")
    public String fullName;
    @SerializedName("profileId")
    public String profileId;
    @SerializedName("gender")
    public String gender;
    @SerializedName("age")
    public String age;
    @SerializedName("height")
    public String height;
    @SerializedName("religion")
    public String religion;
    @SerializedName("education")
    public String education;
    @SerializedName("occupation")
    public String occupation;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("accepted")
    public boolean accepted;
    @SerializedName("notInterested")
    public boolean notInterested;
    @SerializedName("showProfilePicture")
    public boolean showProfilePicture;

}
