package com.weds.weddfix.matrimony.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.adapter.MtnyViewedProfileRecyclerViewAdapter;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDataCommonResponseElementModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDataCommonResponseModel;
import com.weds.weddfix.matrimony.common.MtnyActivityIndicator;
import com.weds.weddfix.matrimony.common.MtnyApiClient;
import com.weds.weddfix.matrimony.common.MtnyCommonMethods;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnySingleton;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;
import com.weds.weddfix.matrimony.interfaces.MtnyRetrofitApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by karthick on 9/18/2016.
 */
public class MtnyViewedMyProfileFragment extends Fragment {

    private View view;
    private RecyclerView recyclerView;
    private Context mContext;
    private MtnyRetrofitApiInterface service;
    private MtnyActivityIndicator pDialog;
    private MtnyStorePreference mtnyStorePreference;
    private String userId,profileId;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.mtny_fragment_viewed_profile, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.viewed_profile_recycler_view);

        if (MtnyCommonMethods.isNetworkConnectionAvailable(mContext)) {
            getVwdMyProfileData();
        } else {
            MtnyCommonMethods.showToast(MtnyConstants.TOAST_CONNECTION_ERROR, mContext);
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
        mtnyStorePreference = new MtnyStorePreference(mContext);
        userId = mtnyStorePreference.getStringValue(MtnyConstants.USER_ID);
        profileId=mtnyStorePreference.getStringValue(MtnyConstants.PROFILE_ID);

        service = MtnyApiClient.getClient().create(MtnyRetrofitApiInterface.class);

    }

    public void getVwdMyProfileData(){
        pDialog = new MtnyActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<MtnyProfileDataCommonResponseElementModel> call = service.getWhoVwdMyProfileData(profileId);
        call.enqueue(new Callback<MtnyProfileDataCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<MtnyProfileDataCommonResponseElementModel> call, Response<MtnyProfileDataCommonResponseElementModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    ArrayList<MtnyProfileDataCommonResponseModel> mtnyVwdMyProfileModelList = response.body().mtnyVwdMyProfileList;
                    if(mtnyVwdMyProfileModelList.size()>0){
                        MtnySingleton.getInstance().mtnyVwdMyProfileList=mtnyVwdMyProfileModelList;

                        MtnyViewedProfileRecyclerViewAdapter mtnyViewedProfileRecyclerViewAdapter = new MtnyViewedProfileRecyclerViewAdapter(mContext);
                        LinearLayoutManager mtnyLinearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(mtnyLinearLayoutManager);
                        recyclerView.setAdapter(mtnyViewedProfileRecyclerViewAdapter);
                    }
                } else {
                    Toast.makeText(mContext, MtnyConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                }
                if (pDialog != null)
                    pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MtnyProfileDataCommonResponseElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
                if (pDialog != null)
                    pDialog.dismiss();
            }
        });
    }
}
