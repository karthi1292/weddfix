package com.weds.weddfix.matrimony.common;

import com.weds.weddfix.matrimony.apiresponse.MtnyMasterCasteDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterCityDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterRegsiterDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMasterStateDataModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyMessagesCommonResponseModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyProfileDataCommonResponseModel;
import com.weds.weddfix.matrimony.apiresponse.MtnyUserProfileDataModel;

import java.util.ArrayList;

/**
 * Created by KarthickRaja on 1/22/2017.
 */

public class MtnySingleton {

    private static MtnySingleton instance = null;
    public static MtnySingleton getInstance() {
        if (instance == null) {
            synchronized (MtnySingleton.class) {
                if (instance == null) {
                    instance = new MtnySingleton();
                }
            }
        }
        return instance;
    }

    public MtnyMasterRegsiterDataModel mtnyMasterRegsiterDataModel =new MtnyMasterRegsiterDataModel();
    public MtnyMasterStateDataModel mtnyMasterStateDataModel=new MtnyMasterStateDataModel();
    public MtnyMasterCityDataModel mtnyMasterCityDataModel=new MtnyMasterCityDataModel();
    public MtnyMasterCasteDataModel mtnyMasterCasteDataModel=new MtnyMasterCasteDataModel();
    public MtnyUserProfileDataModel mtnyUserProfileDataModel=new MtnyUserProfileDataModel();

    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyHomeMatchesList;
    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyVwdMyProfileList;
    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyVwdAndNotContactedProfileList;
    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyShortlistedProfileList;
    public ArrayList<MtnyProfileDataCommonResponseModel> mtnyRecentlyVwdProfileList;

    public ArrayList<MtnyMessagesCommonResponseModel> mtnyInboxMsgesList;

}
