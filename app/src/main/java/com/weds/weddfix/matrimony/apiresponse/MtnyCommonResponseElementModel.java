package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by KarthickRaja on 2/8/2017.
 */

public class MtnyCommonResponseElementModel {
    @SerializedName("register")
    public MtnyCommonResponseModel mtnyRegBioCommonResponseModel;
    @SerializedName("partnermtnySendInterestResponsePreference")
    public MtnyCommonResponseModel mtnyRegPartnerCommonResponseModel;
    @SerializedName("forgotPassword")
    public MtnyCommonResponseModel mtnyForgotPwdCommonResponseModel;
    @SerializedName("shortlist")
    public MtnyCommonResponseModel mtnyShortListResponse;
    @SerializedName("unShortlist")
    public MtnyCommonResponseModel mtnyUnShortlistResponse;
    @SerializedName("sendInterest")
    public MtnyCommonResponseModel mtnySendInterestResponse;
}
