package com.weds.weddfix.matrimony.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.common.MtnyActivityIndicator;
import com.weds.weddfix.matrimony.common.MtnyApiClient;
import com.weds.weddfix.matrimony.common.MtnyConstants;
import com.weds.weddfix.matrimony.common.MtnySingleton;
import com.weds.weddfix.matrimony.common.MtnyStorePreference;
import com.weds.weddfix.matrimony.interfaces.MtnyRetrofitApiInterface;

/**
 * Created by KarthickRaja on 2/28/2017.
 */

public class MtnyInboxMsgsRecyclerViewAdapter extends RecyclerView.Adapter<MtnyInboxMsgsRecyclerViewAdapter.ViewHolder> {
    private Context mContext;
    private MtnyRetrofitApiInterface webService;
    private MtnyStorePreference mntyStorePreference;
    private String userId;
    private MtnyActivityIndicator pDialog;

    public MtnyInboxMsgsRecyclerViewAdapter(Context context) {
        this.mContext = context;
        webService = MtnyApiClient.getClient().create(MtnyRetrofitApiInterface.class);

        mntyStorePreference = new MtnyStorePreference(mContext);
        userId = mntyStorePreference.getStringValue(MtnyConstants.USER_ID);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.mtny_inbox_msgs_recyler_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String nameAndMaritialStatus = MtnySingleton.getInstance().mtnyInboxMsgesList.get(position).fullName;
        String ageAndHt = MtnySingleton.getInstance().mtnyInboxMsgesList.get(position).age + ", " + MtnySingleton.getInstance().mtnyInboxMsgesList.get(position).height;
        String religion = MtnySingleton.getInstance().mtnyInboxMsgesList.get(position).religion + ", ";
        String education = MtnySingleton.getInstance().mtnyInboxMsgesList.get(position).education;

        holder.btnAccept.setTag(position);
        holder.btnNotInterest.setTag(position);
        holder.bioDataLinearLayout.setTag(position);

        holder.nameTextView.setText(nameAndMaritialStatus);
        holder.ageTextView.setText(ageAndHt);
        holder.religionTextView.setText(religion);
        holder.educationTextView.setText(education);

       /* boolean shortlisted = MtnySingleton.getInstance().mtnyInboxMsgesList.get(position).shortlisted;
        if (shortlisted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.shortlistTextView.setTextColor(mContext.getColor(R.color.gray));
            } else {
                holder.shortlistTextView.setTextColor(mContext.getResources().getColor(R.color.gray));
            }
            holder.shortlistTextView.setText("ShortListed");
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.shortlistTextView.setTextColor(mContext.getColor(R.color.colorPrimary));
            } else {
                holder.shortlistTextView.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            }
            holder.shortlistTextView.setText("ShortList");
        }*/

    }


    @Override
    public int getItemCount() {
        return MtnySingleton.getInstance().mtnyInboxMsgesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView profileImageView;
        private TextView nameTextView, ageTextView, religionTextView, educationTextView;
        private LinearLayout bioDataLinearLayout;
        private Button btnAccept,btnNotInterest;

        public ViewHolder(View view) {
            super(view);

            profileImageView = (ImageView) view.findViewById(R.id.profile_imageview);
            nameTextView = (TextView) view.findViewById(R.id.name_textview);
            ageTextView = (TextView) view.findViewById(R.id.age_textview);
            religionTextView = (TextView) view.findViewById(R.id.religion_textview);
            educationTextView = (TextView) view.findViewById(R.id.education_textview);

            btnAccept= (Button) view.findViewById(R.id.btn_accept);
            btnNotInterest = (Button) view.findViewById(R.id.btn_not_interested);
            bioDataLinearLayout = (LinearLayout) view.findViewById(R.id.bio_data_linear_layout);

            btnAccept.setOnClickListener(this);
            btnNotInterest.setOnClickListener(this);
            bioDataLinearLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
