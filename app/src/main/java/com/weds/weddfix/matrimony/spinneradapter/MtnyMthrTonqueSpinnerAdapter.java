package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterMthrTongueDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 2/1/2017.
 */

public class MtnyMthrTonqueSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterMthrTongueDataModel> mthrTongueDataModelList;

    public MtnyMthrTonqueSpinnerAdapter(Context context, List<MtnyRegisterMthrTongueDataModel> mtnyMthrTongueDataModelList) {
        this.mContext=context;
        this.mthrTongueDataModelList=mtnyMthrTongueDataModelList;
    }

    @Override
    public int getCount() {
        return mthrTongueDataModelList.size();
    }

    @Override
    public MtnyRegisterMthrTongueDataModel getItem(int position) {
        return mthrTongueDataModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getMthrTongueName(int position) {
        return getItem(position).getMotherTongueName();
    }

    public String getMthrTongueId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, parent, false);
            holder.txtMthrTongueName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtMthrTongueName.setText(getMthrTongueName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtMthrTongueName;
    }
}
