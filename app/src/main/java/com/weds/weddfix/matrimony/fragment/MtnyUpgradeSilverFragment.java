package com.weds.weddfix.matrimony.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weds.weddfix.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MtnyUpgradeSilverFragment extends Fragment {


    public MtnyUpgradeSilverFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.mtny_fragment_upgrade_silver, container, false);
    }

}
