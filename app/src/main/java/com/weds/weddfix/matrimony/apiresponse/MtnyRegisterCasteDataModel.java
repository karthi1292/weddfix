package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/29/2017.
 */

public class MtnyRegisterCasteDataModel {

    @SerializedName("id")
    public String id;
    @SerializedName("casteName")
    public String casteName;

    public MtnyRegisterCasteDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCasteName() {
        return casteName;
    }

    public void setCasteName(String casteName) {
        this.casteName = casteName;
    }
}
