package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/8/2017.
 */
//Commonly used for NewMatches, RecentVwd,Shrtlited,VwdAndNotContacted,WhoVwdMyProfile API
public class MtnyProfileDataCommonResponseModel {
    @SerializedName("userId")
    public String userId;
    @SerializedName("fullName")
    public String fullName;
    @SerializedName("profileId")
    public String profileId;
    @SerializedName("gender")
    public String gender;
    @SerializedName("age")
    public String age;
    @SerializedName("height")
    public String height;
    @SerializedName("religion")
    public String religion;
    @SerializedName("maritalStatus")
    public String maritalStatus;
    @SerializedName("country")
    public String country;
    @SerializedName("education")
    public String education;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("aboutMe")
    public String aboutMe;
    @SerializedName("shortlisted")
    public boolean shortlisted;
    @SerializedName("sendInterested")
    public boolean sendInterested;
    @SerializedName("showProfilePicture")
    public boolean showProfilePicture;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public boolean isShortlisted() {
        return shortlisted;
    }

    public void setShortlisted(boolean shortlisted) {
        this.shortlisted = shortlisted;
    }

    public boolean isSendInterested() {
        return sendInterested;
    }

    public void setSendInterested(boolean sendInterested) {
        this.sendInterested = sendInterested;
    }

    public boolean isShowProfilePicture() {
        return showProfilePicture;
    }

    public void setShowProfilePicture(boolean showProfilePicture) {
        this.showProfilePicture = showProfilePicture;
    }
}
