package com.weds.weddfix.matrimony.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.adapter.MtnySearchRecyclerViewAdapter;

/**
 * Created by karthick on 9/18/2016.
 */
public class MtnySearchFragment extends Fragment implements View.OnClickListener {
    View view;
    private Button searchButton;
    private CardView searchCardView;
    private RecyclerView searchRecyclerView;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.mtny_fragment_search, container, false);

        searchButton=(Button)view.findViewById(R.id.search_button);
        searchCardView=(CardView)view.findViewById(R.id.search_cardview);
        searchRecyclerView=(RecyclerView)view.findViewById(R.id.search_recycler_view);
        searchButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.search_button:
                searchCardView.setVisibility(View.GONE);
                searchRecyclerView.setVisibility(View.VISIBLE);

                MtnySearchRecyclerViewAdapter mtnyMatchesRecyclerViewAdapter = new MtnySearchRecyclerViewAdapter(mContext);

                LinearLayoutManager mtnyLinearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                searchRecyclerView.setLayoutManager(mtnyLinearLayoutManager);
                searchRecyclerView.setAdapter(mtnyMatchesRecyclerViewAdapter);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;

    }
}
