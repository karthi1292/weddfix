package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/8/2017.
 */

public class MtnyCommonRootModel {

    @SerializedName("register")
    public MtnyCommonResponseElementModel mtnyRegisterCommonResponse;

    @SerializedName("partnerPreference")
    public MtnyCommonResponseElementModel mtnyResgisterPartnerResponse;
}
