package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by KarthickRaja on 2/27/2017.
 */

public class MtnyMessagesCommonElementModel {
    @SerializedName("inbox")
    public ArrayList<MtnyMessagesCommonResponseModel> mtnyInboxMessagesModelList;
    @SerializedName("accepted")
    public ArrayList<MtnyMessagesCommonResponseModel> mtnyAcceptedMessagesModelList;
    @SerializedName("sent")
    public ArrayList<MtnyMessagesCommonResponseModel> mtnySentMessagesModelList;
    @SerializedName("notInterested")
    public ArrayList<MtnyMessagesCommonResponseModel> mtnyNotInterestedMessagesModelList;


}
