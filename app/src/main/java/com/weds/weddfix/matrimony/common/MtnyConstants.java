package com.weds.weddfix.matrimony.common;

import android.Manifest;

/**
 * Created by KarthickRaja on 2/3/2017.
 */

public class MtnyConstants {
    public static String SUCCESS = "success";
    public static String TOAST_CONNECTION_ERROR = "Communication Failure";
    public static final String MyPREFERENCESNAME = "WeddfixCategory";
    public static String[] PERMISSIONS = {
            Manifest.permission.READ_CONTACTS, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    //Store Preference Constants
    public static String USER_ID = "userId";
    public static String PROFILE_ID = "profileId";
    public static String LOGGED_IN = "loggedIn";

    //Intent Constants
    public static String VIEW_OTHER_USER_ID="viewOtherUserId";
    public static String VIEW_OTHER_PROFILE_ID="viewOtherProfileId";

}
