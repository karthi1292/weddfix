package com.weds.weddfix.matrimony.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/22/2017.
 */
public class MtnyRegisterOccupationDataModel implements Parcelable {
    @SerializedName("id")
    public String id;
    @SerializedName("occupationName")
    public String occupationName;

    public MtnyRegisterOccupationDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOccupationName() {
        return occupationName;
    }

    public void setOccupationName(String occupationName) {
        this.occupationName = occupationName;
    }

    protected MtnyRegisterOccupationDataModel(Parcel in) {
        id = in.readString();
        occupationName = in.readString();
    }

    public static final Creator<MtnyRegisterOccupationDataModel> CREATOR = new Creator<MtnyRegisterOccupationDataModel>() {
        @Override
        public MtnyRegisterOccupationDataModel createFromParcel(Parcel in) {
            return new MtnyRegisterOccupationDataModel(in);
        }

        @Override
        public MtnyRegisterOccupationDataModel[] newArray(int size) {
            return new MtnyRegisterOccupationDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(occupationName);
    }
}
