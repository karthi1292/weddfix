package com.weds.weddfix.matrimony.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 2/3/2017.
 */

public class MtnyForgotPwdModel {
    @SerializedName("status")
    public String status;
    @SerializedName("mtny_message")
    public String message;
}

