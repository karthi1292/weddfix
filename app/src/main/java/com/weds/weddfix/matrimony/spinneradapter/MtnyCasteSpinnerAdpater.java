package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterCasteDataModel;


import java.util.List;

/**
 * Created by KarthickRaja on 1/31/2017.
 */

public class MtnyCasteSpinnerAdpater extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterCasteDataModel> casteDataModelList;

    public MtnyCasteSpinnerAdpater(Context context, List<MtnyRegisterCasteDataModel> mtnyRegisterCasteDataModelList) {

        this.mContext=context;
        this.casteDataModelList=mtnyRegisterCasteDataModelList;
   }

    @Override
    public int getCount() {
        return casteDataModelList.size();
    }

    @Override
    public MtnyRegisterCasteDataModel getItem(int position) {
        return casteDataModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getCasteName(int position) {
        return getItem(position).getCasteName();
    }

    public String getCasteId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, parent, false);
            holder.txtCasteName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtCasteName.setText(getCasteName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtCasteName;
    }
}
