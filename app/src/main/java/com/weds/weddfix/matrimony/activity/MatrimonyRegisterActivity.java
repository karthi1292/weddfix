package com.weds.weddfix.matrimony.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;


import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.common.MtnyBaseActivity;
import com.weds.weddfix.matrimony.fragment.MtnyRegisterBasicsFragment;


public class MatrimonyRegisterActivity extends MtnyBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mtny_activity_register);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        MtnyRegisterBasicsFragment mtnyRegisterBasicsFragment = new MtnyRegisterBasicsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.register_frame, mtnyRegisterBasicsFragment).commit();
    }
}
