package com.weds.weddfix.matrimony.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by KarthickRaja on 1/22/2017.
 */
public class MtnyRegisterProfileDataModel implements Parcelable {
    @SerializedName("id")
    public String id;
    @SerializedName("profileFor")
    public String profileFor;

    public MtnyRegisterProfileDataModel() {

    }

    protected MtnyRegisterProfileDataModel(Parcel in) {
        id = in.readString();
        profileFor = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfileFor() {
        return profileFor;
    }

    public void setProfileFor(String profileFor) {
        this.profileFor = profileFor;
    }

    public static Creator<MtnyRegisterProfileDataModel> getCREATOR() {
        return CREATOR;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(profileFor);
    }

    public static final Creator<MtnyRegisterProfileDataModel> CREATOR = new Creator<MtnyRegisterProfileDataModel>() {
        @Override
        public MtnyRegisterProfileDataModel createFromParcel(Parcel in) {
            return new MtnyRegisterProfileDataModel(in);
        }

        @Override
        public MtnyRegisterProfileDataModel[] newArray(int size) {
            return new MtnyRegisterProfileDataModel[size];
        }
    };
}
