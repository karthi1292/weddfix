package com.weds.weddfix.matrimony.spinneradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weds.weddfix.R;
import com.weds.weddfix.matrimony.apiresponse.MtnyRegisterEducationDataModel;

import java.util.List;

/**
 * Created by KarthickRaja on 1/27/2017.
 */

public class MtnyHighestEducationSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<MtnyRegisterEducationDataModel> mtnyRegisterEducationDataModelArrayList;

    public MtnyHighestEducationSpinnerAdapter(Context context, List<MtnyRegisterEducationDataModel> educationDataModelList) {
        this.mContext=context;
        this.mtnyRegisterEducationDataModelArrayList=educationDataModelList;
    }

    @Override
    public int getCount() {
        return mtnyRegisterEducationDataModelArrayList.size();
    }

    @Override
    public MtnyRegisterEducationDataModel getItem(int position) {
        return mtnyRegisterEducationDataModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getEducationName(int position) {
        return getItem(position).getEducationName();
    }

    public String getEducationId(int position) {
        return getItem(position).getId();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, parent, false);
            holder.txtEducationName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtEducationName.setText(getEducationName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtEducationName;
    }
}
